<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Googlecalendar extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->library('google_calendar');
        $this->calendar = new Google_Service_Calendar($this->google_calendar->client());
    }

    public function getEvents(){
        $timeMin = false;
        $timeMax = false;
        if (!$timeMin) {
            $timeMin = date("c", strtotime(date('Y-m-d ').' 00:00:00'));
        } else {
            $timeMin = date("c", strtotime($timeMin));
        }
        if (!$timeMax) {
            $timeMax = date("c", strtotime(date('Y-m-d ').' 23:59:59'));
        } else {
            $timeMax = date("c", strtotime($timeMax));
        }
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );
        $results = $this->calendar->events->listEvents('primary', $optParams);

        //print_r($results->getItems()).die();
        
        $data = array();
        foreach ($results->getItems() as $item) {
            $start = date('d-m-Y H:i', strtotime($item->getStart()->dateTime));

            $attendees = array();
            foreach($item->attendees as $att){
                array_push(
                    $attendees,
                    array(
                        'name'      => $att->displayName,
                        'email'     => $att->email
                    )
                );
            }

            $arr_item = array(
                'id'          => $item->getId(),
                'summary'     => $item->getSummary(),
                'location'    => $item->getLocation(),
                'description' => $item->getDescription(),
                'creator'     => $item->getCreator(),
                'start'       => $item->getStart()->dateTime,
                'end'         => $item->getEnd()->dateTime,
                'attendees'   => $attendees,
            );
            array_push($data,$arr_item);
        }
        return $data;

    }
    public function addEvent($data)
    {
        $array = array(
            'summary'   => $data['summary'],
            'location'  => $data['location'],
            'description'=> $data['description'],
            'start' => array(
                'dateTime' => $data['start_time'],
                'timeZone' => 'Asia/Jakarta',
            ),
            'end' => array(
                'dateTime' => $data['end_time'],
                'timeZone' => 'Asia/Jakarta',
            ),
            'recurrence' => array(
                'RRULE:FREQ=DAILY;COUNT=2'
            ),
            'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 24 * 60),
                    array('method' => 'popup', 'minutes' => 10),
                ),
            )
        );
        $event = new Google_Service_Calendar_Event($array);

        $email_invite_arr = explode(',', $data['email']);
        foreach($email_invite_arr as $item){
            if($item){
                $attendee = new Google_Service_Calendar_EventAttendee();
                $attendee->setEmail($item);
                $attendee_arr[]= $attendee;
            }
        }
        
        $optParams = Array(
            'sendNotifications' => true,
        );
        $createdEvent = $this->calendar->events->insert('primary', $event);
        $event->setAttendees($attendee_arr);
        return $this->calendar->events->patch('primary',$createdEvent->getId(),$event, $optParams);
    }
}