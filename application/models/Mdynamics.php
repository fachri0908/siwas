<?php

class Mdynamics extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    function get($table='',$where='',$order_column='',$order_type='') {
        if (!empty($where)){
            $query = $this->db->where($where);
        }
        if (!empty($order_column) && !empty($order_type)) {
            $this->db->order_by($order_column, $order_type);
        }
        $query = $this->db->get($table);
        return $query;
    }
    function save($table,$data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    function update($table,$where,$data) {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
    function delete($table,$where) {
        $this->db->where($where);
        return $this->db->delete($table);
    }
}

?>
