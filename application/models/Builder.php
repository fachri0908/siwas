<?php

class Builder extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    function getAll($table){
        $this->db->order_by('created_at', 'DESC');
        return $this->db->get_where($table, array('deleted_at' => NULL))->result();
    }
    
    function getAllOrder($table,$order,$type){
        $this->db->order_by($order, $type);
        return $this->db->get_where($table, array('deleted_at' => NULL))->result();
    }
    
    function store($table,$data,$redirect){
        if($this->db->insert($table,$data)){
            $this->session->set_flashdata('success','Berhasil Menambahkan Data');
            redirect($redirect);
        }else{
            // $this->session->set_flashdata('failed','Gagal Menambahkan Data');
            // redirect($redirect);
            echo json_encode($this->db->error());
        }
    }
    
    function destroy($table,$id, $redirect){
        $this->db->where('id',$id);
        if($this->db->update($table,array('deleted_at'=>date('Y-m-d H:i:s')))){
            $this->session->set_flashdata('success','Data Berhasil Dihapus');
            redirect($redirect);
        }else{
            $this->session->set_flashdata('failed','Gagal Menghapus Data');
            redirect($redirect);
        }
    }
    
    function update($table, $data,$redirect){
        $this->db->where('id', $data['id']);
        if($this->db->update($table,$data)){
            $this->session->set_flashdata('success',' Data Berhasil Diubah');
            redirect($redirect);
        }else{
            $this->session->set_flashdata('failed','Gagal Merubah Data');
            redirect($redirect);
        }
    }
    
    function getNewApplicantCode(){
        $x=$this->db->query("select id from applicants order by id desc limit 1")->result();
        if(count($x)>0){
            $l=$x[0]->id+1;
            return date('Ymd')."-".$l;
        }else{
            return date('Ymd').'-1';
        }
    }
    
    function getNewEmployeeCode(){
        $x=$this->db->query("select id from employees order by id desc limit 1")->result();
        if(count($x)>0){
            $l=$x[0]->id+1;
            return "EMP-".$l;
        }else{
            return 'EMP-1';
        }
    }
    
    function getNewExtensionCode(){
        $x=$this->db->query("select id from extensions order by id desc limit 1")->result();
        if(count($x)>0){
            $l=$x[0]->id+1;
            return 'EXT-'.str_pad($l,4,"0",STR_PAD_LEFT);
        }else{
            return 'EXT-0001';
        }
    }
    
    function getNewBillboardCode(){
        $x=$this->db->query("select id from billboards order by id desc limit 1")->result();
        if(count($x)>0){
            $l=$x[0]->id+1;
            return date('Ymd').'-'.str_pad($l,4,"0",STR_PAD_LEFT);
        }else{
            return date('Ymd').'-0001';
        }
    }
    
    function getNewPaymentCode(){
        $x=$this->db->query("select id from payments order by id desc limit 1")->result();
        if(count($x)>0){
            $l=$x[0]->id+1;
            return 'PMT-'.str_pad($l,4,"0",STR_PAD_LEFT);
        }else{
            return 'PMT-0001';
        }
    }
    
    function storeReturnId($table,$data,$redirect){
        if($this->db->insert($table,$data)){
            return $this->db->insert_id();
        }else{
            $this->session->set_flashdata('failed','Gagal Menambahkan Data');
            redirect($redirect);
        }
    }
    
    function getCodeById($table,$id){
        $this->db->select('code');
        $x=$this->db->get_where($table,array('id' => $id))->result();
        return $x[0]->code;
    }
    
    function getIdByCode($table,$code){
        $this->db->select('id');
        $x=$this->db->get_where($table,array('code' => $code))->result();
        return $x[0]->id;
    }
    
    
    function getRecordById($table,$conditions){
        return $this->db->get_where($table,$conditions)->result();
    }
    
    function getRecordByCond($table,$conditions){
        return $this->db->get_where($table,$conditions)->result();
    }
    
    
    function raw($query){
        $x=$this->db->query($query)->result();
        return $x;
    }
    
    function getNameByCond($table,$cond){
        $data=$this->db->get_where($table,$cond)->result();
        if(count($data)>0){
            echo $data[0]->name;
        }else{
            "-";
        }
    }
    
    function checkAttachment($code){
        $data=$this->db->query("select id from  registration_attachments where billboard_code='$code'")->result();
        return count($data);
    }
    
    function generateNsrAndTax($code, $data){
        $area=$data['area']; //kelas jalan (protokol a, b atau lingkungan dll)
        $category=$data['category']; //kategori produk non produk
        $isCigarette=$data['cigarette']; //rokok atau bukan
        $isThirdPerson=$data['third_person']; //oleh pihak ketiga atau bukan
        $contractValue=$data['contract_value']; //nilai kontrak jika melalui pihak ketiga
        $location=$data['location'];//lokasi pemasangan
        $height=$data['height'];//tinggi reklame
        $size=$data['size'];//luas reklame
        $tax=$data['tax'];//persentase pajak
        $days=$data['total_days'];//jumlah hari pemasangan
        $times=$data['total_seconds'];//jumlah detik penayangan (led/suara/film)
        $type=$data['type'];//tipe reklame
        $slbTotal=$data['total_selebaran'];
        $months=$data['month'];//jumlah bulan penayangan (udara)
        $totalPrg=$data['total_prg'];//total peragaan(reklame peragaan only)
        $tax=$tax/100;
        
        //base for others
        $nsr=$this->db->query("select rate from nsr where street_class='$area' and type='$category'")->result();
        $nsr=$nsr[0]->rate;
        if($type=='PBM' || $type=='KN'){
            $nsr=$nsr*$size*$days;
        }else if($type=="LED"){
             //base for led
            $nsr=$this->db->query("select price from nsr_price where start_size <= $size and end_size >= $size")->result();
            $nsr=$nsr[0]->price;
            $nsr=$nsr*ceil($times/30);
        }else if($type=="STK"){
            $nsr=$nsr*$size;
        }else if($type=="SLB"){
            $nsr=$nsr*$slbTotal;
        }else if($type=="BRJ"){
            $nsr=$nsr*$size*$days;
        }else if($type=="UDR"){
            $nsr=$nsr*$month;
        }else if($type=="APG"){
            $nsr=$nsr*$month;
        }else if($type=="SR"){
            $nsr=$nsr * (ceil($times/30));
        }else if($type=="FLM"){
            $nsr=$nsr * (ceil($times/30));
        }else if($type=="PRG"){
            $nsr=$nsr * $total_prg;
        }
        
        if($isCigarette==1){
            $nsr=$nsr+(25/100*$nsr);
        }
        
        if($height>15){
            $mheight=$height-15;//kelebihan 15meter
            $mheight=ceil($mheight/15);
            $nsr=$nsr+(20/100*$mheight*$nsr);
        }
        
        if($location=='INDR'){
            $nsr=$nsr/2;
        }
        
        
        //total pajak jika penyelenggara adalah pihak ketiga
        if($isThirdPerson==1){
            $totaltax=$tax*$contractValue;
        }else{
            $totaltax=$tax/$nsr;
        }
        $nsrdata=array(
            'code'=>$code,
            'nsr'=>$nsr,
            'tax'=>$totaltax,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        $this->db->where('code',$code);
        $this->db->update('billboards', $nsrdata);
    }
    
    function generateRequirements($code, $data){
        $reqs=array('BDFRONT','KTP','KK','PBB','SPIZIN','SPIMBBR','VP1','VP2','VP3','GIPTB','LAKLAL','SPARS');
        // jika reklame dibuat oleh pihak ketiga
        if($data['third_person']==1){
            array_push($reqs, "SKUASA");
        }
        // jika reklame melekat pada dinding
        if($data['building']==1){
            array_push($reqs, "IMB");
        }
        
        //jika reklame 2sisi
        if($data['view_point']=="VP2"){
            array_push($reqs, "BDSIDE1");
        }
        //jika reklame 3 sisi
        if($data['view_point']=="VP3"){
            array_push($reqs, "BDSIDE1","BDSIDE2");
        }
        //jika reklame 4 sisi
        if($data['view_point']=="VP4"){
            array_push($reqs, "BDSIDE1","BDSIDE2","BDBACK");
        }
        
        //location
        if($data['location']=="TPP"){
            array_push($reqs, "RTPL");
        }else if($data['location']=="TPR"){
            array_push($reqs, "SHM");
        }else if($data['location']=="TBUM"){
            array_push($reqs, "SPBUM");
        }else if($data['location']=="TSW"){
            array_push($reqs, "PSPTB","SPTK","KTP-PT");
        }
        
        $this->db->trans_start();
        foreach($reqs as $r){
            $data=array(
                'billboard_code'=>$code,
                'type'=>$r,
                'created_by'=>$this->session->userdata('user_id')
            );
            $this->db->insert('registration_attachments', $data);
        }
        $this->db->trans_complete();
    }
    
    
    public function checkCompletenes($code){
        $data=$this->db->query("select (select count(id) from registration_attachments where billboard_code='$code') as total, (select count(id) from registration_attachments where billboard_code='$code' and file is not NULL) as uploaded")->result();
        return $data[0];
    }
    
    public function checkExtensionCompletenes($code){
        $data=$this->db->query("select (select count(id) from extension_attachments where extension_code='$code') as total, (select count(id) from extension_attachments where extension_code='$code' and file is not NULL) as uploaded")->result();
        return $data[0];
    }
    
    public function extend($code){
        $ext=$this->db->query("select billboard_code,duration,finish_date,time_duration from extensions where code='$code'")->result();
        $bbcode=$ext[0]->billboard_code;
        $bb=$this->db->query("select code,extension,finish_date,install_duration,time_duration from billboards where code='$bbcode'")->result();
        $data=array(
            'extension'=>$bb[0]->extension+1,
            'finish_date'=>$ext[0]->finish_date,
            'install_duration'=>$ext[0]->duration+$bb[0]->install_duration,
            'time_duration'=>$ext[0]->time_duration+$bb[0]->time_duration
        );
        $this->db->where('code', $bb[0]->code);
        $this->db->update('billboards', $data);
    }
    
    public function generatePayment($type, $billboard_code, $approve_date){
        $billboard=$this->db->query("select applicant_code,nsr,tax,contract_value from billboards where code='$billboard_code'")->result();
        $due=$this->db->query("select due_days from payment_due where payment_type='$type'")->result();
        $due=$due[0]->due_days." days";
        $duedate=date_create($approve_date);
        date_add($duedate,date_interval_create_from_date_string($due));
        $duedate=date_format($duedate,"Y-m-d");
        $duedate=date('Y-m-d',strtotime($duedate));
        $data=array(
            'code'=>$this->getNewPaymentCode(),
            'applicant_code'=>$billboard[0]->applicant_code,
            'billboard_code'=>$billboard_code,
            'type'=>$type,
            'nsr'=>$billboard[0]->nsr,
            'contract_value'=>$billboard[0]->contract_value,
            'tax'=>$billboard[0]->tax,
            'status'=>'UNPAID',
            'due_date'=>$duedate,
        );
        $this->db->insert('payments',$data);
    }
    
    public function generateExtensionPayment($extensioncode){
        $extension=$this->db->query("select * from extensions where code='$extensioncode'")->result();
        $billboard_code=$extension[0]->billboard_code;
        $billboard=$this->db->query("select applicant_code,nsr,tax,contract_value from billboards where code='$billboard_code'")->result();
        $billboard=$billboard[0];
        $due=$this->db->query("select due_days from payment_due where payment_type='EXT'")->result();
        $due=$due[0]->due_days." days";
        $duedate=date_create(date('Y-m-d'));
        date_add($duedate,date_interval_create_from_date_string($due));
        $duedate=date_format($duedate,"Y-m-d");
        $duedate=date('Y-m-d',strtotime($duedate));
        $data=array(
            'code'=>$this->getNewPaymentCode(),
            'applicant_code'=>$billboard->applicant_code,
            'billboard_code'=>$billboard_code,
            'type'=>'EXT',
            'nsr'=>$extension[0]->nsr,
            'contract_value'=>$extension[0]->contract_value,
            'tax'=>$extension[0]->tax,
            'status'=>'UNPAID',
            'due_date'=>$duedate,
        );
        $this->db->insert('payments',$data);
    }
    
    function generateExtensionNsr($billboard_code, $duration, $time){
        $billboard=$this->db->query("select * from billboards where code='$billboard_code'")->result();
        $billboard=$billboard[0];
        if($billboard->product_type=='RKAL'){
            $isCigarette=1;
        }else{
            $isCigarette=0;
        }
        
        if($billboard->contract_value!=NULL){
            $contractValue=$billboard->contract_value;
            $isThirdPerson=1;
        }else{
            $contractValue=NULL;
            $isThirdPerson=0;
        }
        $area=$billboard->street_class; //kelas jalan (protokol a, b atau lingkungan dll)
        $category=$billboard->category; //kategori produk non produk
        $location=$billboard->placement_billboard;//lokasi pemasangan
        $height=$billboard->height;//tinggi reklame
        $size=$billboard->size;//luas reklame
        $tax=$billboard->year_rate;//persentase pajak
        $days=$duration;//jumlah hari pemasangan
        $times=$time;//jumlah detik penayangan (led/suara/film)
        $type=$billboard->billboard_type;//tipe reklame
        $slbTotal=$duration;
        $months=$duration;//jumlah bulan penayangan (udara)
        $totalPrg=$duration;//total peragaan(reklame peragaan only)
        $tax=$tax/100;
        
        //base for others
        $nsr=$this->db->query("select rate from nsr where street_class='$area' and type='$category'")->result();
        $nsr=$nsr[0]->rate;
        if($type=='PBM' || $type=='KN'){
            $nsr=$nsr*$size*$days;
        }else if($type=="LED"){
             //base for led
            $nsr=$this->db->query("select price from nsr_price where start_size <= $size and end_size >= $size")->result();
            $nsr=$nsr[0]->price;
            $nsr=$nsr*ceil($times/30);
        }else if($type=="STK"){
            $nsr=$nsr*$size;
        }else if($type=="SLB"){
            $nsr=$nsr*$slbTotal;
        }else if($type=="BRJ"){
            $nsr=$nsr*$size*$days;
        }else if($type=="UDR"){
            $nsr=$nsr*$month;
        }else if($type=="APG"){
            $nsr=$nsr*$month;
        }else if($type=="SR"){
            $nsr=$nsr * (ceil($times/30));
        }else if($type=="FLM"){
            $nsr=$nsr * (ceil($times/30));
        }else if($type=="PRG"){
            $nsr=$nsr * $total_prg;
        }
        
        if($isCigarette==1){
            $nsr=$nsr+(25/100*$nsr);
        }
        
        if($height>15){
            $mheight=$height-15;//kelebihan 15meter
            $mheight=ceil($mheight/15);
            $nsr=$nsr+(20/100*$mheight*$nsr);
        }
        
        if($location=='INDR'){
            $nsr=$nsr/2;
        }
        
        
        //total pajak jika penyelenggara adalah pihak ketiga
        if($isThirdPerson==1){
            $totaltax=$tax*$contractValue;
        }else{
            $totaltax=$tax/$nsr;
        }
        $nsrdata=array(
            'nsr'=>$nsr,
            'tax'=>$totaltax,
        );
        return $nsrdata;
    }
    
    
    
    // public function generateExtensionData($billboard_code, $extension_code){
    //     $billboard=$this->db->query("select * from billboards where code='$billboard_code'")->result();
    //     $extension=$this->db->query("select * from extensions where code='$extension_code'")->result();
    // }
    
    public function generateNewLog($billboard_code){
        $billboard=$this->db->query("select * from billboard where code='$billboard_code'")->result();
        $log=array(
            'billboard_code'=>$billboard_code,
            'start_date'=>$billboard[0]->install_date,
            'finish_date'=>$billboard[0]->finish_date,
            'nsr'=>$billboard[0]->nsr,
            'contract_value'=>$billboard[0]->contract_value,
            'tax'=>$billboard[0]->tax,
            'created_by'=>$this->session->userdata('user_id')
        );
        $this->db->insert('billboard_logs', $log);
    }
    
    public function generateExtLog($extension_code){
        $extension=$this->db->query("select * from extension where code='$extension_code")->result();
        $bcode=$extension[0]->billboard_code;
        $billboard=$this->db->query("select * from billboard where code='$bcode'")->result();
        $log=array(
            'billboard_code'=>$billboard_code,
            'start_date'=>$extension[0]->start_date,
            'finish_date'=>$extension[0]->finish_date,
            'nsr'=>$billboard[0]->nsr,
            'contract_value'=>$billboard[0]->contract_value,
            'tax'=>$billboard[0]->tax,
            'created_by'=>$this->session->userdata('user_id')
        );
        $this->db->insert('billboard_logs', $log);
    }
    
    // public function getExtensionNsr(){
        
    // }
    
    public function generateQr($billboard_code){
        header("Content-Type: image/png");
        $this->load->library('ciqrcode');
        $params['data'] = 'https://smiwebportal.com/siwas/showbillboard?code='.$billboard_code;
        $params['savename'] = './assets/image-upload/qr/'.$billboard_code.'.png';
        $this->ciqrcode->generate($params);
    }
}

?>
