<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Libemail {
    
    protected $_ci;
    
    function __construct() {
        $this->_ci = &get_instance();
        require_once('PHPMailer/src/Exception.php');
        require_once('PHPMailer/src/PHPMailer.php');
        require_once('PHPMailer/src/SMTP.php');
    }
    
    function send_email($email,$subject,$message){
    	$mail = new PHPMailer();// create a new object
        $mail->From     = 'hrsystem@sentral.co.id';
        $mail->FromName = 'Employee Self-Service';
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            ) 
         );
        $mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true;  // authentication enabled
        $mail->Username = 'hrsystem@sentral.co.id';
        $mail->Password = 'hrsystem123';
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = 'mail.sentral.co.id';
        $mail->Port = '465';
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $message;
        $mail->AddAddress($email);
        $mail->send();
    }
    function confirmemail($email, $subject,$message){
    	$mail = new PHPMailer();// create a new object
        $mail->From     = 'hrsystem@sentral.co.id';
        $mail->FromName = 'Employee Self-Service';
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            ) 
         );
        $mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true;  // authentication enabled
        $mail->Username = 'hrsystem@sentral.co.id';
        $mail->Password = 'hrsystem123';
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = 'mail.sentral.co.id';
        $mail->Port = '465';
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $message;
        
        try{
            $mail->send(); // Attempt to send the email
            $path = "{mail.sentral.co.id:143/notls}INBOX.Sent"; 
            //Tell your server to open an IMAP connection using the same username and password as you used for SMTP 
            $imapStream = imap_open($path, 'hrsystem@sentral.co.id', 'hrsystem123'); 
            $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage()); 
            imap_close($imapStream);
        } catch(phpmailerException $e) {
            echo $e->errorMessage();
        }
    }
}
