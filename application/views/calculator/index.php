<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label">Kategori</label>
                            <select class="from-control" name="category" id="category" onchange="getNsr()" data-toggle="select">
                                <option selected hidden value="0"></option>
                                <option value="PRD">Produk</option>
                                <option value="NPRD">Non Produk</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Area Pemasangan</label>
                            <select class="form-control" id="street_class" onchange="getNsr()" data-toggle="select" name="street_class">
                                <option></option>
                                <?php foreach($street_class as $s){ ?>
                                <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <!--<div class="form-group">-->
                        <!--    <label class="form-control-label">Jenis Reklame</label>-->
                        <!--    <select class="from-control" name="billboard_type" id="billboard_type" data-toggle="select">-->
                        <!--        <option value="0" selected hidden></option>-->
                        <!--        <?php foreach($types as $t){ ?>-->
                        <!--        <option value="<?=$t->code?>"><?=$t->name?></option>-->
                        <!--        <?php } ?>-->
                        <!--    </select>-->
                        <!--</div>-->
                        <!--<div class="form-group">-->
                        <!--    <label class="form-control-label">Sudut Pandang</label>-->
                        <!--    <select class="form-control" name="view_point">-->
                        <!--        <option></option>-->
                        <!--        <?php foreach($viewpoints as $s){ ?>-->
                        <!--        <option value="<?=$s->code?>"><?=$s->name?></option>-->
                        <!--        <?php } ?>-->
                        <!--    </select>-->
                        <!--</div>-->
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Panjang (m)</label>
                                <input class="form-control" type="text" onkeyup="getNsr()" id="length" value="1" name="length" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Lebar (m)</label>
                                <input class="form-control" type="text" name="width" onkeyup="getNsr()" id="width" onc value="1" placeholder="Default input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Tinggi Media</label>
                            <input type="text" class="form-control" name="height" type="text" placeholder="tinggi media">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Durasi Pemasangan (hari)</label>
                            <input type="number" class="form-control" name="duration" id="duration" value="1" onkeyup="getNsr()" type="text" placeholder="Durasi Pemasangan">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Tahun Tarif</label>
                            <select name="year_rate" class="form-control" id="year_rate" onchange="getNsr()" data-toggle="select">
                                <option value="0"></option>
                                <?php foreach($rates as $r) { ?>
                                <option value="<?=$r->rate?>"><?=$r->year?> | <?=$r->rate?>%</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Permohonan</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label">Luas m<sup>2</sup></label>
                            <input type="text" class="form-control" name="size" value="1" id="size" type="text" placeholder="luas m/2" readonly="">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">NSR</label>
                            <input type="text" class="form-control" name="nsr" id="nsr" type="text" placeholder="Nilai Sewa Reklame" readonly="">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Pajak</label>
                            <input type="text" class="form-control" name="tax" id="tax" type="text" placeholder="Pajak" readonly="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getNsr(){
        let cat = $('#category').val()
        let type= $('#street_class').val()
        let length= $('#length').val()
        let width= $('#width').val()
        let duration= $('#duration').val()
        $('#size').val(length*width)
        if(cat!=0 && type!=0){
            $.get("<?=base_url()?>calculator/getNsr?cat="+cat+"&class="+type, function(res){
                let size=length*width
                console.log(size)
                $("#nsr").val(res*size*duration)
                getTax(res*size*duration)
            });
        }
        
    }
    function getTax(nsr){
        let rate=$('#year_rate').val()
        $('#tax').val((rate/100)*nsr)
    }
</script>