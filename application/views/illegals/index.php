<div class="container-fluid mt--6">
        <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php } ?>
        
        
      <div class="card">
            <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0 float-left">Reklame Ilegal</h3>
          <a class="btn btn-primary btn-sm float-right" href="<?=base_url()?>illegals/create">Tambah reklame ilegal</a>
          <!-- <p class="text-sm mb-0">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p> -->
        </div>
        
        <div class="table-responsive py-4">
          <table class="table table-flush">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Text Reklame</th>
                <th>Jalan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $x=1; foreach($illegals as $b){
              ?>
              <tr>
                <td width="5%"><?=$x++?></td>
                <td><?=$b->billboard_text?></td>
                <td><?=$b->street_name?></td>
                <td width="5%">
                    <a href="<?=base_url()?>illegals/show/<?=$b->id?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        
      </div>
</div>



<div class="modal" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        
        <div class="modal-header">
            <h4 class="modal-title">Detail - <span id="name"></span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
            <div id="detail-reklame"></div>
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>


<script>
    function loadBillboard(status){
        $.get("<?=base_url()?>lists/billboards/"+status, function(res){
            console.log(res)
        });
    }
</script>