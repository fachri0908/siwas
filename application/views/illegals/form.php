<style>
               
  #map { 
    height: 400px;    
    width: 100%;            
  }  
</style>
<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
        <form method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Data Reklame Ilegal</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Tipe Reklame</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="billboard_type" id="city_id" data-toggle="select">
                                    <option selected hidden></option>
                                        <?php foreach($types as $t){ ?>
                                        <option value="<?=$t->code?>"><?=$t->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Text Reklame</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" name="billboard_text" class="form-control form-control-small" id="billboard_text" placeholder="Text Reklame">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Kota</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="city_code" id="city_id" data-toggle="select" onchange="changeCity(this.value)">
                                        <option selected hidden></option>
                                        <?php foreach($city as $c){ ?>
                                        <option value="<?=$c->code?>"><?=$c->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Kecamatan</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="district_id" id="district" data-toggle="select" onchange="changeDistrict(this.value)">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label>jalan</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="street_id" id="street" data-toggle="select" onchange="codeAddress(this)">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div id="map"></div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label class="control-label">Latitude, Longitude</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="latitude_longitude" id="latlong" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Gambar Reklame</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="file" name="picture" class="form-control form-control-small" id="picture" placeholder="Foto Reklame">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script>
    function changeCity(val){
        $.get("<?=base_url()?>lists/districtByCity/"+val, function(res){
            res=JSON.parse(res)
            $("#district").empty();
            $("#district").append(new Option('',''));
            res.forEach(function(el){
                $("#district").append(new Option(el.name,el.id));
            })
        });
    }
    
    function changeDistrict(val){
        $.get("<?=base_url()?>lists/streetListByDistrict/"+val, function(res){
            res=JSON.parse(res)
            $("#street").empty();
            $("#street").append(new Option('',''));
            res.forEach(function(el){
                $("#street").append(new Option(el.name,el.id));
            })
        });
    }
</script>
<script type="text/javascript">
    var geocoder;
    var map;
    function initMaps() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.2087634, 106.845599),
          zoom: 17,
          disableDoubleClickZoom: true,
        });
        
        // Update lat/long value of div when anywhere in the map is clicked    
        google.maps.event.addListener(map,'click',function(event) {                
            document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
        });   
        
        // Create new marker on double click event on the map
        google.maps.event.addListener(map,'dblclick',function(event) {
            var marker = new google.maps.Marker({
              position: event.latLng, 
              map: map, 
              title: event.latLng.lat()+', '+event.latLng.lng()
            });
            
            // Update lat/long value of div when the marker is clicked
            marker.addListener('click', function() {
              document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
            });            
        });
    }
    
    function codeAddress(selected) {
        var address = selected.options[selected.selectedIndex].text
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            document.getElementById('latlong').value = results[0].geometry.location.lat()+','+results[0].geometry.location.lng();
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: results[0].geometry.location
            });
          } else {
            // alert("Geocode was not successful for the following reason: " + status);
          }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmOv0twOwgE0VlvmWnmtb1B8f1KYnbR9E&callback=initMaps" async defer></script>