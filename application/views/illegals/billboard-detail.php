<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Status Permohonan</h3>
                        <a href="<?=base_url()?>billboards" class="btn btn-danger btn-sm float-right"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                    <div class="card-body">
                        <div class="timeline timeline-one-side" data-timeline-content="axis" data-timeline-axis-style="dashed">
                            <?php $x=1; foreach($levels as $l){ ?>
                            <?php $appr=$this->builder->getRecordByCond('approvals',['billboard_code'=>$billboard[0]->code,'level'=>$l->level]);
                                $icon='history';
                                $badge='secondary';
                                $status='Menunggu Pengecekan';
                                $receive='Menunggu Pengecekan';
                                $approve='Menunggu Pengecekan';
                                $text='dark';
                                if(count($appr)>0){
                                    if($appr[0]->approve_status==0){
                                        $badge='secondary';
                                        $icon='history';
                                        $status='Menunggu Pengecekan';
                                        $text='dark';
                                    }else if($appr[0]->approve_status==1){
                                        $badge='success';
                                        $icon='check';
                                        $status='Disetujui';
                                        $text='success';
                                    }else{
                                        $badge='danger';
                                        $icon='times';
                                        $status='Ditolak';
                                        $text='danger';
                                    }
                                    $receive=date('d F Y', strtotime($appr[0]->receive_at));
                                    if($appr[0]->approved_at != NULL){
                                        $approve=date('d F Y', strtotime($appr[0]->approved_at));
                                    }else{
                                        $approve='Menunggu Pengecekan';
                                    }
                                }
                            ?>
                            
                            <div class="timeline-block">
                              <span class="timeline-step badge-<?=$badge?>">
                                <i class="fa fa-<?=$icon?>"></i>
                              </span>
                              <div class="timeline-content">
                                <div class="d-flex justify-content-between pt-1">
                                  <div>
                                    <span class="text-<?=$text?> text-sm font-weight-bold"><?=$l->name?> (<?=$approve?>)</span>
                                  </div>
                                  <div class="text-right">
                                    <small class="text-muted"><i class="fas fa-clock-o"></i><?=$receive?></small>
                                  </div>
                                </div>
                                <h6 class="text-sm mt-1 mb-2"><?=$status?></h6>
                                <?php if(count($appr)>0 && $this->session->userdata('user_position')==$l->approver_position_code && $appr[0]->approve_status==0){ ?>
                                    <a href="<?=base_url()?>billboards/approve/<?=$l->level?>?code=<?=$billboard[0]->code?>" class="btn btn-success btn-sm" onclick="return confirm('Are you sure you want to confirm this billboard?')"><i class="fa fa-check"></i></a>
                                    <a href="<?=base_url()?>billboards/reject/<?=$l->level?>?code=<?=$billboard[0]->code?>" class="btn btn-warning btn-sm" onclick="return confirm('Are you sure you want to reject this billboard?')"><i class="fa fa-times"></i></a>
                                <?php } ?>
                              </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <br>
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Data Pemohon</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Nama</th><td width="80%"><?=$applicant[0]->name?></td>
                                </tr>
                                <tr>
                                    <th>Alamat</th><td><?=$applicant[0]->address?></td>
                                </tr>
                                <tr>
                                    <th>No HP</th><td><?=$applicant[0]->phone_number?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Jenis Reklame</th><td width="80%"><?php $this->builder->getNameByCond('categories',['code'=>$billboard[0]->category])?></td>
                                </tr>
                                <tr>
                                    <th>Tipe Reklame</th><td><?php $this->builder->getNameByCond('billboard_types',['code'=>$billboard[0]->billboard_type])?></td>
                                </tr>
                                <tr>
                                    <th>Text Reklame</th><td><?=$billboard[0]->billboard_text?></td>
                                </tr>
                                <tr>
                                    <th>Sudut pandang</th><td><?php $this->builder->getNameByCond('view_points',['code'=>$billboard[0]->view_point])?></td>
                                </tr>
                                <tr>
                                    <th>Ukuran</th>
                                    <td>
                                        Panjang : <?=$billboard[0]->length?>m, Lebar : <?=$billboard[0]->width?>m, Luas : <?=$billboard[0]->size?>m<sup>2</sup>, Tinggi Media : <?=$billboard[0]->height?>m
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Informasi Lokasi Pemasangan</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Kelas Jalan</th><td width="80%"><?php $this->builder->getNameByCond('street_class',['code'=>$billboard[0]->street_class])?></td>
                                </tr>
                                <tr>
                                    <th>Kota</th><td><?php $this->builder->getNameByCond('city',['code'=>$billboard[0]->city_code])?></td>
                                </tr>
                                <tr>
                                    <th>Kecamatan</th><td><?php $this->builder->getNameByCond('districts',['id'=>$billboard[0]->district_id])?></td>
                                </tr>
                                <tr>
                                    <th>Jalan</th><td><?php $this->builder->getNameByCond('streets',['id'=>$billboard[0]->street_id])?></td>
                                </tr>
                                <tr>
                                    <th>Alamat Lengkap</th><td><?=$billboard[0]->address?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>