<style>
               
  #map { 
    height: 400px;    
    width: 100%;            
  }  
</style>
<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <form method="POST" enctype="multipart/form-data" action="">
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Data Reklame Ilegal</h3>
                    </div>
                    <div class="card-body">
                        <?php if(isset($illegal) && $illegal[0]->picture!=''){ ?>
                        <center>
                            <img src="<?=base_url()?>assets/image-upload/illegal-billboards/<?=$illegal[0]->picture?>" width="500px">
                        </center>
                        <?php } ?>
                        <div class="row mt-3">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Tipe Reklame</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="billboard_type" id="city_id" data-toggle="select">
                                    <option selected hidden></option>
                                        <?php foreach($types as $t){ ?>
                                        <option <?=(isset($illegal) && $t->code==$illegal[0]->billboard_type)?'selected':''?> value="<?=$t->code?>"><?=$t->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Text Reklame</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" name="billboard_text" value="<?=(isset($illegal))?$illegal[0]->billboard_text:''?>" class="form-control form-control-small" id="billboard_text" placeholder="Text Reklame">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Kota</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="city_code" id="city_id" data-toggle="select" onchange="changeCity(this.value)">
                                        <?php foreach($city as $c){ ?>
                                        <option <?=(isset($illegal) && $c->code==$illegal[0]->city_code)?'selected':''?> value="<?=$c->code?>"><?=$c->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Kecamatan</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="district_id" id="district" data-toggle="select" onchange="changeDistrict(this.value)">
                                        <?php foreach($districts as $d){ ?>
                                        <option <?=(isset($illegal) && $d->id==$illegal[0]->district_id)?'selected':''?> value="<?=$d->id?>"><?=$d->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Jalan</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" name="street_id" id="street" data-toggle="select" onchange="codeAddress(this)">
                                        <?php foreach($streets as $s){ ?>
                                        <option <?=(isset($illegal) && $s->id==$illegal[0]->street_id)?'selected':''?> value="<?=$s->id?>"><?=$s->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div id="map"></div>
                            </div>
                        </div>
                        
                        <div class="row mt-3">
                            <div class="col-md-2">
                                <label class="control-label">Latitude, Longitude</label>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="latitude_longitude" value="<?=(isset($illegal))?$illegal[0]->latitude.', '.$illegal[0]->longitude:''?>" id="latlong" required="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Upload Foto Baru</label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="file" name="picture" class="form-control form-control-small" id="picture" placeholder="Foto Reklame">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button href="<?=base_url()?>illegals" class="btn btn-warning">Batal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script>
    function changeCity(val){
        $.get("<?=base_url()?>lists/districtByCity/"+val, function(res){
            res=JSON.parse(res)
            $("#district").empty();
            $("#district").append(new Option('',''));
            res.forEach(function(el){
                $("#district").append(new Option(el.name,el.id));
            })
        });
    }
    
    function changeDistrict(val){
        $.get("<?=base_url()?>lists/streetListByDistrict/"+val, function(res){
            res=JSON.parse(res)
            $("#street").empty();
            $("#street").append(new Option('',''));
            res.forEach(function(el){
                $("#street").append(new Option(el.name,el.id));
            })
        });
    }
</script>
<script type="text/javascript">
    var geocoder;
    var map;
    var markers=[];
    function initMaps() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(<?=$illegal[0]->latitude.', '.$illegal[0]->longitude?>),
          zoom: 17,
          disableDoubleClickZoom: true,
        });
        
        // Update lat/long value of div when anywhere in the map is clicked    
        google.maps.event.addListener(map,'click',function(event) {                
            document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
            setMarkers(map,event.latLng.lat(),event.latLng.lng())
        });   
        
        // Create new marker on double click event on the map
        google.maps.event.addListener(map,'dblclick',function(event) {
            var marker = new google.maps.Marker({
              position: event.latLng, 
              map: map, 
              title: event.latLng.lat()+', '+event.latLng.lng()
            });
            
            // Update lat/long value of div when the marker is clicked
            marker.addListener('click', function() {
              document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
            });            
        });
        setMarkers(map,<?=$illegal[0]->latitude?>,<?=$illegal[0]->longitude?>);
    }
    
    function setMarkers(map,lat,long){
        var markerIcon = {
            url: '<?=base_url()?>assets/img/icons/location/location-expired.png',
            scaledSize: new google.maps.Size(25, 25),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(32,65),
            labelOrigin: new google.maps.Point(40,33)
        };
        
        var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: {lat: lat, lng: long},
            map: map,
            icon: markerIcon,
            id: <?=$illegal[0]->id?>
        });
    }
    function codeAddress(selected) {
        var address = selected.options[selected.selectedIndex].text
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            document.getElementById('latlong').value = results[0].geometry.location.lat()+','+results[0].geometry.location.lng();
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: results[0].geometry.location
            });
          } else {
            alert("Geocode was not successful for the following reason: " + status);
          }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmOv0twOwgE0VlvmWnmtb1B8f1KYnbR9E&callback=initMaps" async defer></script>