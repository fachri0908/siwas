<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Informasi Permohonan</h3>
                        <?php $this->button->back('applicants/billboard')?>
                    </div>
                    <?php if($billboard[0]->approval_status==1){ ?>
                    <center>
                        <br>
                        <img src="<?=base_url()?>assets/image-upload/qr/<?=$billboard[0]->code?>.png">
                    </center>
                    <?php } ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                                    
                                    <a data-toggle="tab" role="tab" href="#status-permohonan" class="btn btn-secondary active">Status Permohonan</a>
                                    <a data-toggle="tab" role="tab"  href="#applicant-tab" class="btn btn-secondary">Pemohon</a>
                                    <a data-toggle="tab" role="tab" href="#billboard-tab" class="btn btn-secondary">Reklame</a>
                                    <a data-toggle="tab" role="tab" href="#location-tab" class="btn btn-secondary">Lokasi</a>
                                    <a data-toggle="tab" role="tab" href="#attachment-tab" class="btn btn-secondary">Persyaratan</a>
                                    <a data-toggle="tab" role="tab" href="#rate-tab" class="btn btn-secondary">Tarif</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-content" id="pills-tabContent">
                            
                            
                            <div class="tab-pane fade show active" id="status-permohonan" role="tabpanel">
                                        
                                <div class="timeline timeline-one-side" data-timeline-content="axis" data-timeline-axis-style="dashed">
                                    <?php $x=1; foreach($levels as $l){ ?>
                                    <?php $appr=$this->builder->getRecordByCond('approvals',['billboard_code'=>$billboard[0]->code,'level'=>$l->level]);
                                        $icon='history';
                                        $badge='secondary';
                                        $status='Menunggu Pengecekan';
                                        $receive='Menunggu Pengecekan';
                                        $approve='Menunggu Pengecekan';
                                        $text='dark';
                                        $remark='';
                                        if(count($appr)>0){
                                            $remark=$appr[0]->remark;
                                            if($appr[0]->approve_status==0){
                                                $badge='secondary';
                                                $icon='history';
                                                $status='Menunggu Pengecekan';
                                                $text='dark';
                                            }else if($appr[0]->approve_status==1){
                                                $badge='success';
                                                $icon='check';
                                                $status='Disetujui';
                                                $text='success';
                                            }else{
                                                $badge='danger';
                                                $icon='times';
                                                $status='Ditolak';
                                                $text='danger';
                                            }
                                            $receive=date('d F Y', strtotime($appr[0]->receive_at));
                                            if($appr[0]->approved_at != NULL){
                                                $approve=date('d F Y', strtotime($appr[0]->approved_at));
                                            }else{
                                                $approve='Menunggu Pengecekan';
                                            }
                                        }
                                    ?>
                                    
                                    <div class="timeline-block">
                                      <span class="timeline-step badge-<?=$badge?>">
                                        <i class="fa fa-<?=$icon?>"></i>
                                      </span>
                                      <div class="timeline-content">
                                        <div class="d-flex justify-content-between pt-1">
                                          <div>
                                            <span class="text-<?=$text?> text-sm font-weight-bold"><?=$l->name?> (<?=$approve?>)</span>
                                          </div>
                                          <div class="text-right">
                                            <small class="text-muted"><i class="fas fa-clock-o"></i><?=$receive?></small>
                                          </div>
                                        </div>
                                        <h6 class="text-sm mt-1 mb-2"><?=$status?></h6>
                                      <small><?=$remark?></small>
                                      </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                
                            </div>
                            
                            <div class="tab-pane fade" id="applicant-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Nama</th><td width="80%"><?=$applicant[0]->name?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th><td><?=$applicant[0]->address?></td>
                                        </tr>
                                        <tr>
                                            <th>No HP</th><td><?=$applicant[0]->phone_number?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="billboard-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Jenis Reklame</th><td width="80%"><?php $this->builder->getNameByCond('categories',['code'=>$billboard[0]->category])?></td>
                                        </tr>
                                        <tr>
                                            <th>Tipe Reklame</th><td><?php $this->builder->getNameByCond('billboard_types',['code'=>$billboard[0]->billboard_type])?></td>
                                        </tr>
                                        <tr>
                                            <th>Text Reklame</th><td><?=$billboard[0]->billboard_text?></td>
                                        </tr>
                                        <tr>
                                            <th>Sudut pandang</th><td><?php $this->builder->getNameByCond('view_points',['code'=>$billboard[0]->view_point])?></td>
                                        </tr>
                                        <tr>
                                            <th>Ukuran</th>
                                            <td>
                                                Panjang : <?=$billboard[0]->length?>m, Lebar : <?=$billboard[0]->width?>m, Luas : <?=$billboard[0]->size?>m<sup>2</sup>, Tinggi Media : <?=$billboard[0]->height?>m
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="location-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Kelas Jalan</th><td width="80%"><?php $this->builder->getNameByCond('street_class',['code'=>$billboard[0]->street_class])?></td>
                                        </tr>
                                        <tr>
                                            <th>Kota</th><td><?php $this->builder->getNameByCond('city',['code'=>$billboard[0]->city_code])?></td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th><td><?php $this->builder->getNameByCond('districts',['id'=>$billboard[0]->district_id])?></td>
                                        </tr>
                                        <tr>
                                            <th>Jalan</th><td><?php $this->builder->getNameByCond('streets',['id'=>$billboard[0]->street_id])?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat Lengkap</th><td><?=$billboard[0]->address?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="attachment-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <?php foreach($attachments as $a){ ?>
                                        <tr>
                                            <td>
                                                <?=$a->atc_name?><br>
                                                <?php if($a->file!=NULL){ ?>
                                                    <img width="700px" src="<?=base_url()?>assets/image-upload/registration_attachments/<?=$a->file?>">
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="rate-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>NSR</th><td width="80%"><?=number_format($billboard[0]->nsr)?></td>
                                        </tr>
                                        <tr>
                                            <th>Pajak</th><td><?=number_format($billboard[0]->tax)?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Setujui permohonan</h5>
          </div>
       <form method="post" id="approve-form">
          <div class="modal-body">
            <textarea name="approve-remark" class="form-control" placeholder="Input Penjelasan (boleh dikosongkan)"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
