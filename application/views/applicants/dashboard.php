
<div class="container-fluid mt--6">
    
    
<!--new disgn-->
    <div class="row">
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>applicants/billboard">Draft Reklame</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$is_drat?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                    <i class="ni ni-fat-add"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 12%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>applicants/billboard"> Reklame Diajukan </a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$diajukan?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-delivery-fast"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-info mr-2"><i class="fa fa-equals"></i> 23% </span>
                <span class="text-nowrap">Dari Bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>applicants/billboard">Reklame Disetujui</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$disetujui?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-check-bold"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 20%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>applicants/billboard">Reklame Ditolak</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$ditolak?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-button-power"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> 11%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
    </div>
    
<!--end new design-->





<!--start row peta dan pemberitahuan-->
<div class="row">
        
        
    <!--KIRI-->
    <div class="col-xl-9">
        <div class="card">
            <div class="card-body p-3">
                <div id="formap"></div>
            </div>
        </div>
    </div>
        
        
    <!--KANAN-->
    <div class="col-xl-3">
    
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>applicants/billboard"> Masa Tenggang </a></h5>
                  <span class="h2 font-weight-bold mb-0"> <?=$soonexpired?> </span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-time-alarm"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                <span class="text-nowrap">Since last month</span>
              </p>
            </div>
        </div>  
                  
<?php
function limit_words($string, $word_limit){
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}
?>
                  
        <div class="card bg-gradient-default">
          <div class="card-body p-2">
             <div id="b" class="carousel slide pt-2" data-ride="carousel">
                 <h4 style="position: absolute; color: white; z-index: 2; margin: -5px 5px 2px;"> PEMBERITAHUAN </h4>
                <div class="carousel-inner">
                    <?php $no = 0; foreach($announcement as $sc) { ?>
                        <div class="carousel-item <?=($no == 0) ? 'active':''?>" data-interval="5000">
                            <div class="card bg-default">
                                <div class="card-body">
                                    <div class="mb-2">
                                      <div class="text-light mt-2 text-sm"><a href="#" onclick="showmodalannouncement('<?=$sc->announcement_id?>')"><b><?=$sc->announcement_title?></b></a></div>
                                      <p class="text-light"><small>Dibuat tanggal : <?=date('Y-m-d', strtotime($sc->created_at))?></small></p>
                                    </div>
                                    <div class="row">
                                      <div class="col">
                                          <small class="text-light"><?=(strlen($sc->announcement_content) > 100) ? limit_words($sc->announcement_content, 12) . ' . . .' : $sc->announcement_content?></small>
                                      </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    <?php $no++; } ?>
                </div>
                  <a class="carousel-control-prev" href="#b" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon mb-2" aria-hidden="true"  style="position: absolute; bottom: 0;"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#b" role="button" data-slide="next">
                    <span class="carousel-control-next-icon mb-2" aria-hidden="true"  style="position: absolute; bottom: 0;"></span>
                    <span class="sr-only">Next</span>
                  </a>
                  <ol class="carousel-indicators">
                    <?php $no = 0; foreach($announcement as $sc) { ?>
                        <li data-target="#b" data-slide-to="<?=$no?>" class="<?=($no == 0) ? 'active':''?>"></li>
                    <?php $no++; } ?>
                  </ol>
            </div>
          </div>
        </div>

    </div>
        
</div>
<!--end row-->

</div>






<div class="modal" id="modalannouncement">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"> PEMBERITAHUAN </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 mb-2" for="title">
                    <h2 class="text-center"> </h2>
                    <p class="text-center mt-n2 text-dark" style="font-size: 15px"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" for="content">
                    <p></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>


<div class="modal" id="modalmap">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header pb-2">
            <h4 class="modal-title"> Detail Reklame </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body pt-2 pb-1">
            <div class="row">
                <div class="col-md-12" for="map-content"></div>
            </div>
        </div>
        
    </div>
  </div>
</div>




<style>    
    #formap { 
        height: 555px; width: 100%;
    }
</style>


<script type="text/javascript">


    function showmodalannouncement(e) {
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>applicants/announcement_detail",
            data : {
                'id' : e,  
            },
            dataType: 'json',
            success: function(data){
                var haris = ['Minggu', 'Senin','Selasa','Rabu','Kamis','Jum&#39;at','Sabtu'];
                var bulans = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                var dd = new Date(data.created_at).getDate();
                var mm = new Date(data.created_at).getMonth();
                var yy = new Date(data.created_at).getYear();
                var hari = new Date(data.created_at).getDay();
                var tahun = (yy < 1000) ? yy + 1900 : yy;
                $("#modalannouncement .modal-body [for='title'] > h2").html(":: " + data.announcement_title + " ::");
                $("#modalannouncement .modal-body [for='title'] > p").html("Dibuat pada : " + haris[hari]+', '+dd+' '+bulans[mm]+' '+tahun);
                $("#modalannouncement .modal-body [for='content']").html(data.announcement_content);
                $("#modalannouncement").modal("show");
            }
        });
    }


</script>

<script type="text/javascript">
    function initMaps() {
        var map = new google.maps.Map(document.getElementById('formap'), {
            zoom: 11,
            center: new google.maps.LatLng(-6.1822265, 106.8014451),
        });
        setMarkers(map);
    }
    function setMarkers(map) {
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>applicants/points",
            dataType: 'json',
            success: function(data){
                let result = JSON.stringify(data);
                let rs = $.parseJSON(result);
                Object.keys(rs).forEach(function(key) {
                    let id              = rs[key].id;
                    let code            = rs[key].code;
                    let lat             = parseFloat(rs[key].latitude);
                    let long            = parseFloat(rs[key].longitude);
                    let status          = rs[key].approval_status;
                    let image           = '';
                    console.log(rs);
                        if(status == 0){
                            image = '<?=base_url()?>assets/img/icons/location/location-waiting.png';
                        } else if(status == 1){
                            image = '<?=base_url()?>assets/img/icons/location/location-active.png';
                        } else {
                            image = '<?=base_url()?>assets/img/icons/location/location-expired.png';
                        }
                    var markerIcon = {
                        url: image,
                        scaledSize: new google.maps.Size(25, 25),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(32,65),
                        labelOrigin: new google.maps.Point(40,33)
                    };
                    var marker = new google.maps.Marker({
                        // animation: google.maps.Animation.DROP,
                        position: {lat: lat, lng: long},
                        map: map,
                        icon: markerIcon,
                        id: id,
                        code: code
                    });
                    marker.addListener('click', function() {
                        // window.location.replace("<?=base_url()?>applicants/billboarddetail/"+marker.code);
                        $.ajax({
                            type: "POST",
                            url: "<?=base_url()?>applicants/detail_map_reklame",
                            data : {
                                'id'    : marker.id, 
                                'code'  : marker.code,  
                            },
                            dataType: 'html',
                            success: function(data){
                                $(".modal-body [for='map-content']").html(data);
                                $("#modalmap").modal("show");
                            }
                        });
                    
                    });
                })
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmOv0twOwgE0VlvmWnmtb1B8f1KYnbR9E&callback=initMaps" async defer></script>
