<div class="container-fluid mt--6">
    <?=$this->session->flashdata('flash')?>
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <form method="POST" id="registration_form" action="" enctype="multipart/form-data" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Ganti Password</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Password Lama</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="password" name="old_password" class="form-control form-control-small" id="old_password" placeholder="Password lama" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Password Baru</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="password" name="new_password" class="form-control form-control-small" id="old_password" placeholder="Password Baru" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Konfirmasi Password Baru</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="password" name="new_password_confirm" class="form-control form-control-small" id="new_password_confirm" placeholder="Konfirmasi Password Baru" required>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="saveandupload" >Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6">
                <div class="copyright text-center text-lg-left text-muted">
                    &copy; 2019 <a href="https://www.creative-tim.com/" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
                </div> 
            </div>
            <div class="col-lg-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com/" class="nav-link" target="_blank">Creative Tim</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="http://blog.creative-tim.com/" class="nav-link" target="_blank">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com/license" class="nav-link" target="_blank">License</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<script>
    function showPassword(){
        let atr=$('#password').attr('type');
        if(atr=='text'){
            $('#password').attr('type','password')
        }else{
            $('#password').attr('type','text')    
        }
        
    }
</script>