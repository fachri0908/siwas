<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <form method="POST" id="registration_form" action="" enctype="multipart/form-data" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Edit Profile</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Email</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="email" value="<?=$this->session->userdata('user_email')?>" class="form-control form-control-small" id="" placeholder="Email" disabled required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Nama</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="name" value="<?=$applicants[0]->name?>" class="form-control form-control-small" id="" placeholder="Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Alamat</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="address" value="<?=$applicants[0]->address?>" class="form-control form-control-small" id="" placeholder="Alamat" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Hp</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="phone_number" value="<?=$applicants[0]->phone_number?>" class="form-control form-control-small" id="" placeholder="HP" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">KTP</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="file" name="ktp" class="form-control form-control-small" id="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">NPWP</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="file" name="npwp" class="form-control form-control-small" id="">
                                </div>
                            </div>
                        </div>
                        
                        <!--<div class="row">-->
                        <!--    <div class="col-md-1">-->
                        <!--        <label class="form-control-label" for="exampleFormControlInput1">KTP</label>  <input type="hidden" name="ktp" id="ktpfile">-->
                        <!--    </div>-->
                        <!--    <div class="col-md-8">-->
                        <!--        <div class="form-group">-->
                            
                        <!--            <div class="dropzone dropzone-single mb-3" id="ktpfile" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration/create_dev/ktp" data-form="registration_form" data-button-submit="saveandupload">-->
                        <!--                <div class="fallback">-->
                        <!--                    <div class="custom-file">-->
                        <!--                        <input type="file" name="ktp" class="custom-file-input" id="projectCoverUploads">-->
                        <!--                        <label class="custom-file-label" for="projectCoverUploads">Choose file</label>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div class="dz-preview dz-preview-single">-->
                        <!--                    <div class="dz-preview-cover">-->
                        <!--                    <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <!--<div class="row">-->
                        <!--    <div class="col-md-1">-->
                        <!--        <label class="form-control-label" for="">NPWP</label> <input type="hidden" name="npwp" id="npwpfile">-->
                        <!--    </div>-->
                        <!--    <div class="col-md-8">-->
                        <!--        <div class="form-group">-->
                        <!--            <div class="dropzone dropzone-single mb-3" id="npwpfile" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration/create_dev/npwp" data-form="registration_form" data-button-submit="saveandupload">-->
                        <!--                <div class="fallback">-->
                                            
                        <!--                    <div class="custom-file">-->
                        <!--                        <label class="custom-file-label" for="npwpfile">Choose file</label>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--                <div class="dz-preview dz-preview-single">-->
                        <!--                    <div class="dz-preview-cover">-->
                        <!--                    <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>-->
                        <!--                    </div>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <hr>
                        <h2>Informasi Perusahaan</h2>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Nama Perusahaan</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="company_name" value="<?=$applicants[0]->company_name?>" class="form-control form-control-small" id="" placeholder="Nama Perusahaan" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Jabatan</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="company_position" value="<?=$applicants[0]->company_position?>" class="form-control form-control-small" id="" placeholder="Jabatan" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Alamat Perusahaan</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="company_address" value="<?=$applicants[0]->company_address?>" class="form-control form-control-small" id="" placeholder="Alamat Perusahaan" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">No Telp Perusahaan</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="company_phone" value="<?=$applicants[0]->company_phone?>" class="form-control form-control-small" id="" placeholder="Telp Perusahaan" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Fax Perusahaan</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="company_fax" value="<?=$applicants[0]->company_fax?>" class="form-control form-control-small" id="" placeholder="Fax Perusahaan" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">NPWPD</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="company_npwpd" value="<?=$applicants[0]->company_fax?>" class="form-control form-control-small" id="" placeholder="NPWPD Perusahaan" required>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="saveandupload">Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
