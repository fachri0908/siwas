<div class="container-fluid mt--6">
    
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    
    
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Reklame</h3>
                        <a class="btn btn-primary float-right btn-sm" href="<?=base_url()?>registration">Permohonan Baru</a>
                    </div>
                    
                    <div class="row mt-2 ml-2">
                        <div class="col-md-12">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                                <a data-toggle="tab" role="tab"  href="#draft-tab" class="btn btn-secondary active">Draft</a>
                                <a data-toggle="tab" role="tab" href="#waiting-tab" class="btn btn-secondary">Menunggu Pengecekan</a>
                                <a data-toggle="tab" role="tab" href="#approve-tab" class="btn btn-secondary">Diterima</a>
                                <a data-toggle="tab" role="tab" href="#rejected-tab" class="btn btn-secondary">Ditolak</a>
                                <!--<a data-toggle="tab" role="tab" href="#expired-tab" class="btn btn-secondary"> Masa Tenggang </a>-->
                            </div>
                        </div>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="draft-tab" role="tabpanel">
                            <div class="table-responsive py-4">
                              <table class="table table-flush" id="datatable-nobuttons">
                                <thead class="thead-light">
                                  <tr>
                                    <th>No</th>
                                    <th>Kode Reklame</th>
                                    <th>Tipe Reklame</th>
                                    <th>Teks reklame</th>
                                    <th>Persyaratan</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1; foreach($billboards as $b){
                                        if($b->is_draft==1){ ?>
                                      <tr>
                                        <td width="5%"><?=$x++?></td>
                                        <td><?=$b->code?></td>
                                        <td><?php $this->builder->getNameByCond('billboard_types',['code'=>$b->billboard_type])?></td>
                                        <td><?=$b->billboard_text?></td>
                                        <td>
                                            <?php $p=$this->builder->checkCompletenes($b->code); ?>
                                            <small class="text-center"> Persyaratan <?=$p->uploaded?> / <?=$p->total?> (<?=number_format($p->uploaded/$p->total*100)?>%)</small>
                                            <div class="progress progress-xs my-2 bg-dark">
                                              <div class="progress-bar bg-success" style="width: <?=number_format($p->uploaded/$p->total*100)?>%"></div>
                                            </div>
                                        </td>
                                        <td width="10%">
                                            <a class="btn btn-success btn-sm" href="<?=base_url()?>registration/attachment/<?=$b->code?>"><i class="fa fa-upload"></i> Upload Persyaratan</a>
                                            <a class="btn btn-primary btn-sm" href="<?=base_url()?>registration/editregistration/<?=$b->code?>"><i class="fa fa-pencil"></i> Edit</a>
                                            <?php
                                                $pem = ($b->uploaded_atc == 0) ? 1 : $b->uploaded_atc;
                                                $at =  $pem / $b->total_atc;
                                            ?>
                                            <a onclick="return confirm('apakah anda yakin untuk mengirim permohonan ini..?(pastikan persyaratan telah dilengkapi dengan benar)')" class="btn btn-primary btn-sm <?=($at < 1)?'disabled':''?>" href="<?=base_url()?>registration/sendregistration/<?=$b->code?>"><i class="fa fa-paper-plane-o"></i> Kirim Permohonan </a>
                                            
                                        </td>
                                      </tr>
                                  <?php } } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <!--waiting-->
                        <div class="tab-pane fade" id="waiting-tab" role="tabpanel">
                            <div class="table-responsive py-4">
                              <table class="table table-flush" id="datatable-nobuttons">
                                <thead class="thead-light">
                                  <tr>
                                    <th>No</th>
                                    <th>Kode Reklame</th>
                                    <th>Tipe Reklame</th>
                                    <th>Teks reklame</th>
                                    <th>Persyaratan</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1; foreach($billboards as $b){
                                        if($b->is_draft==0 && $b->approval_status==0){ ?>
                                      <tr>
                                        <td width="5%"><?=$x++?></td>
                                        <td><?=$b->code?></td>
                                        <td><?php $this->builder->getNameByCond('billboard_types',['code'=>$b->billboard_type])?></td>
                                        <td><?=$b->billboard_text?></td>
                                        <td>
                                            <?php $p=$this->builder->checkCompletenes($b->code); ?>
                                            <small class="text-center"> Persyaratan <?=$p->uploaded?> / <?=$p->total?> (<?=number_format($p->uploaded/$p->total*100)?>%)</small>
                                            <div class="progress progress-xs my-2 bg-dark">
                                              <div class="progress-bar bg-success" style="width: <?=number_format($p->uploaded/$p->total*100)?>%"></div>
                                            </div>
                                        </td>
                                        <td width="10%">
                                            <a class="btn btn-primary btn-sm" href="<?=base_url()?>applicants/billboarddetail/<?=$b->code?>"><i class="fa fa-eye"></i> Detail</a>
                                        </td>
                                      </tr>
                                  <?php } } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        
                        <!--diterima-->
                        <div class="tab-pane fade" id="approve-tab" role="tabpanel">
                            <div class="table-responsive py-4">
                              <table class="table table-flush" id="datatable-nobuttons">
                                <thead class="thead-light">
                                  <tr>
                                    <th>No</th>
                                    <th>Kode Reklame</th>
                                    <th>Tipe Reklame</th>
                                    <th>Teks reklame</th>
                                    <th>Qr</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1; foreach($billboards as $b){
                                        if($b->is_draft==0 && $b->approval_status==1){ ?>
                                      <tr>
                                        <td width="5%"><?=$x++?></td>
                                        <td><?=$b->code?></td>
                                        <td><?php $this->builder->getNameByCond('billboard_types',['code'=>$b->billboard_type])?></td>
                                        <td><?=$b->billboard_text?></td>
                                        <td>
                                            <img src="<?=base_url()?>assets/image-upload/qr/<?=$b->code?>.png">
                                        </td>
                                        <td width="10%">
                                            <a class="btn btn-primary btn-sm" href="<?=base_url()?>applicants/billboarddetail/<?=$b->code?>"><i class="fa fa-eye"></i> Detail</a>
                                            <!--<a class="btn btn-success btn-sm" href="<?=base_url()?>extensions/request/<?=$b->code?>"><i class="fa fa-handshake"></i> Ajukan Perpanjangan</a>-->
                                            <a onclick="return confirm('pastikan reklame telah terpasang sebelum melanjutkan, klik OK untuk melanjutkan!!!')" class="btn btn-warning btn-sm" href="<?=base_url()?>applicants/installbillboard/<?=$b->code?>"><i class="fa fa-map-signs"></i> Pasang Reklame</a>
                                        </td>
                                      </tr>
                                  <?php } } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        
                        <!--ditolak-->
                        <div class="tab-pane fade" id="rejected-tab" role="tabpanel">
                            <div class="table-responsive py-4">
                              <table class="table table-flush" id="datatable-nobuttons">
                                <thead class="thead-light">
                                  <tr>
                                    <th>No</th>
                                    <th>Kode Reklame</th>
                                    <th>Tipe Reklame</th>
                                    <th>Teks reklame</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1; foreach($billboards as $b){
                                        if($b->is_draft==0 && $b->approval_status==2){ ?>
                                      <tr>
                                        <td width="5%"><?=$x++?></td>
                                        <td><?=$b->code?></td>
                                        <td><?php $this->builder->getNameByCond('billboard_types',['code'=>$b->billboard_type])?></td>
                                        <td><?=$b->billboard_text?></td>
                                        <td>
                                            
                                            <?php $status=$this->builder->raw("select approvals.*, positions.name as position_name from approvals, approval_levels, positions where approvals.level=approval_levels.level and approval_levels.approver_position_code=positions.code and approvals.approve_status=2 and approvals.billboard_code='$b->code'");
                                                    echo "Ditolak Oleh : ".$status[0]->position_name."<br>Pada : ".date('d F Y',strtotime($status[0]->approved_at))."<br>Keterangan : ".$status[0]->remark;
                                            ?>
                                            
                                        </td>
                                        <td width="10%">
                                            <a class="btn btn-primary btn-sm" href="<?=base_url()?>applicants/billboarddetail/<?=$b->code?>"><i class="fa fa-eye"></i> Detail</a>
                                        </td>
                                      </tr>
                                  <?php } } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        
                        <!--masa tenggang-->
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
