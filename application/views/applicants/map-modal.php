
<div class="row">
    <div class="col-md-6" style="padding: 0px 30px">
        <h2> Data Reklame </h2>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Nomor Formulir
            </div>
            <div class="col-md-7">
                : <?=$bb->code?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Kategori
            </div>
            <div class="col-md-7">
                : <?=$bb->category_name?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
               Jenis Reklame
            </div>
            <div class="col-md-7">
                : <?=$bb->billboard_type_name?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Sudut Pandang
            </div>
            <div class="col-md-7">
                : <?=$bb->view_point_name?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Lokasi
            </div>
            <div class="col-md-7">
                : <?=$bb->street_name?>, <?=$bb->district_name?> - <?=$bb->city_name?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Area Pemasangan
            </div>
            <div class="col-md-7">
                : <?=$bb->street_class_name?>
            </div>
        </div>
        
        <h4 class="mt-4"> Keterangan Reklame </h4>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Tanggal Pemasangan
            </div>
            <div class="col-md-7">
                : <?=$bb->install_date?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Tanggal Selesai
            </div>
            <div class="col-md-7">
                : <?=$bb->finish_date?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Total Hari
            </div>
            <div class="col-md-7">
                : <?=$bb->install_duration?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Total Jam
            </div>
            <div class="col-md-7">
                : <?=$bb->time_duration?>
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Jumlah
            </div>
            <div class="col-md-7">
                : <?=$bb->total?>
            </div>
        </div>
    </div>
    
    
    <div class="col-md-6" style="padding: 0px 30px">
        <h2> Ukuran/Dimensi Reklame </h2>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Panjang Reklame
            </div>
            <div class="col-md-7">
                : <?=$bb->length?> M
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Lebar Reklame
            </div>
            <div class="col-md-7">
                : <?=$bb->width?> M
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Tinggi Reklame
            </div>
            <div class="col-md-7">
                : <?=$bb->height?> M
            </div>
        </div>
        <div class="row border border-left-0 border-top-0 border-right-0 mt-3 pb-2">
            <div class="col-md-5">
                Luas Reklame
            </div>
            <div class="col-md-7">
                : <?=$bb->size?> M<sup>2</sup>
            </div>
        </div>
        
        
        
        <h2 class="mt-4"> Desain Reklame </h2>
        <div class="card bg-gradient-default" style="height: 370px">
          <div class="card-body p-2">
             <div id="bb" class="carousel slide pt-2" data-ride="carousel" style="height: 100%">
                <div class="carousel-inner" style="height: 100%">
                    
                    <?php if($desain_billboard) { ?>
                        <?php $no = 0; foreach($desain_billboard as $deb) { ?>
                            <div class="carousel-item <?=($no == 0)?'active':''?>" data-interval="5000" style="height: 100%">
                                <div class="card p-2" style="height: 85%">
                                    <div class="row">
                                      <div class="col">
                                            <?php
                                                if (strpos($deb->file, 'depan') !== false) {
                                                    echo 'Desain Depan';
                                                } else if (strpos($deb->file, 'belakang') !== false) {
                                                    echo 'Desain Belakang';
                                                } else if (strpos($deb->file, 'kanan') !== false) {
                                                    echo 'Desain Kanan';
                                                } else if (strpos($deb->file, 'kiri') !== false) {
                                                    echo 'Desain Kiri';
                                                }
                                            ?>
                                            <img src="<?=base_url()?>assets/img/billboard-frame.png" width="480px" height="260px" style="margin: 0px 0px 0px -4px">
                                            <img src="<?=base_url()?>assets/image-upload/registration_attachments/<?=$deb->file?>"
                                                style="position: absolute; z-index: 9999; top: 45px; left: 47px; height: 153px; width: 406px">
                                      </div>
                                    </div>
                                </div>
                            </div>
                        <?php $no++; } ?>
                    <?php } ?>
                            
                    <?php if(!$desain_billboard) { ?>
                        <div class="carousel-item active" data-interval="5000" style="height: 100%">
                            <div class="card p-2" style="height: 85%">
                                <div class="row">
                                  <div class="col"></div>
                                    <h1> No Image </h1>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                            
                    
                </div>
                
                <a class="carousel-control-prev" href="#bb" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon mb-2" aria-hidden="true"  style="position: absolute; bottom: 0;"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#bb" role="button" data-slide="next">
                    <span class="carousel-control-next-icon mb-2" aria-hidden="true"  style="position: absolute; bottom: 0;"></span>
                    <span class="sr-only">Next</span>
                </a>
                
                <ol class="carousel-indicators">
                    
                    <?php if($desain_billboard) { ?>
                        <?php $no = 0; foreach($desain_billboard as $deb) { ?>
                            <li data-target="#bb" data-slide-to="<?=$no?>" class="<?=($no == 0)?'active':''?>"></li>
                        <?php $no++; } ?>
                    <?php } ?>
                            
                           
                    <?php if(!$desain_billboard) { ?>
                        <li data-target="#bb" data-slide-to="0" class="active"></li>
                    <?php } ?>
                    
                </ol>
                
            </div>
          </div>
        </div>
    </div>
</div>

