<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Pemohon</h3>
                    </div>

                        <div class="table-responsive py-4">
                          <table class="table table-flush" id="datatable-buttons">
                            <thead class="thead-light">
                              <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Tanggal Mendaftar</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php $x=1; foreach($applicants as $a){ ?>
                                  <tr>
                                    <td width="5%"><?=$x++?></td>
                                    <td><?=$a->name?></td>
                                    <td><?=$a->address?></td>
                                    <td><?=date('d F Y', strtotime($a->created_at))?></td>
                                    <td>
                                        <?php if($a->validated_at==NULL){ 
                                            $active=0;
                                        }else{
                                            $active=1;
                                        }
                                        if($a->approved_at==NULL){
                                            $approved=0;
                                        }else{
                                            $approved=1;
                                        }
                                        ?>
                                        <button type="button" class="btn btn-block btn-sm btn-<?=($active==1)?'primary':'warning'?>">
                                          <?=($active==1)?'Email Aktif':'Email Belum Aktif'?>
                                        </button>
                                        <!--<button type="button" class="btn btn-<?=($approved==1)?'primary':'warning'?>">-->
                                        <!--  <?=($approved==1)?'Sudah di Verifikasi':'Belum di Verifikasi'?>-->
                                        <!--</button>-->
                                    </td>
                                  </tr>
                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
    
    <!--<footer class="footer pt-0">-->
    <!--    <div class="row align-items-center justify-content-lg-between">-->
    <!--        <div class="col-lg-6">-->
    <!--            <div class="copyright text-center text-lg-left text-muted">-->
    <!--                &copy; 2019 <a href="https://www.creative-tim.com/" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>-->
    <!--            </div> -->
    <!--        </div>-->
    <!--        <div class="col-lg-6">-->
    <!--            <ul class="nav nav-footer justify-content-center justify-content-lg-end">-->
    <!--                <li class="nav-item">-->
    <!--                    <a href="https://www.creative-tim.com/" class="nav-link" target="_blank">Creative Tim</a>-->
    <!--                </li>-->
    <!--                <li class="nav-item">-->
    <!--                    <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>-->
    <!--                </li>-->
    <!--                <li class="nav-item">-->
    <!--                    <a href="http://blog.creative-tim.com/" class="nav-link" target="_blank">Blog</a>-->
    <!--                </li>-->
    <!--                <li class="nav-item">-->
    <!--                    <a href="https://www.creative-tim.com/license" class="nav-link" target="_blank">License</a>-->
    <!--                </li>-->
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</footer>-->
    
</div>