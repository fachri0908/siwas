<div class="table-responsive py-4">
  <table class="table table-flush" id="datatable-buttons">
    <thead class="thead-light">
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>Kelas Jalan</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
        <?php $x=1; foreach($streets as $r){ ?>
          <tr>
            <td width="5%"><?=$x++?></td>
            <td><?=$r->name?></td>
            <th><?=$r->area_name?></th>
            <td width="10%">
                <button class="btn btn-primary btn-sm" onclick="editStreet('<?=$r->id?>','<?=$r->name?>','<?=$r->street_class_code?>')"><i class="fa fa-edit"></i></button>
                <a href="<?=base_url()?>setup/street?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
      <?php } ?>
    </tbody>
  </table>
</div>