<table class="table table-flush table-bordered table-hover" id="datatable-buttons">
    <thead>
        <tr>
            <th>No</th>
            <th>Jenis Reklame</th>
            <th>Teks Reklame</th>
            <th>Lokasi Reklame</th>
        </tr>
    </thead>
    <tbody>
        <?php $x=1; foreach($billboards as $b){ ?>
        <tr>
            <td width="5%"><?=$x++?></td>
            <td><?=$b->type?></td>
            <td><?=$b->billboard_text?></td>
            <td><?=$this->builder->getNameByCond('streets',['id'=>$b->street_id]);?>, 
                KECAMATAN <?=$this->builder->getNameByCond('districts',['id'=>$b->district_id]);?>, 
                <?=$this->builder->getNameByCond('city',['code'=>$b->city_code]);?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>