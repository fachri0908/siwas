<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Tagihan</h3>
                    </div>

                    <div class="table-responsive py-4">
                      <table class="table table-flush" id="datatable-buttons">
                        <thead class="thead-light">
                          <tr>
                            <th>No</th>
                            <th>Invoice</th>
                            <th>Jenis Tagihan</th>
                            <th>Kode Reklame</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $x=1; foreach($payments as $p){ ?>
                              <tr>
                                <td width="5%"><?=$x++?></td>
                                <th><?=$p->code?></th>
                                <td><?=($p->type=='RGS')?'Registrasi Reklame Baru':'Perpanjangan Reklame'?></td>
                                <td><?=$p->billboard_code?></td>
                                <td width="20%"><span class="badge badge-<?=($p->status=='PAID')?'success':'warning'?> badge-lg"><?=($p->status=='PAID')?'Sudah Bayar':'Belum Bayar / Menunggu Konfirmasi'?></span></td>
                                <td width="5%">
                                    <a class="btn btn-primary btn-sm" href="<?=base_url()?>adminpayments/detail/<?=$p->code?>">Detail</a>
                                </td>
                              </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>