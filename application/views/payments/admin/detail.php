<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Tagihan</h3>
                        <?php $this->button->back('adminpayments')?>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-flush">
                            <tr>
                                <th>Jenis Pembayaran</th><td width="75%"><?=($payment[0]->type=='RGS')?'Registrasi Baru':'Perpanjangan Reklame'?></td>
                            </tr>
                            <tr>
                                <th>Invoice</th><td width="75%"><?=$payment[0]->code?></td>
                            </tr>
                            <?php
                                if($payment[0]->status=='UNPAID'){
                                    if($payment[0]->file!='' || $payment[0]->file!=NULL){
                                        $status='<span class="badge badge-warning">Belum Bayar</span>';
                                    }else{
                                        $status='<span class="badge badge-secondary">Menunggu Verifikasi</span>';
                                    }
                                }else{
                                    $status='<span class="badge badge-success">Sudah bayar</span>';
                                }
                            ?>
                            <tr>
                                <th>Status</th><td width="75%"><?=($payment[0]->status=='PAID')?'Sudah Bayar':'Belum Bayar'?></td>
                            </tr>
                            <tr>
                                <th>Kode Reklame</th><td><a target="_blank" href="<?=base_url()?>applicants/billboarddetail/<?=$payment[0]->billboard_code?>"><?=$payment[0]->billboard_code?></a></td>
                            </tr>
                            <tr>
                                <th>NSR</th><td><?=number_format($payment[0]->nsr)?></td>
                            </tr>
                            <tr>
                                <th>Nilai Kontrak</th><td><?=number_format($payment[0]->contract_value)?></td>
                            </tr>
                            <tr>
                                <th>Pajak</th><td><?=number_format($payment[0]->tax)?></td>
                            </tr>
                            <?php if($payment[0]->file!='' && $payment[0]->file!=NULL) { ?>
                            <tr>
                                <th>Bukti Pembayaran</th><td><img src="<?=base_url()?>assets/image-upload/payments/<?=$payment[0]->file?>"></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <?php  if($payment[0]->status=='UNPAID'){
                    ?>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a onclick="return confirm('pastikan pembayaran telah diterima dan jumlah yang di berikan sesuai')" href="<?=base_url()?>adminpayments/confirm/<?=$payment[0]->code?>" class="btn btn-primary" ><i class="fa fa-upload"></i>Konfirmasi</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>