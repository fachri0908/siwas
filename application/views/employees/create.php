<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <form method="POST" id="registration_form" action="" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Tambah Pegawai</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Email</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control form-control-small" id="" placeholder="Email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Nama</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="name"  class="form-control form-control-small" id="" placeholder="Name" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Posisi</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="position_code" class="form-control">
                                        <?php foreach($positions as $p) { ?>
                                        <option value="<?=$p->code?>"><?=$p->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="">Password</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="password" class="form-control form-control-small" id="" placeholder="Password" required>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="saveandupload">Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script>
    function showPassword(){
        let atr=$('#password').attr('type');
        if(atr=='text'){
            $('#password').attr('type','password')
        }else{
            $('#password').attr('type','text')    
        }
        
    }
</script>