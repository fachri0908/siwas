<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Pegawai</h3>
                        <a class="btn btn-primary float-right btn-sm" href="<?=base_url()?>employees/create">Tambah Pegawai</a>
                    </div>

                        <div class="table-responsive py-4">
                          <table class="table table-flush" id="datatable-buttons">
                            <thead class="thead-light">
                              <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Posisi</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php $x=1; foreach($employees as $e){ ?>
                                  <tr>
                                    <td width="5%"><?=$x++?></td>
                                    <td><?=$e->name?></td>
                                    <td><?=$e->position_code?></td>
                                  </tr>
                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>