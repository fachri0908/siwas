<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Urutan Persetujuan Perpanjangan</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" id="form-approvals">
                          <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="example3cols1Input">Level</label>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" class="form-control" name="level" id="level" placeholder="Level" readonly value="<?=count($levels)+1?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="example3cols1Input">Deskripsi</label>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Deskripsi" required="">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-1">
                                <label class="form-control-label" for="example3cols1Input">Approver</label>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <select name="approver_position_code" id="approver" class="form-control">
                                    <option></option>
                                    <?php foreach($positions as $p) { ?>
                                    <option value="<?=$p->code?>"><?=$p->name?></option>
                                    <?php } ?>
                                </select>
                              </div>
                            </div>
                            
                          </div>
                          
                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
                              </div>
                            </div>
                          </div>
                      </form>
                    </div>
                        <div class="table-responsive py-4">
                          <table class="table table-flush">
                            <thead class="thead-light">
                              <tr>
                                <th>Level</th>
                                <th>Nama</th>
                                <th>Approver</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                                <?php $x=1; foreach($levels as $e){ ?>
                                  <tr>
                                    <td width="8%"><?=$e->level?></td>
                                    <td><?=$e->name?></td>
                                    <td><?=$this->builder->getNameByCond('positions',['code'=>$e->approver_position_code])?></td>
                                    <td width="10%">
                                        <button class="btn btn-primary btn-sm" onclick="edit('<?=$e->id?>','<?=$e->level?>','<?=$e->name?>','<?=$e->approver_position_code?>')"><i class="fa fa-edit"></i></button>
                                        <?php if($x!=1){?>
                                        <a class="btn btn-warning btn-sm" href="<?=base_url()?>approvals/extensionup/<?=$e->level?>" onclick="return confirm('ubah urutan approval?')"><i class="fa fa-arrow-up"></i></a>
                                        <?php } ?>
                                        <?php if($x!=count($levels)){?>
                                        <a class="btn btn-warning btn-sm" href="<?=base_url()?>approvals/extensiondown/<?=$e->level?>" onclick="return confirm('ubah urutan approval?')"><i class="fa fa-arrow-down"></i></a>
                                        <?php } ?>
                                    </td>
                                  </tr>
                              <?php $x++;} ?>
                          </table>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function edit(id,level, name, approver){
        document.getElementById('form-approvals').setAttribute('action','<?=base_url()?>approvals/updateextension/'+id);
        document.getElementById('level').value=level
        document.getElementById('name').value=name
        document.getElementById('approver').value=approver
        document.getElementById('name').focus()
    }
    function cancel(){
        document.getElementById('form-approvals').setAttribute('action','');
    }
</script>
