<div class="container-fluid mt--6">
        <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php } ?>
        
        
      <div class="card">
            <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Reklame Perpanjangan</h3>
          <!-- <p class="text-sm mb-0">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p> -->
        </div>
        <div class="row mx-2 mt-2">
            <div class="col-md-12">
                <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                    <a data-toggle="tab" role="tab"  href="#draft-tab" class="btn btn-secondary active">Draft</a>
                    <a data-toggle="tab" role="tab"  href="#pending-tab" class="btn btn-secondary">Menunggu persetujuan</a>
                    <a data-toggle="tab" role="tab" href="#approved-tab" class="btn btn-secondary">Diterima</a>
                    <a data-toggle="tab" role="tab" href="#rejected-tab" class="btn btn-secondary">Ditolak</a>
                </div>
            </div>
        </div>
        <br>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="draft-tab" role="tabpanel">
              
            <div class="table-responsive py-4">
              <table class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Kode Permohonan</th>
                    <th>Kode Reklame</th>
                    <th>Persyaratan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $x=1; foreach($extensions as $b){
                    if($b->is_draft==1){
                  ?>
                  <tr>
                    <td width="5%"><?=$x++?></td>
                    <th width="15%"><?=$b->code?></th>
                    <td><?=$b->billboard_code?></td>
                    <td>
                        <?php $p=$this->builder->checkExtensionCompletenes($b->code); ?>
                        <small class="text-center"> Persyaratan <?=$p->uploaded?> / <?=$p->total?> (<?=number_format($p->uploaded/$p->total*100)?>%)</small>
                        <div class="progress progress-xs my-2 bg-dark">
                          <div class="progress-bar bg-success" style="width: <?=number_format($p->uploaded/$p->total*100)?>%"></div>
                        </div>
                    </td>
                    <td width="20%">
                        <a href="<?=base_url()?>extensions/edit/<?=$b->code?>" class="btn btn-success btn-sm"><i class="fa fa-wrench"></i> Edit</a>
                        <a href="<?=base_url()?>extensions/attachment/<?=$b->code?>" class="btn btn-primary btn-sm"><i class="fa fa-upload"></i> Upload persyaratan</a>
                        <?php if($p->total==$p->uploaded){ ?>
                        <a href="<?=base_url()?>extensions/submit/<?=$b->code?>" class="btn btn-secondary btn-sm"><i class="fa fa-paper-plane"></i> Kirim Permohonan</a>
                        <?php } ?>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>
              </table>
            </div>
              
          </div>
          <div class="tab-pane fade" id="pending-tab" role="tabpanel">
              
            <div class="table-responsive py-4">
              <table class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Kode Permohonan</th>
                    <th>Kode Reklame</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $x=1; foreach($extensions as $b){
                    if($b->approval_status==0 && $b->is_draft==0){
                  ?>
                  <tr>
                    <td width="5%"><?=$x++?></td>
                    <th width="15%"><?=$b->code?></th>
                    <td><?=$b->billboard_code?></td>
                    <td>
                        menunggu persetujuan
                    </td>
                    <td width="10%">
                        <a href="<?=base_url()?>extensions/detail/<?=$b->code?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Detail</a>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>
              </table>
            </div>
              
          </div>
          <div class="tab-pane fade" id="approved-tab" role="tabpanel">
              
            <div class="table-responsive py-4">
              <table class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Kode Permohonan</th>
                    <th>Kode Reklame</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $x=1; foreach($extensions as $b){
                    if($b->approval_status==1){
                  ?>
                  <tr>
                    <td width="5%"><?=$x++?></td>
                    <th width="15%"><?=$b->code?></th>
                    <td><?=$b->billboard_code?></td>
                    <td width="10%">
                        <a href="<?=base_url()?>extensions/detail/<?=$b->code?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Detail</a>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>
              </table>
            </div>
              
                
          </div>
          <div class="tab-pane fade" id="rejected-tab" role="tabpanel">
              
            <div class="table-responsive py-4">
              <table class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Kode Permohonan</th>
                    <th>Kode Reklame</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $x=1; foreach($extensions as $b){
                    if($b->approval_status==2){
                  ?>
                  <tr>
                    <td width="5%"><?=$x++?></td>
                    <th width="15%"><?=$b->code?></th>
                    <td><?=$b->billboard_code?></td>
                    <td>
                        <?php $status=$this->builder->raw("select extension_approvals.*, positions.name as position_name from extension_approvals, extensions_approval_levels, positions where extension_approvals.level=extensions_approval_levels.level and extensions_approval_levels.approver_position_code=positions.code and extension_approvals.approve_status=2 and extension_approvals.extension_code='$b->code'");
                            echo "Ditolak Oleh : ".$status[0]->position_name."<br>Pada : ".date('d F Y',strtotime($status[0]->approved_at))."<br>Keterangan : ".$status[0]->remark;
                        ?>
                    </td>
                    <td width="10%">
                        <a href="<?=base_url()?>extensions/detail/<?=$b->code?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Detail</a>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>
              </table>
            </div>
              
          </div>
        </div>
      </div>
</div>



<div class="modal" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        
        <div class="modal-header">
            <h4 class="modal-title">Detail - <span id="name"></span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
            <div id="detail-reklame"></div>
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>


<script>
    function loadBillboard(status){
        $.get("<?=base_url()?>lists/billboards/"+status, function(res){
            console.log(res)
        });
    }
</script>