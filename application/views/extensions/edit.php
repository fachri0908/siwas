<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <form method="POST" id="extension_form" action="" >
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Pengajuan Perpanjangan Reklame</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Kode Reklame</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="billboard_code" value="<?=$billboard[0]->code?>" class="form-control form-control-small" id="" placeholder="Kode Reklame" disabled required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Mulai Perpanjangan</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <?php
                                        $code=$billboard[0]->code;
                                        $d=$this->builder->raw("select finish_date from billboards where code='$code'");
                                    ?>
                                    <input type="date" name="start_date" value="<?=date('Y-m-d',strtotime($d[0]->finish_date));?>" class="form-control form-control-small" id="" placeholder="Tanggal mulai" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Tanggal Selesai</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="date" name="finish_date" value="<?=$extension[0]->finish_date?>" class="form-control form-control-small" id="finish_date" placeholder="Tanggal Berakhir" onkeyup="changeFinishDate()" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="">Durasi Perpanjangan</label>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" readonly name="install_duration" value="<?=$extension[0]->duration?>" class="form-control form-control-small" id="install_duration" onkeyup="getTimeDuration()" placeholder="durasi" required>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <?php
                                    $type=$billboard[0]->billboard_type;
                                    $uom=$this->builder->raw("select uom from billboard_types where code='$type'");
                                ?>
                                <div class="form-group">
                                    <input type="text" name="uom" class="form-control" value="hari" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="visibility:<?=($billboard[0]->billboard_type=='LED' || $billboard[0]->billboard_type=='FLM' || $billboard[0]->billboard_type=='SR')?'visible':'hidden'?>">
                            <div class="col-md-2">
                                <label class="control-label">Waktu Tayang</label>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="time" name="start_time" class="form-control" value="<?=$billboard[0]->start_time?>" placeholder="" id="t1" onchange="getTimeDuration()" readonly  onkeyup="getTimeDuration()">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="time" name="end_time" class="form-control"  value="<?=$billboard[0]->end_time?>" placeholder="" id="t2" onchange="getTimeDuration()" readonly onkeyup="getTimeDuration()">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="tt" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="" id="time_duration" name="time_duration" readonly>
                                </div>
                            </div>
                            <div class="col-md-1">
                                detik
                            </div>
                        </div>
                        <!--<div class="row">-->
                        <!--    <div class="col-md-2">-->
                        <!--        <label class="form-control-label" for="">NSR</label>-->
                        <!--    </div>-->
                        <!--    <div class="col-md-8">-->
                        <!--        <div class="form-group">-->
                        <!--            <input type="text" name="nsr" value="" class="form-control form-control-small" id="nsr" placeholder="NSR" readonly>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <!--<div class="row">-->
                        <!--    <div class="col-md-2">-->
                        <!--        <label class="form-control-label" for="">Nilai Kontrak</label>-->
                        <!--    </div>-->
                        <!--    <div class="col-md-8">-->
                        <!--        <div class="form-group">-->
                        <!--            <input type="text" name="contract_value" value="" class="form-control form-control-small" id="contract_value" placeholder="Nilai Kontrak">-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <!--<div class="row">-->
                        <!--    <div class="col-md-2">-->
                        <!--        <label class="form-control-label" for="">Tax</label>-->
                        <!--    </div>-->
                        <!--    <div class="col-md-8">-->
                        <!--        <div class="form-group">-->
                        <!--            <input type="text" name="tax" value="" class="form-control form-control-small" id="tax" placeholder="Tax" readonly>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="saveandupload">Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                    </div>
                    <div class="card-header">
                        Informasi Permohonan
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                                    <a data-toggle="tab" role="tab" href="#billboard-tab" class="btn btn-secondary active">Reklame</a>
                                    <a data-toggle="tab" role="tab" href="#location-tab" class="btn btn-secondary">Lokasi</a>
                                    <a data-toggle="tab" role="tab" href="#attachment-tab" class="btn btn-secondary">Persyaratan</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade  show active" id="billboard-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Jenis Reklame</th><td width="80%"><?php $this->builder->getNameByCond('categories',['code'=>$billboard[0]->category])?></td>
                                        </tr>
                                        <tr>
                                            <th>Tipe Reklame</th><td><?php $this->builder->getNameByCond('billboard_types',['code'=>$billboard[0]->billboard_type])?></td>
                                        </tr>
                                        <tr>
                                            <th>Text Reklame</th><td><?=$billboard[0]->billboard_text?></td>
                                        </tr>
                                        <tr>
                                            <th>Sudut pandang</th><td><?php $this->builder->getNameByCond('view_points',['code'=>$billboard[0]->view_point])?></td>
                                        </tr>
                                        <tr>
                                            <th>Ukuran</th>
                                            <td>
                                                Panjang : <?=$billboard[0]->length?>m, Lebar : <?=$billboard[0]->width?>m, Luas : <?=$billboard[0]->size?>m<sup>2</sup>, Tinggi Media : <?=$billboard[0]->height?>m
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="location-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Kelas Jalan</th><td width="80%"><?php $this->builder->getNameByCond('street_class',['code'=>$billboard[0]->street_class])?></td>
                                        </tr>
                                        <tr>
                                            <th>Kota</th><td><?php $this->builder->getNameByCond('city',['code'=>$billboard[0]->city_code])?></td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th><td><?php $this->builder->getNameByCond('districts',['id'=>$billboard[0]->district_id])?></td>
                                        </tr>
                                        <tr>
                                            <th>Jalan</th><td><?php $this->builder->getNameByCond('streets',['id'=>$billboard[0]->street_id])?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat Lengkap</th><td><?=$billboard[0]->address?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="attachment-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <?php foreach($attachments as $a){ ?>
                                        <tr>
                                            <td>
                                                <?=$a->atc_name?><br>
                                                <?php if($a->file!=NULL){ ?>
                                                    <img width="700px" src="<?=base_url()?>assets/image-upload/registration_attachments/<?=$a->file?>">
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        getTimeDuration()
    })
    function getTimeDuration() {
        var t1 = document.getElementById('t1').value;
        var t2 = document.getElementById('t2').value;
        var dur = document.getElementById('install_duration').value;
        var tt1 = t1.split(':');
        console.log(tt1)
        var ttt1 = tt1[1].split(' ');
        var tt2 = t2.split(':');
        var ttt2 = tt2[1].split(' ');
        var menit = ttt2[0] - ttt1[0];
        var jam = tt2[0] - tt1[0];
        if (menit < 0) {
            menit = 60 + menit;
            jam = jam - 1;
        }
        var second = (jam * 3600) + (menit * 60);
        document.getElementById('tt').value = jam + " jam " + menit + " menit, atau " + second + " detik";
        document.getElementById('time_duration').value = second*dur;
    }
    function changeFinishDate(){
        let finishDate=document.getElementById('finish_date').value;
        const date1 = new Date(finishDate);
        const date2 = new Date('<?=$billboard[0]->finish_date?>');
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        document.getElementById('install_duration').value = diffDays;
        getTimeDuration()
    }
</script>
