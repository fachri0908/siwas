 <!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title> SIWAS Reklame </title>
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->
  <!-- Favicon -->
  <link rel="icon" href="<?=base_url()?>assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/%40fortawesome/fontawesome-free/css/all.min.css" type="text/css">


  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/dropify.css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?=base_url()?>assets/css/argon.min9f1e.css?v=1.1.0" type="text/css">
  <script src="<?=base_url()?>assets/vendor/jquery/dist/jquery.min.js"></script>
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NKDMSK6');
  </script>
  
  
  
  <!-- End Google Tag Manager -->
  <style>
        input, select, textarea{
            color: #212121 !important;
        }
        
        table td {
            border-left: 1px solid #ededed !important;
            border-right: 1px solid #ededed !important;
        }
        
        table td:first-child {
            border-left: none !important;
        }
        
        table tr:last-child {
            border-right: none !important;
            border-bottom: 1px solid #ededed !important;
        }
  </style>
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="<?=base_url()?>" style="margin-left:25%">
          <!--<img src="<?=base_url()?>assets/img/icons/logodki.png" class="navbar-brand-img" alt="...">-->
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
          <div style="text-align:center">
              <a href="<?=base_url()?>">
              <img src="<?=base_url()?>assets/img/icons/logodki.png" width="50%" alt="...">
              </a>
          </div>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <?php if($this->session->userdata('role')=='usr'){
              include('user-menu.php');
          }else{
              include('admin-menu.php');
          }?>
          <!-- Divider -->
          <hr class="my-3">
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-light bg-secondary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <form class="navbar-search navbar-search-dark form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Search" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center ml-md-auto">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-light" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
                  <h6 class="text-sm text-muted m-0">Kamu memiliki <strong class="text-primary">13</strong> pemberitahuan baru.</h6>
                </div>
                <!-- List group -->
                
                <!--get all notif here-->
                <div class="list-group list-group-flush">
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">Informasi Persetujuan</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>09 februari 2019</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Permohonan anda telah disetujui</p>
                      </div>
                    </div>
                  </a>
                </div>
                <!-- View all -->
                <!--<a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>-->
              </div>
            </li>
            
            <!--widget button-->
            
            <!--<li class="nav-item dropdown">-->
            <!--  <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
            <!--    <i class="ni ni-ungroup"></i>-->
            <!--  </a>-->
            <!--  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default dropdown-menu-right">-->
            <!--    <div class="row shortcuts px-4">-->
            <!--      <a href="#!" class="col-4 shortcut-item">-->
            <!--        <span class="shortcut-media avatar rounded-circle bg-gradient-red">-->
            <!--          <i class="ni ni-calendar-grid-58"></i>-->
            <!--        </span>-->
            <!--        <small>Calendar</small>-->
            <!--      </a>-->
            <!--      <a href="#!" class="col-4 shortcut-item">-->
            <!--        <span class="shortcut-media avatar rounded-circle bg-gradient-orange">-->
            <!--          <i class="ni ni-email-83"></i>-->
            <!--        </span>-->
            <!--        <small>Email</small>-->
            <!--      </a>-->
            <!--      <a href="#!" class="col-4 shortcut-item">-->
            <!--        <span class="shortcut-media avatar rounded-circle bg-gradient-info">-->
            <!--          <i class="ni ni-credit-card"></i>-->
            <!--        </span>-->
            <!--        <small>Payments</small>-->
            <!--      </a>-->
            <!--      <a href="#!" class="col-4 shortcut-item">-->
            <!--        <span class="shortcut-media avatar rounded-circle bg-gradient-green">-->
            <!--          <i class="ni ni-books"></i>-->
            <!--        </span>-->
            <!--        <small>Reports</small>-->
            <!--      </a>-->
            <!--      <a href="#!" class="col-4 shortcut-item">-->
            <!--        <span class="shortcut-media avatar rounded-circle bg-gradient-purple">-->
            <!--          <i class="ni ni-pin-3"></i>-->
            <!--        </span>-->
            <!--        <small>Maps</small>-->
            <!--      </a>-->
            <!--      <a href="#!" class="col-4 shortcut-item">-->
            <!--        <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">-->
            <!--          <i class="ni ni-basket"></i>-->
            <!--        </span>-->
            <!--        <small>Shop</small>-->
            <!--      </a>-->
            <!--    </div>-->
            <!--  </div>-->
            <!--</li>-->
          </ul>
          <ul class="navbar-nav align-items-center ml-auto ml-md-0">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <div class="media-body ml-2 d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?=$this->session->userdata('user_name')?> &nbsp;</span>
                  </div>
                  <span class="avatar avatar-sm rounded-circle">
                      <?php 
                        if($this->session->userdata('picture')==''){
                            $image=base_url().'assets/img/theme/team-4.jpg';
                        }else{
                            $image=base_url().'assets/img/profile/'.$this->session->userdata('picture');
                        }
                      ?>
                    <img alt="Image placeholder" src="<?=$image?>">
                  </span>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <?php if($this->session->userdata('role')=='usr'){ ?>
                <a href="<?=base_url()?>applicants" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>My profile</span>
                </a>
                <?php } ?>
                <a href="<?=base_url()?>applicants/changepassword" class="dropdown-item">
                  <i class="fa fa-lock"></i>
                  <span>Ganti Password</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?=base_url()?>auth/logout" class="dropdown-item">
                  <i class="ni ni-button-power"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 d-inline-block mb-0"><?=$breadcrumb['title']?></h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                  <li class="breadcrumb-item"><a href="#"><i class="<?=$breadcrumb['icon']?>"></i></a></li>
                  <li class="breadcrumb-item"><a href="#"><?=$breadcrumb['parent']?></a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?=$breadcrumb['child']?></li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <?=$content?>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?=base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>

  <!-- Optional JS -->
  <script src="<?=base_url()?>assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <script src="<?=base_url()?>assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
  <script src="<?=base_url()?>assets/js/vendor/jvectormap/jquery-jvectormap-world-mill.js"></script>

  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <!-- <script src="<?=base_url()?>assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script> -->
  <script src="<?=base_url()?>assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/dropzone/dist/min/dropzone.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/select2/dist/js/select2.min.js"></script>
  <!-- datatables -->


  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>




  <!-- Argon JS -->
  <!--<script src="<?=base_url()?>assets/js/argon-backup.js?v=1.1.0"></script>-->
  <script src="<?=base_url()?>assets/js/argon.min9f1e.js?v=1.1.0"></script>
  <!-- Demo JS - remove this in your project -->
  <script src="<?=base_url()?>assets/js/demo.min.js"></script>
  <script src="<?=base_url()?>assets/js/dropify.js"></script>
  <script>
            $(document).ready(function(){
                // Basic
                $('.dropify').dropify();

                // Translated
                $('.dropify-fr').dropify({
                    messages: {
                        default: 'Glissez-déposez un fichier ici ou cliquez',
                        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                        remove:  'Supprimer',
                        error:   'Désolé, le fichier trop volumineux'
                    }
                });

                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function(event, element){
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function(e){
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
        </script>
  <script>
    // Facebook Pixel Code Don't Delete
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window,
      document, 'script', 'http://connect.facebook.net/en_US/fbevents.js');

    try {
      fbq('init', '111649226022273');
      fbq('track', "PageView");

    } catch (err) {
      console.log('Facebook Track Error:', err);
    }
  </script>
  
  <script>
  var bar = document.getElementById("chart-bars3");
    var myBarChart = new Chart(bar, {
      type: 'bar',
      data: {
        labels: ["Jan","Feb","Maret","April","Mai","Juni","Juli","Agu","Sep","Okt","Nov","Des"],
        datasets:[{
            label: "<?=date('Y')?>",
            backgroundColor: ["#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b"],
            data:[100,90,120,70,140,120,70,140,90],
        },{
            label: "<?=date('Y')-1?>",
            backgroundColor: ["#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b"],
            data:[90,100,90,70,110,90,120,70,140,120,100,110],
        },{
            label: "<?=date('Y')-2?>",
            backgroundColor: ["#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c"],
            data:[120,110,80,90,100,90,120,70,140,120,90,80],
        }]
      },
      options: {
        legend: { 
            display: true,
            position: "right",
        },
        title: {
          display: true,
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return 'Rp. '+ tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawOnChartArea: true
                }
            }],
            xAxes: [{
                barPercentage: 3,
                gridLines: {
                  drawOnChartArea: false
                }
            }]
        },
      },
    });
  </script>
  
  
  <script>
  var bar = document.getElementById("chart-bars30");
    var myBarChart = new Chart(bar, {
      type: 'bar',
      data: {
        labels: ["Januari","Februari","Maret","April","Mai","Juni","Juli","Agustus","September","Oktober","November","Desember"],
        datasets:[{
            label: "<?=date('Y')?>",
            backgroundColor: ["#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b"],
            data:[<?=@$data_label[date('Y')]?>],
        },{
            label: "<?=date('Y')-1?>",
            backgroundColor: ["#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b"],
            data:[<?=@$data_label[date('Y') - 1]?>],
        },{
            label: "<?=date('Y')-2?>",
            backgroundColor: ["#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c","#6ab04c"],
            data:[<?=@$data_label[date('Y') - 2]?>],
        }]
      },
      options: {
        legend: { 
            display: true,
            position: "right",
        },
        title: {
          display: true,
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawOnChartArea: true
                }
            }],
            xAxes: [{
                barPercentage: 3,
                gridLines: {
                  drawOnChartArea: false
                }
            }]
        },
      },
    });
  </script>
  
  <script>
  var bar2 = document.getElementById("chart-bars2");
    var myBarChart2 = new Chart(bar2, {
      type: 'bar',
      data: {
        labels: ["Januari","Februari","Maret","April","Mai","Juni","Juli","Agustus","September","Oktober","November","Desember"],
        datasets:[{
            label: "Target",
            backgroundColor: ["#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b","#f0932b"],
            data:[100,90,120,70,140,120,70,140,90],
        },{
            label: "Realisasi",
            backgroundColor: ["#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b","#30336b"],
            data:[90,100,90,70,110,90,120,70,140,120,100,110],
        },]
      },
      options: {
        legend: { 
            display: true,
            position: "right",
        },
        title: {
          display: true,
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    return 'Rp. '+ tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); }, },
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawOnChartArea: true
                }
            }],
            xAxes: [{
                barPercentage: 3,
                gridLines: {
                  drawOnChartArea: false
                }
            }]
        },
      },
    });
    
  </script>
  
  
  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&amp;ev=PageView&amp;noscript=1" />
  </noscript>
  
  
  
</body>


<!-- Mirrored from demos.creative-tim.com/argon-dashboard-pro/pages/dashboards/alternative.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Aug 2019 02:45:39 GMT -->
</html>