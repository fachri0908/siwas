<ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#navbar-dashboards" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-dashboards">
        <i class="ni ni-chart-pie-35 text-orange"></i>
        <span class="nav-link-text">Dashboard</span>
      </a>
      <div class="collapse" id="navbar-dashboards">
        <ul class="nav nav-sm flex-column">
          <li class="nav-item">
            <a href="<?=base_url()?>dashboard" class="nav-link">Dashboard</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>dashboard/compare" class="nav-link">Target vs Realisasi</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>users">
        <i class="fa fa-user text-green"></i>
        <span class="nav-link-text">List Pemohon</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>billboards/installed">
        <i class="fa fa-image text-green"></i>
        <span class="nav-link-text">Reklame Terpasang</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>billboards">
        <i class="ni ni-money-coins text-yellow"></i>
        <span class="nav-link-text">Permohonan Baru</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>extensions/admin">
        <i class="ni ni-money-coins text-yellow"></i>
        <span class="nav-link-text">Perpanjangan</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>adminpayments">
        <i class="ni ni-money-coins text-yellow"></i>
        <span class="nav-link-text">Pembayaran</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>illegals">
        <i class="ni ni-money-coins text-red"></i>
        <span class="nav-link-text">Reklame Ilegal</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>pointing">
        <i class="fa fa-map-marker text-red"></i>
        <span class="nav-link-text">Peta Reklame</span>
      </a>
    </li>
    
    
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url()?>announcement">
        <i class="ni ni-bell-55 text-info"></i>
        <span class="nav-link-text"> Pemberitahun </span>
      </a>
    </li>
    
    <!--<li class="nav-item">-->
    <!--  <a class="nav-link" href="<?=base_url()?>calculator">-->
    <!--    <i class="ni ni-tv-2 text-red"></i>-->
    <!--    <span class="nav-link-text">Tax Calculator</span>-->
    <!--  </a>-->
    <!--</li>-->
    
    <li class="nav-item">
      <a class="nav-link" href="#navbar-setups" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-setups">
        <i class="ni ni-settings text-primary"></i>
        <span class="nav-link-text">Setup</span>
      </a>
      <div class="collapse" id="navbar-setups">
        <ul class="nav nav-sm flex-column">
          <li class="nav-item">
            <a href="<?=base_url()?>setup/rates" class="nav-link">Ketetapan Tarif Pajak</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/category" class="nav-link">Jenis Reklame</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/type" class="nav-link">Tipe Reklame</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/street_class" class="nav-link">Kelas Jalan</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/nsr" class="nav-link">Nilai Sewa Reklame</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/nsr_led" class="nav-link">Nilai Sewa Reklame (LED)</a>
          </li>
          <!--<li class="nav-item">-->
          <!--  <a href="<?=base_url()?>setup/viewpoint" class="nav-link">Sudut Pandang</a>-->
          <!--</li>-->
          <!--<li class="nav-item">-->
          <!--  <a href="<?=base_url()?>setup/njor" class="nav-link">NJOR</a>-->
          <!--</li>-->
          <li class="nav-item">
            <a href="<?=base_url()?>setup/viewpoint" class="nav-link">Sudut Pandang</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/city" class="nav-link">Kota</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/district" class="nav-link">Kecamatan</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/status_tanah" class="nav-link">Status Tanah</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/letak_reklame" class="nav-link">Letak Reklame</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>employees" class="nav-link">Pegawai</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>setup/uom" class="nav-link">UOM</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>approvals/registration" class="nav-link">Persetujuan Pendaftaran</a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>approvals/extension" class="nav-link">Persetujuan Perpanjangan</a>
          </li>
        </ul>
      </div>
    </li>
</ul>