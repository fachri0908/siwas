<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Daftar SIWAS </title>
  <link rel="canonical" href="https://www.creative-tim.com/product/argon-dashboard-pro" />
  <meta name="keywords" content="dashboard, bootstrap 4 dashboard, bootstrap 4 design, bootstrap 4 system, bootstrap 4, bootstrap 4 uit kit, bootstrap 4 kit, argon, argon ui kit, creative tim, html kit, html css template, web template, bootstrap, bootstrap 4, css3 template, frontend, responsive bootstrap template, bootstrap ui kit, responsive ui kit, argon dashboard">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta itemprop="name" content="Argon - Premium Dashboard for Bootstrap 4 by Creative Tim">
  <meta itemprop="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta itemprop="image" content="https://s3.amazonaws.com/creativetim_bucket/products/137/original/opt_adp_thumbnail.jpg">
  <meta name="twitter:card" content="product">
  <meta name="twitter:site" content="@creativetim">
  <meta name="twitter:title" content="Argon - Premium Dashboard for Bootstrap 4 by Creative Tim">
  <meta name="twitter:description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="twitter:creator" content="@creativetim">
  <meta name="twitter:image" content="https://s3.amazonaws.com/creativetim_bucket/products/137/original/opt_adp_thumbnail.jpg">
  <meta property="fb:app_id" content="655968634437471">
  <meta property="og:title" content="Argon - Premium Dashboard for Bootstrap 4 by Creative Tim" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="https://demos.creative-tim.com/argon-dashboard/index.html" />
  <meta property="og:image" content="https://s3.amazonaws.com/creativetim_bucket/products/137/original/opt_adp_thumbnail.jpg" />
  <meta property="og:description" content="Start your development with a Dashboard for Bootstrap 4." />
  <meta property="og:site_name" content="Creative Tim" />
  <link rel="icon" href="<?=base_url()?>assets/img/brand/favicon.png" type="image/png">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/%40fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/argon.min9f1e.css?v=1.1.0" type="text/css">
  <style>
      .error-validation{
          color:red;
          font-size:10px;
      }
  </style>
</head>
<body class="bg-default">
  <div class="main-content">
    <!--<div class="header bg-gradient-primary py-7 ">-->
    <!--    <div class="separator separator-bottom separator-skew zindex-100">-->
    <!--        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">-->
    <!--          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>-->
    <!--        </svg>-->
    <!--    </div>-->
    <!--</div>-->
    <div class="container mt-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs12">
          <div class="card bg-secondary border-0 mb-0">
            <div class="card-header bg-transparent">
              <div class="text-muted text-center mt-2 mb-1"><small>MENDAFTAR SIWAS</small></div>
            </div>
            <div class="card-body py-lg-5">
              <?php if($this->session->flashdata('success')){ ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                  <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>
              <form method="POST">
                <div class="form-group mx-2">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
                    </div>
                    <input class="form-control" placeholder="Nama" type="text" name="name" required>
                  </div>
                </div>
                <div class="form-group mx-2">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-map"></i></span>
                    </div>
                    <input class="form-control" placeholder="Alamat" type="text" name="address" required>
                  </div>
                </div>
                <div class="form-group mx-2">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-phone"></i></span>
                    </div>
                    <input class="form-control" placeholder="No HP" type="text" name="phone_number" required>
                  </div>
                  <span class="error-validation" id="phone-validation"></span>
                </div>
                <div class="form-group mx-2">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Email" type="email" name="email" required onchange="checkemail(this.value)">
                  </div>
                  <span class="error-validation" id="email-validation"></span>
                </div>
                <div class="form-group mx-2">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-user"></i></span>
                    </div>
                    <select name="third_person" class="form-control">
                        <option selected hidden>-Daftar Sebagai-</option>
                        <option value="0">Pihak Pertama</option>
                        <option value="1">Pihak Ketiga</option>
                    </select>
                  </div>
                </div>
                <div class="form-group mx-2">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i id="password-icon" class="ni ni-lock-circle-open"></i>
                      </span>
                    </div>
                    <input class="form-control" placeholder="Password" type="password" name="password">
                  </div>
                </div>
                <div class="form-group mx-2">
                  <span class="text-muted">Sudah punya akun..? <a href="<?=base_url()?>auth">login disini</a></span>
                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-primary">Daftar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?=base_url()?>assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="<?=base_url()?>assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
  <script src="<?=base_url()?>assets/js/vendor/jvectormap/jquery-jvectormap-world-mill.js"></script>
  <script src="<?=base_url()?>assets/js/argon.min9f1e.js?v=1.1.0"></script>
  <script src="<?=base_url()?>assets/js/demo.min.js"></script>
  <script>
      function checkemail(val){
          $.get('<?=base_url()?>auth/checkemail?email='+val, function(res){
              if(res==1){
                  $('#email-validation').html('email sudah digunakan');
              }else{
                  $('#email-validation').html('');
              }
          })
      }
  </script>
</body>
</html>

