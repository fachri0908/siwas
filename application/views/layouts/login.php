<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Reklame Online | Pemerintah Provinsi DKI Jakarta</title>
    <link rel="icon" href="<?=base_url()?>assets/img/brand/favicon.png" type="image/png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/%40fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/argon.min9f1e.css?v=1.1.0" type="text/css">
    <style>
        .swap-button {
            border: 1px solid #e67e22;
            border-radius: 8px;
        }
        
        .big-text {
            font-size: 5em;
            color: #fff;
            font-weight: bold;
            font-family: arial;
        }
        
        .small-text {
            font-size: 2em;
            color: #fff;
            font-weight: bold;
            font-family: arial;
        }
        
        .info-item {
            border-radius: 4px;
            text-align: center;
            vertical-align: middle;
            margin: 5px;
            margin-bottom: 20px;
            color: #fff;
            cursor: pointer;
            height: 110px;
        }
        
        .info-item-12 {
            border-radius: 4px;
            text-align: center;
            vertical-align: middle;
            margin: 0px;
            margin-bottom: 20px;
            color: #fff;
            cursor: pointer;
            height: 75px;
        }
        
        .info-icon {
            margin-top: 0.7em;
            font-size: 2em;
        }
        
        .bg-orange {
            background-color: #ffa801;
        }
        
        .bg-gradient-primary {
            background: linear-gradient(87deg, #e67e22 0, #f39c12 100%)!important;
        }
        
        .bg-primary {
            background: #204C9C !important;
        }
        
        .btn-primary {
            background: #204C9C !important;
        }
        
        .active {
            background-color: #f39c12 !important;
        }
        .modal-dialog{
            margin-top:0px !important;
        }
        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 999999999999999;
            display: none;
        }
        #loader {
            display: block;
            position: relative;
            left: 50%;
            top: 50%;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #9370DB;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }
        #loader:before {
            content: "";
            position: absolute;
            top: 5px;
            left: 5px;
            right: 5px;
            bottom: 5px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #BA55D3;
            -webkit-animation: spin 3s linear infinite;
            animation: spin 3s linear infinite;
        }
        #loader:after {
            content: "";
            position: absolute;
            top: 15px;
            left: 15px;
            right: 15px;
            bottom: 15px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: #FF00FF;
            -webkit-animation: spin 1.5s linear infinite;
            animation: spin 1.5s linear infinite;
        }
        @-webkit-keyframes spin {
            0%   {
                -webkit-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spin {
            0%   {
                -webkit-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
</head>

<body class="bg-default">
    <div id="preloader">
      <div id="loader"></div>
    </div>
    <div class="main-content">
        <div class="header bg-gradient-primary py-2 pt-lg-9">
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <div class="card-body mt--8 pb-5" style="margin-top: 0px !important">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs12">
                    <div class="card border-0 mb-0" style="background:rgba(255,255,255,0.1);">
                        <div class="card-body py-lg-5">
                            <center>
                                <img class="" src="<?=base_url()?>assets/img/icons/dki-large.png" width="40%">
                                <br>
                                <span class="big-text">Reklame Online</span>
                                <span class="small-text">Pemerintah Provinsi DKI Jakarta</span>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-header bg-transparent">
                            <div class="btn-group btn-block btn-group-toggle swap-button" data-toggle="buttons">
                                <label class="btn btn-secondary active btn-lg" onclick="swap(1)">
                                    <input type="radio" autocomplete="off" checked> Login
                                </label>
                                <label class="btn btn-secondary btn-lg" onclick="swap(2)">
                                    <input type="radio" id="option2" autocomplete="off"> Informasi
                                </label>
                            </div>
                        </div>

                        <div class="card-body py-lg-5" id="login-box" style="height:455px;margin-top:-30px">
                            <?=$this->session->flashdata('flash')?>
                            <?php if($this->session->flashdata('success')){ ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                                    <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php } ?>
                            <form method="POST">
                                <div class="form-group mx-2 mb-4" style="margin-top: 20px">
                                    <label>USERNAME / EMAIL</label>
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Email" type="email" name="email" required value="siwas@mail.dev">
                                    </div>
                                </div>
                                <div class="form-group mx-2 mb-4">
                                    <label>PASSWORD</label>
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Password" type="password" name="password" required value="dev">
                                    </div>
                                </div>
                                <div class="form-group mx-2">
                                    <span class="text-muted float-left">Belum punya akun ? <a href="<?=base_url()?>auth/register">daftar disini</a></span>
                                    <span class="text-muted float-right"><a href="<?=base_url()?>auth/forgetpassword">lupa password..?</a></span>
                                </div><br><br>
                                <div class="text-right">
                                    <input type="submit" name="login" value="Login" class="btn btn-primary">
                                </div>
                            </form>
                            <div style="position: absolute; bottom:30px;left:30%">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="copyright text-sm text-muted">
                                            <center> &copy;
                                                <?=date('Y')?> <a href="<?=base_url()?>" class="font-weight-bold ml-1" target="_blank">Pemerintah Provinsi DKI Jakarta</a></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body py-lg-5" id="info-box" style="display:none;height:455px;margin-top:-30px">
                            <div class="row justify-content-around">

                                <div class="col-5 info-item bg-primary" onclick="showInfo(1)">
                                    <i class="fa fa-check-circle info-icon"></i>
                                    <br> Reklame Ber - SIPR
                                </div>
                                <div class="col-5 info-item bg-primary" onclick="showInfo(2)">
                                    <i class="fa fa-history info-icon"></i>
                                    <br> Reklame Dalam Proses
                                </div>
                                <div class="col-5 info-item bg-primary" onclick="showInfo(3)">
                                    <i class="fa fa-hand-paper info-icon"></i>
                                    <br> Reklame Ditolak
                                </div>
                                <div class="col-5 info-item bg-primary" onclick="showInfo(4)">
                                    <i class="fa fa-times-circle info-icon"></i>
                                    <br> Reklame Dibongkar
                                </div>
                                <div class="col-11 info-item-12 bg-primary" onclick="showMap()">
                                    <i class="fa fa-map info-icon"></i> Peta Reklame
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="min-width:98%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="info-modal-title">Reklame Ber - SIPR</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="info-container" style="min-height:450px">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal animated fadeIn" id="showMapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document" style="min-width:98%; margin-top: 10px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="info-modal-title">Peta Reklame</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="map-container" style="margin-top: -20px"></div>
            </div>
        </div>
    </div>

    <script src="<?=base_url()?>assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="<?=base_url()?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
    <script src="<?=base_url()?>assets/js/vendor/jvectormap/jquery-jvectormap-world-mill.js"></script>
    <script src="<?=base_url()?>assets/js/argon.min9f1e.js?v=1.1.0"></script>
    <script src="<?=base_url()?>assets/js/demo.min.js"></script>

    <script>
        function swap(val) {
            if (val == 1) {
                $('#login-box').css('display', 'block')
                $('#info-box').css('display', 'none')
            } else {
                $('#info-box').css('display', 'block')
                $('#login-box').css('display', 'none')
            }
        }

        function showInfo(val) {
            $("#info-container").html('');
            $.get("<?=base_url()?>lists/infobillboard/" + val, function(res) {
                $("#info-container").html(res);
            });
            $('#showInfoModal').modal('show')
            if (val == 1) {
                $('#info-modal-title').html('Reklame Ber - SIPR')
            } else if (val == 2) {
                $('#info-modal-title').html('Reklame Dalam Proses')
            } else if (val == 3) {
                $('#info-modal-title').html('Reklame Ditolak')
            } else {
                $('#info-modal-title').html('Peta Reklame')
            }

        }

        function showMap() {
            $("#preloader").show();
            $.get("<?=base_url()?>lists/billboardmap", function(res) {
                if(res){
                    $("#preloader").hide();
                    $("#map-container").html(res);
                    $('#showMapModal').modal('show')
                }
            });
        }
    </script>
</body>

</html>