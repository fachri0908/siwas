<ul class="navbar-nav">
    <li class="nav-item">
        
      <a class="nav-link" href="<?=base_url()?>applicants/dashboard">
        <i class="ni ni-chart-pie-35 text-orange"></i>
        <span class="nav-link-text">Dashboard</span>
      </a>
      
      <a class="nav-link" href="<?=base_url()?>applicants/billboard">
        <i class="fa fa-image text-yellow"></i>
        <span class="nav-link-text">Permohonan Reklame</span>
      </a>
      
      <a class="nav-link" href="<?=base_url()?>applicants/installed">
        <i class="fa fa-image text-green"></i>
        <span class="nav-link-text">Reklame Terpasang</span>
      </a>
      
      <a class="nav-link" href="<?=base_url()?>extensions">
        <i class="fa fa-anchor"></i>
        <span class="nav-link-text">Perpanjangan</span>
      </a>
      
      <a class="nav-link" href="<?=base_url()?>payments">
        <i class="fa fa-tag text-yellow" aria-hidden="true"></i>
        <span class="nav-link-text">Tagihan</span>
      </a>
      
    </li>
    
    <!--<li class="nav-item">-->
    <!--  <a class="nav-link" href="#navbar-setups" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-setups">-->
    <!--    <i class="ni ni-settings text-primary"></i>-->
    <!--    <span class="nav-link-text">Setup</span>-->
    <!--  </a>-->
    <!--  <div class="collapse" id="navbar-setups">-->
    <!--    <ul class="nav nav-sm flex-column">-->
    <!--      <li class="nav-item">-->
    <!--        <a href="<?=base_url()?>setup/rates" class="nav-link">Ketetapan Tarif Pajak</a>-->
    <!--      </li>-->
    <!--      <li class="nav-item">-->
    <!--        <a href="<?=base_url()?>setup/category" class="nav-link">Jenis Reklame</a>-->
    <!--      </li>-->
    <!--    </ul>-->
    <!--  </div>-->
    <!--</li>-->
 </ul>