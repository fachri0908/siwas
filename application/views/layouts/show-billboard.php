<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Reklame Online | Pemerintah Provinsi DKI Jakarta</title>
    <link rel="icon" href="<?=base_url()?>assets/img/brand/favicon.png" type="image/png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/%40fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/argon.min9f1e.css?v=1.1.0" type="text/css">
    <style>
        .billboard-content{
            min-height:500px !important;
        }
    </style>
</head>

<body class="bg-default">
    <div id="preloader">
      <div id="loader"></div>
    </div>
    <div class="main-content">
        <div class="card-body pb-5" style="margin-top: 0px !important">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs12">
                    <div class="card border-0 mb-0 billboard-content" style="background:#fff;">
                        <div class="card-body">
                        <center class="mb-5">
                            <img width="300em" src="<?=base_url()?>assets/image-upload/registration_attachments/<?=$attachment[0]->file?>">
                        </center>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                                    <a data-toggle="tab" role="tab" href="#applicant-tab" class="btn btn-secondary active">Pemohon</a>
                                    <a data-toggle="tab" role="tab" href="#billboard-tab" class="btn btn-secondary">Reklame</a>
                                    <a data-toggle="tab" role="tab" href="#location-tab" class="btn btn-secondary">Lokasi</a>
                                    <a data-toggle="tab" role="tab" href="#status-tab" class="btn btn-secondary">Status</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-content" id="pills-tabContent">
                            
                            <div class="tab-pane fade show active" id="applicant-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Nama</th><td width="80%"><?=$applicant[0]->name?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th><td><?=$applicant[0]->address?></td>
                                        </tr>
                                        <tr>
                                            <th>No HP</th><td><?=$applicant[0]->phone_number?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="billboard-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Jenis Reklame</th><td width="80%"><?php $this->builder->getNameByCond('categories',['code'=>$billboard[0]->category])?></td>
                                        </tr>
                                        <tr>
                                            <th>Tipe Reklame</th><td><?php $this->builder->getNameByCond('billboard_types',['code'=>$billboard[0]->billboard_type])?></td>
                                        </tr>
                                        <tr>
                                            <th>Text Reklame</th><td><?=$billboard[0]->billboard_text?></td>
                                        </tr>
                                        <tr>
                                            <th>Sudut pandang</th><td><?php $this->builder->getNameByCond('view_points',['code'=>$billboard[0]->view_point])?></td>
                                        </tr>
                                        <tr>
                                            <th>Ukuran</th>
                                            <td>
                                                Panjang : <?=$billboard[0]->length?>m, Lebar : <?=$billboard[0]->width?>m, Luas : <?=$billboard[0]->size?>m<sup>2</sup>, Tinggi Media : <?=$billboard[0]->height?>m
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="location-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Kelas Jalan</th><td width="80%"><?php $this->builder->getNameByCond('street_class',['code'=>$billboard[0]->street_class])?></td>
                                        </tr>
                                        <tr>
                                            <th>Kota</th><td><?php $this->builder->getNameByCond('city',['code'=>$billboard[0]->city_code])?></td>
                                        </tr>
                                        <tr>
                                            <th>Kecamatan</th><td><?php $this->builder->getNameByCond('districts',['id'=>$billboard[0]->district_id])?></td>
                                        </tr>
                                        <tr>
                                            <th>Jalan</th><td><?php $this->builder->getNameByCond('streets',['id'=>$billboard[0]->street_id])?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat Lengkap</th><td><?=$billboard[0]->address?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="status-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Tanggal Pasang</th><td width="80%"><?=date('d F Y',strtotime($billboard[0]->install_date))?></td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Berakhir</th><td><?=date('d F Y',strtotime($billboard[0]->finish_date))?></td>
                                        </tr>
                                        <?php $bcode=$billboard[0]->code; $payment=$this->builder->raw("select * from payments where type = 'RGS' and billboard_code='$bcode'"); ?>
                                        <tr>
                                            <th>Status</th><td><?=($payment[0]->status)?'<span class="badge badge-warning">Belum Bayar pajak</span>':'<span class="badge badge-success">Sudah Bayar Pajak</span>'?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="<?=base_url()?>assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="<?=base_url()?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
    <script src="<?=base_url()?>assets/js/vendor/jvectormap/jquery-jvectormap-world-mill.js"></script>
    <script src="<?=base_url()?>assets/js/argon.min9f1e.js?v=1.1.0"></script>
    <script src="<?=base_url()?>assets/js/demo.min.js"></script>
</body>

</html>