<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-4">
          <div class="form-group">
              <select class="form-control">
                <option selected hidden>Pilih tahun</option>
                <option>2019</option>
                <option>2018</option>
                <option>2017</option>
              </select>
          </div>
        </div>
        <div class="col-md-1">
          <div class="form-group">
          <button class="btn btn-primary">Cari</button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Target Pribadi</h5>
                  <span class="h2 font-weight-bold mb-0">2 M</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                    <i class="ni ni-fat-add"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 12%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Realisasi Pribadi</h5>
                  <span class="h2 font-weight-bold mb-0">1.8M</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-delivery-fast"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-warning mr-2">90%</span>
                <span class="text-nowrap"> Tercapai</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Target Instansi</h5>
                  <span class="h2 font-weight-bold mb-0">10 M</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-check-bold"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Realisasi Instansi</h5>
                  <span class="h2 font-weight-bold mb-0">11 M</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-button-power"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2">110%</span>
                <span class="text-nowrap">Tercapai</span>
              </p>
            </div>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="card" style="width:100%">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Target vs Realisasi</h6>
                  <h2 class="h3 mb-0">Penerimaan  Pajak Reklame</h2>
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="chart">
                <canvas id="chart-bars2" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card" style="width:100%">
            <div class="card-header">
              <h3 class="mb-0">Penerimaan Pajak Reklame (Rp.)</h3>
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>Tahun</th>
                    <th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>Mei</th><th>Jun</th>
                    <th>Jul</th><th>Agu</th><th>Sep</th><th>Okt</th><th>Nov</th><th>Des</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Target</td>
                    <td>1.2 M</td><td>1.2 M</td><td>945 JT</td><td>2.3 M</td><td>850 JT</td><td>3.5 M</td><td>2.5 M</td><td>2.2 M</td><td>1.25 M</td><td>1.32 M</td><td>2.21 M</td><td>980 JT</td>
                  </tr>
                  <tr>
                    <td>Realisasi</td>
                    <td>2.2 M</td><td>945 JT</td><td>2.3 M</td><td>850 JT</td><td>3.5 M</td><td>800 JT</td><td>3 M</td><td>2.2 M</td><td>1.35 M</td><td></td><td></td><td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>