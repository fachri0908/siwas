<div class="container-fluid mt--6">
    
    
<!--    
    <div class="row">
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Pendaftaran baru</h5>
                  <span class="h2 font-weight-bold mb-0">1097</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                    <i class="ni ni-fat-add"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 12%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Perpanjangan</h5>
                  <span class="h2 font-weight-bold mb-0">478</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-delivery-fast"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-info mr-2"><i class="fa fa-equals"></i></span>
                <span class="text-nowrap">Bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Reklame Disetujui</h5>
                  <span class="h2 font-weight-bold mb-0">73%</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-check-bold"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 20%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Reklame Ditolak</h5>
                  <span class="h2 font-weight-bold mb-0">27%</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-button-power"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> 11%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
    </div>
-->



<!--new disgn-->
    <div class="row">
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>billboards">Pendaftaran baru</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$menunggu_persetujuan?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                    <i class="ni ni-fat-add"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 12%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>billboards">Reklame Disetujui</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$disetujui?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-check-bold"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 20%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>billboards">Reklame Ditolak</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$ditolak?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-button-power"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> 11%</span>
                <span class="text-nowrap">Dari bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
        
        
        <div class="col-xl-3 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0"><a href="<?=base_url()?>illegals">Reklame Ilegal</a></h5>
                  <span class="h2 font-weight-bold mb-0"><?=$ilegal?></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-delivery-fast"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-info mr-2"><i class="fa fa-equals"></i> 23% </span>
                <span class="text-nowrap">Dari Bulan lalu</span>
              </p>
            </div>
          </div>
        </div>
    </div>
<!--end new design-->






      <div class="row">
        <div class="col-md-9">
          <div class="card" style="width:100%">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Realisasi</h6>
                  <h2 class="h3 mb-0">Penerimaan Pajak Reklame</h2>
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="chart">
                
                <!--id chart-bars30 ambil data dari database-->
                <!--id chart-bars3 data static-->
                <!--<canvas id="chart-bars3" class="chart-canvas"></canvas>-->
                <!--<canvas id="chart-bars30" class="chart-canvas"></canvas>-->
                
                
                <canvas id="chart-bars3" class="chart-canvas"></canvas>
                
                
                
                
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="col-3 pt-3">
         <div class="card bg-gradient-default mt-2">
              <div class="card-body p-2">
                 <div id="top_three" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php $no = 0; foreach($top_three as $sc) { ?>
                            <div class="carousel-item <?=($no == 0) ? 'active':''?>" data-interval="9000">
                                <div class="card bg-gradient-default">
                                    <div class="card-body">
                                        <div class="mb-2">
                                          <div class="text-light mt-2 text-sm"><?=$sc->name?></div>
                                          <div>
                                            <span class="text-success font-weight-600">+ <?=round($sc->ttl / count($all_billboard) * 100, 2)?>%</span> <span class="text-light">(<?=$sc->ttl?>)</span>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col"><small class="text-light">Sales: <?=round($sc->ttl / count($all_billboard) * 100, 2)?>%</small>
                                            <div class="progress progress-xs my-2">
                                              <div class="progress-bar bg-success" style="width: <?=round($sc->ttl / count($all_billboard) * 100, 2)?>%"></div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $no++; } ?>
                    </div>
                      <a class="carousel-control-prev mt-8" href="#top_three" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next mt-8" href="#top_three" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                      <ol class="carousel-indicators">
                        <?php $no = 0; foreach($top_three as $sc) { ?>
                            <li data-target="#top_three" data-slide-to="<?=$no?>" class="<?=($no == 0) ? 'active':''?>"></li>
                        <?php $no++; } ?>
                      </ol>
                </div>
              </div>
            </div>
            
            
            
              
            <div class="card bg-gradient-default mt-2">
              <div class="card-body p-2">
                 <div id="street_class" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php $no = 0; foreach($street_class as $sc) { ?>
                            <div class="carousel-item <?=($no == 0) ? 'active':''?>" data-interval="6000">
                                <div class="card bg-gradient-default">
                                    <div class="card-body">
                                        <div class="mb-2">
                                          <div class="text-light mt-2 text-sm"><?=$sc->name?></div>
                                          <div>
                                            <span class="text-success font-weight-600">+ <?=$percentase_street_class[$sc->code]?></span> <span class="text-light">(<?=$count_street_class[$sc->code]?>)</span>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col">
                                            <small class="text-light">Orders: <?=$percentase_street_class[$sc->code]?></small>
                                            <div class="progress progress-xs my-2">
                                              <div class="progress-bar bg-success" style="width: <?=$percentase_street_class[$sc->code]?>"></div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $no++; } ?>
                    </div>
                      <a class="carousel-control-prev mt-8" href="#street_class" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next mt-8" href="#street_class" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                      <ol class="carousel-indicators">
                        <?php $no = 0; foreach($street_class as $sc) { ?>
                            <li data-target="#street_class" data-slide-to="<?=$no?>" class="<?=($no == 0) ? 'active':''?>"></li>
                        <?php $no++; } ?>
                      </ol>
                </div>
              </div>
            </div>
            
            
        </div>
        
      </div>




      <div class="row">
        <div class="col-md-12">
          <div class="card" style="width:100%">
            <div class="card-header">
              <h3 class="mb-0">Penerimaan Pajak Reklame (Rp.)</h3>
            </div>
            <?php 
        	    $tahun = [ date('Y') - 0, date('Y') - 1, date('Y') - 2];
                $bulan = ["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des"];
            ?>
            <div class="table-responsive py-4">
              <table class="table table-flush">
                <thead class="thead-light">
                  <tr>
                    <th>Tahun</th>
                    <?php foreach($bulan as $bln) { ?>
                        <th><?=$bln?></th>
                    <?php } ?>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($tahun as $thn) { 
                        echo "<tr>";
                        echo "<td>$thn</td>";
                        $da = $jumlah[$thn];
                        foreach(explode(',', $da) as $jml) {
                            echo "<td>$jml</td>";
                        }
                        echo "</tr>";
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
      
      
      
      
      <!--<div class="row">-->
      <!--  <div class="col-md-12">-->
      <!--    <div class="card" style="width:100%">-->
      <!--      <div class="card-header">-->
      <!--        <h3 class="mb-0">Penerimaan Pajak Reklame (Rp.)</h3>-->
      <!--      </div>-->
      <!--      <div class="table-responsive py-4">-->
      <!--        <table class="table table-flush">-->
      <!--          <thead class="thead-light">-->
      <!--            <tr>-->
      <!--              <th>Tahun</th>-->
      <!--              <th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>Mei</th><th>Jun</th>-->
      <!--              <th>Jul</th><th>Agu</th><th>Sep</th><th>Okt</th><th>Nov</th><th>Des</th>-->
      <!--            </tr>-->
      <!--          </thead>-->
      <!--          <tbody>-->
      <!--            <tr>-->
      <!--              <td>2019</td>-->
      <!--              <td>1.2 M</td><td>1.2 M</td><td>945 JT</td><td>2.3 M</td><td>850 JT</td><td>3.5 M</td><td>2.5 M</td><td>2.2 M</td><td>1.25 M</td><td></td><td></td><td></td>-->
      <!--            </tr>-->
      <!--            <tr>-->
      <!--              <td>2018</td>-->
      <!--              <td>2.2 M</td><td>945 JT</td><td>2.3 M</td><td>850 JT</td><td>3.5 M</td><td>800 JT</td><td>3 M</td><td>2.2 M</td><td>1.35 M</td><td>1.32 M</td><td>2.21 M</td><td>980 JT</td>-->
      <!--            </tr>-->
      <!--            <tr>-->
      <!--              <td>2017</td>-->
      <!--              <td>3.3 M</td><td>2.59 M</td><td>1.2 M</td><td>945 JT</td><td>2.3 M</td><td>850 JT</td><td>3.5 M</td><td>1.2 M</td><td>2.59 M</td><td>1.2 M</td><td>945 JT</td><td>2.3 M</td>-->
      <!--            </tr>-->
      <!--          </tbody>-->
      <!--        </table>-->
      <!--      </div>-->
      <!--    </div>-->
      <!--  </div>-->
      <!--</div>-->
      
      

      
      
</div>

