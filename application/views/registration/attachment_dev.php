
<?php if(count($attch) == 0) { redirect('applicants/billboard'); } ?>

<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
        <form method="POST" enctype="multipart/form-data">
            
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            
                            <?php foreach($attch as $rs) { ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            *<?=$rs->atc_name?>
                                        </label>
                                        <br>
                                        <input type="file" class="dropify" name="<?=$rs->type?>" data-height="240" data-default-file="<?=base_url()?>assets/image-upload/registration_attachments/<?=@$rs->file?>">
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <?php if(count($attch) > 0) { ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group text-right">
                                        <a href="<?=base_url()?>applicants/billboard" class="btn btn-warning">Kembali</a>
                                        <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    </form>
</div>


