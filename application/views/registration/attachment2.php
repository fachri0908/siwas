<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
        <form method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'billboard_design','billboard_design_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Desain Reklame</label>
                                    <input type="hidden" class="form-control" name="billboard_design_text" id="billboard_design_text">
                                    <input type="file" name="billboard_design" class="form-control" id="billboard_design">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'loc_front','location_front_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Peta Titik Lokasi (depan)</label>
                                    <input type="hidden" class="form-control" name="loc_front_text" id="location_front_text">
                                    <input type="file" name="loc_front" class="form-control" id="loc_front">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'loc_left','location_left_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Peta Titik Lokasi (kiri)</label>
                                    <input type="hidden" class="form-control" name="loc_left_text" id="location_left_text">
                                    <input type="file" name="loc_left" class="form-control" id="loc_left">
                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'loc_right','location_right_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Peta Titik Lokasi (kiri)</label>
                                    <input type="hidden" class="form-control" name="loc_right_text" id="location_right_text">
                                    <input type="file" name="loc_right" class="form-control" id="loc_right">
                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'aggreement_not_installed','aggreement_not_installed_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Pernyataan belum terpasang</label>
                                    <input type="hidden" class="form-control" name="aggreement_not_installed_text" id="aggreement_not_installed_text">
                                   <input type="file" name="aggreement_not_installed" class="form-control" id="aggreement_not_installed">
                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'pbb','pbb_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Pernyataan belum terpasang</label>
                                    <input type="hidden" class="form-control" name="pbb_text" id="pbb_text">
                                    <input type="file" name="pbb" class="form-control" id="pbb">
                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'power_of_attorney','power_of_attorney_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Kuasa</label>
                                    <input type="hidden" class="form-control" name="aggreement_not_installed_text" id="power_of_attorney_text">
                                    <input type="file" name="power_of_attorney" class="form-control" id="power_of_attorney">
                                    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'eligibility_permission','eligibility_permission_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Kuasa</label>
                                    <input type="hidden" class="form-control" name="eligibility_permission_text" id="eligibility_permission_text">
                                    <input type="file" name="eligibility_permission" class="form-control" id="eligibility_permission">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="reset" class="btn btn-warning">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script>
function testShow(val){
    console.log(val.value)
}
    function changeDesign(val, inputFile, inputText){
        let x=document.getElementById('billboard_design')
        console.log(x)
        if(val.checked){
            document.getElementById(inputText).value=""
            document.getElementById(inputFile).style.visibility='visible'
        }else{
            document.getElementById(inputText).value="doc"
            document.getElementById(inputFile).style.visibility='hidden'
        }
    }
</script>