<style>
    .stepwizard-step p {
        margin-top: 0px;
        color:#666;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        /*opacity: 1 !important;
        filter: alpha(opacity=100) !important;*/
    }
    .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
        opacity:1 !important;
        color:#bbb;
    }
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content:" ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-index: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
</style>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="container-fluid mt--6">
    
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    
    
   
    
    
    <form method="POST" id="registration_form" action="<?=base_url()?>registration/create" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-4">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Data Pemohon</h3>
                    </div>
                    <div class="card-body">
                            <div class="form-group">
                                <label class="form-control-label">Pemohon</label>
                                <select name="applicant" class="form-control" onchange="checkApplicant(this.value)" data-toggle="select">
                                    <option value="new">Baru</option>
                                    <?php foreach($applicants as $ap) { ?>
                                    <option value="<?=$ap->code?>"><?=$ap->name?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="new-applicant-box">
                            <div class="form-group">
                                <label class="form-control-label" for="">Nama</label>
                                <input type="text" name="name" class="form-control form-control-small" id="" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="">Alamat</label>
                                <input type="text" name="address" class="form-control form-control-small" id="" placeholder="Alamat">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="">Hp</label>
                                <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="HP">
                            </div>
                                            
                            
                        
                            
                            <div class="form-group">
                                <label class="form-control-label" for="">NPWP</label> <input type="hidden" name="npwp" id="npwpfile">
                                <div class="dropzone dropzone-single mb-3" id="npwpfile" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration/create_dev/npwp" data-form="registration_form" data-button-submit="saveandupload">
                                    <div class="fallback">
                                        
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="npwpfile">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="dz-preview dz-preview-single">
                                        <div class="dz-preview-cover">
                                        <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlInput1">KTP</label>  <input type="hidden" name="ktp" id="ktpfile">
                                <div class="dropzone dropzone-single mb-3" id="ktpfile" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration/create_dev/ktp" data-form="registration_form" data-button-submit="saveandupload">
                                    <div class="fallback">
                                        <div class="custom-file">
                                            <input type="file" name="ktp" class="custom-file-input" id="projectCoverUploads">
                                            <label class="custom-file-label" for="projectCoverUploads">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="dz-preview dz-preview-single">
                                        <div class="dz-preview-cover">
                                        <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-4">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label">Kategori</label>
                            <select class="from-control" name="category" onchange="getNsr()" data-toggle="select" id="category">
                                <option value="0"></option>
                                <option value="PRD">Produk</option>
                                <option value="NPRD">Non Produk</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Jenis Reklame</label>
                            <select class="from-control" name="billboard_type" data-toggle="select">
                                <option value="0"></option>
                                <?php foreach($types as $t){ ?>
                                <option value="<?=$t->code?>"><?=$t->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Sudut Pandang</label>
                            <select class="form-control" name="view_point">
                                <option value="0"></option>
                                <?php foreach($viewpoints as $s){ ?>
                                <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Kota</label>
                            <select class="form-control" data-toggle="select" name="city_code" onchange="changeCity(this.value)" id="city">
                                <option value="0"></option>
                                <?php foreach($cities as $s){ ?>
                                <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Kecamatan</label>
                            <select class="form-control" data-toggle="select" name="district_id" onchange="changeDistrict(this.value)" id="district">
                                <option value="0"></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Jalan</label>
                            <select class="form-control" data-toggle="select" name="street_id" onchange="changeStreet(this.value)" id="street">
                                <option value="0"></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Area Pemasangan</label>
                            <select class="form-control" name="street_class" onchange="getNsr()" id="street_class" readonly>
                                <option value="0"></option>
                                <?php foreach($street_class as $s){ ?>
                                <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" >Alamat Pemasangan</label>
                            <textarea class="form-control" name="install_address" id="" rows="4"></textarea>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Panjang</label>
                                <input class="form-control" type="text" name="length" value="1" onkeyup="getNsr()" placeholder="panjang (m)" id="length">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Lebar</label>
                                <input class="form-control" type="text" name="width" value="1" onkeyup="getNsr()" placeholder="lebar (m)" id="width">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Luas m<sup>2</sup></label>
                            <input type="text" class="form-control" name="size" type="text" placeholder="luas m/2" value="1" id="size">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Tinggi Media</label>
                            <input type="text" class="form-control" name="height" type="text" placeholder="tinggi media">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Permohonan</h3>
                    </div>
                    <div class="card-body">
                            <div class="form-group">
                                <label class="form-control-label" for="exampleFormControlSelect1">Tahun Tarif</label>
                                <select name="year_rate" class="form-control" data-toggle="select" id="year_rate" onchange="getNsr()" >
                                    <option value="0"></option>
                                    <?php foreach($rates as $r) { ?>
                                    <option value="<?=$r->rate?>"><?=$r->year?> | <?=$r->rate?>%</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="">Tanggal Pasang</label>
                                <input type="date" name="install_date" value="<?=date('Y-m-d')?>" class="form-control" id="install_date" onchange="getNsr()" placeholder="Tanggal Pasang">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="">Tanggal Selesai</label>
                                <input type="date" name="finish_date" value="<?=date('Y-m-d',strtotime('+1 year'))?>" class="form-control" id="finish_date" onchange="getNsr()" placeholder="Tanggal Selesai">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="">Lama Pemasangan (hari)</label>
                                <input type="number" name="install_duration" class="form-control" placeholder="" value="366" id="install_duration" onchange="getNsr()" readonly="">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">NSR</label>
                                <input type="text" class="form-control" name="nsr" type="text" placeholder="Nilai Sewa Reklame" id="nsr" readonly="">
                            </div>
                            <?php if($this->session->userdata('third_person')==1){ ?>
                            <div class="form-group">
                                <label class="form-control-label">Nilai Kontrak</label>
                                <input type="text" class="form-control" name="contract_value" type="text" placeholder="Nilai Kontrak" id="contract_value" readonly="">
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="form-control-label">Pajak</label>
                                <input type="text" class="form-control" name="tax" type="text" placeholder="Total pajak" id="tax" readonly="">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" id="saveandupload">Simpan</button>
                                <button type="reset" class="btn btn-warning">Batal</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    
     
<div class="container">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-1" type="button" class="btn btn-info">1</a>
                <p><small>Shipper</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-2" type="button" class="btn btn-info" disabled="disabled">2</a>
                <p><small>Destination</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-3" type="button" class="btn btn-info" disabled="disabled">3</a>
                <p><small>Schedule</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-4" type="button" class="btn btn-info" disabled="disabled">4</a>
                <p><small>Cargo</small></p>
            </div>
        </div>
    </div>
    
    
    <form role="form">
        <div class="panel panel-primary setup-content" id="step-1">
            <div class="panel-heading">
                 <h3 class="panel-title">Shipper</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">First Name</label>
                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Last Name</label>
                    <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name" />
                </div>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>
        
        
        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading">
                 <h3 class="panel-title">Destination</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Company Name</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Company Address</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address" />
                </div>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>
        
        
        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-heading">
                 <h3 class="panel-title">Schedule</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Company Name</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Company Address</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address" />
                </div>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>
        
        
        <div class="panel panel-primary setup-content" id="step-4">
            <div class="panel-heading">
                 <h3 class="panel-title">Cargo</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Company Name</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Company Address</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address" />
                </div>
                <button class="btn btn-success pull-right" type="submit">Finish!</button>
            </div>
        </div>
        
    </form>
</div>
</div>

<script>
    $(document).ready(function () {
    
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
    
        allWells.hide();
    
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);
    
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-danger').addClass('btn-info');
                $item.addClass('btn-danger');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });
    
        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;
    
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
    
            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });
    
        $('div.setup-panel div a.btn-danger').trigger('click');
    });
    function getNsr(){
        let cat = $('#category').val()
        let type= $('#street_class').val()
        let length= $('#length').val()
        let width= $('#width').val()
        let duration= $('#install_duration').val()
        $('#size').val(length*width)
        if(cat!=0 && type!=0){
            $.get("<?=base_url()?>calculator/getNsr?cat="+cat+"&class="+type, function(res){
                let size=length*width
                $("#nsr").val(res*size*duration)
                getTax(res*size*duration)
            });
        }
        getTotalDay()
        
    }
    function getTax(nsr){
        let rate=$('#year_rate').val()
        $('#tax').val((rate/100)*nsr)
    }
    
    function getTotalDay(){
        let first=$('#install_date').val()
        let last=$('#finish_date').val()
        first=new Date(first)
        last=new Date(last)
        let diff = last.getTime() - first.getTime();
        diff = diff / (1000 * 3600 * 24);
        $('#install_duration').val(diff)
    }
    function checkApplicant(val){
        if(val=='new'){
            document.getElementById('new-applicant-box').style.display='block'
        }else{
            document.getElementById('new-applicant-box').style.display='none'
        }
    }
    
    function changeCity(val){
        $.get("<?=base_url()?>lists/districtByCity/"+val, function(res){
            res=JSON.parse(res)
            $("#district").empty();
            $("#district").append(new Option('',''));
            res.forEach(function(el){
                $("#district").append(new Option(el.name,el.id));
            })
        });
    }
    
    function changeDistrict(val){
        $.get("<?=base_url()?>lists/streetListByDistrict/"+val, function(res){
            res=JSON.parse(res)
            $("#street").empty();
            $("#street").append(new Option('',''));
            res.forEach(function(el){
                $("#street").append(new Option(el.name,el.id));
            })
        });
    }
    function changeStreet(val){
        $.get("<?=base_url()?>lists/streetClassByStreet/"+val, function(res){
            $('#street_class').val(res)
        });
    }
</script>