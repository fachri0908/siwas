<style>
    .stepwizard-step p {
        margin-top: 0px;
        color:#666;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        /*opacity: 1 !important;
        filter: alpha(opacity=100) !important;*/
    }
    .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
        opacity:1 !important;
        color:#bbb;
    }
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content:" ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-index: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
    
    /*.has-error > input, .has-error > select, .has-error > textarea {*/
    /*    border: 1px solid red;*/
    /*}*/
              
  #map { 
    height: 400px;    
    width: 100%;            
  }       
</style>


<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="container-fluid mt--6">
    
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    
    
    
    
<div class="row">
<div class="col-lg-12">
<div class="card-wrapper">
<div class="card">
 

<div class="container-fluid mb-4 mt-5">

    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-1" type="button" class="btn btn-danger">1</a>
                <p><small>Data Pemohon</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-2" type="button" class="btn btn-info" disabled="disabled">2</a>
                <p><small>Titik Detail Reklame</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-3" type="button" class="btn btn-info" disabled="disabled">3</a>
                <p><small>Data Objek Reklame</small></p>
            </div>
            
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-4" type="button" class="btn btn-info" disabled="disabled">4</a>
                <p><small>Atribut Reklame</small></p>
            </div>
            
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-5" type="button" class="btn btn-info" onclick="showNsr()" disabled="disabled">5</a>
                <p><small>Nilai Sewa Reklame</small></p>
            </div>
            
        </div>
    </div>
    
    <form role="form" method="POST" action="<?=base_url()?>registration/update/<?=$billboard[0]->code?>" enctype="multipart/form-data">
        <div class="panel panel-primary setup-content" id="step-1">
            <div class="panel-heading">
                 <h3 class="panel-title">Data Pemohon</h3>
            </div>
            <div class="panel-body">
                
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Tipe Pemohon</label>
                    </div>
                    <div class="col-md-9">
                        
                        
                        <!--<select class="form-control" name="applicant_type" data-toggle="select" id="applicant_type" required>-->
                        <!--    <option value=""> :: Pilih Type Pemohon :: </option>-->
                        <!--    <option value="Personal"> Personal </option>-->
                        <!--    <option value="Badan Usaha"> Badan Usaha </option>-->
                        <!--</select>-->
                        
                        
                        <div class="row">
                            <div class="col-md-3">
                                <!--<input type="radio" name="applicant_code" value="PSN"><label> Personal </label>-->
                                <div class="custom-control custom-radio mb-3">
                                    <input name="applicant_type" class="custom-control-input" id="customRadio5" checked="" type="radio" value="PSN" onclick="isbdu(this.value)">
                                    <label class="custom-control-label" for="customRadio5">Personal</label>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <!--<input type="radio" name="applicant_code" value="PSN"><label> Personal </label>-->
                                <div class="custom-control custom-radio mb-3">
                                    <input name="applicant_type" class="custom-control-input" id="customRadio6"  type="radio" value="BDU" onclick="isbdu(this.value)">
                                    <label class="custom-control-label" for="customRadio6">Badan Usaha</label>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Nama</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="hidden" name="applicant_code" required="required" class="form-control" placeholder="Code" value="<?=$applicants[0]->code?>" />
                        <input maxlength="100" type="text" name="name" required="required" class="form-control" placeholder="Nama" value="<?=$applicants[0]->name?>" />
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Alamat</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="text" name="address" required="required" class="form-control" placeholder="Alamat" value="<?=$applicants[0]->address?>" />
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">No. HP</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="numbe" name="phone_number" required="required" class="form-control" placeholder="No. Hp" value="<?=$applicants[0]->phone_number?>" />
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Email</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="email" name="email" required="required" class="form-control" placeholder="Email" value="<?=$applicants[0]->email?>" />
                    </div>
                </div>
                
                
                
                
                <div class="row mt-3 bdu">
                    <div class="col-md-3">
                        <label class="control-label">Nama Perusahaan</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="text" name="company_name" class="form-control" placeholder="Nama Perusahaan" value="<?=$billboard[0]->company_name?>" />
                    </div>
                </div>
                
                <!--<div class="row mt-3 bdu">-->
                <!--    <div class="col-md-3">-->
                <!--        <label class="control-label">Jabatan</label>-->
                <!--    </div>-->
                <!--    <div class="col-md-9">-->
                <!--        <input maxlength="100" type="text" name="company_position" class="form-control" placeholder="Jabatan" value="<?=$applicants[0]->company_position?>" />-->
                <!--    </div>-->
                <!--</div>-->
                
                
                <div class="row mt-3 bdu">
                    <div class="col-md-3">
                        <label class="control-label">Alamat Perusahaan</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="text" name="company_address" class="form-control" placeholder="Alamat Perusahaan" value="<?=$billboard[0]->company_address?>" />
                    </div>
                </div>
                
                <div class="row mt-3 bdu">
                    <div class="col-md-3">
                        <label class="control-label">Telepon</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="text" name="company_phone" class="form-control" placeholder="Telepon" value="<?=$billboard[0]->company_phone?>" />
                    </div>
                </div>
                
                <div class="row mt-3 bdu">
                    <div class="col-md-3">
                        <label class="control-label">Fax</label>
                    </div>
                    <div class="col-md-9">
                        <input maxlength="100" type="text" name="company_fax" class="form-control" placeholder="Fax" value="<?=$billboard[0]->company_fax?>" />
                    </div>
                </div>
                
                
                
                
                <div class="row mt-5">
                    <div class="col-md-12">
                        <a class="btn btn-primary nextBtn pull-right" type="button" style="float: right">Next</a>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading">
                 <h3 class="panel-title">Titik Detail Reklame</h3>
            </div>
            <div class="panel-body">
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Kategori</label>
                    </div>
                    <div class="col-md-9">
                        <select class="from-control" name="category" onchange="getNsr()" data-toggle="select" id="category" required>
                            <option value="0"></option>
                            <option <?=($billboard[0]->category=="PRD")?'selected':''?> value="PRD">Produk</option>
                            <option <?=($billboard[0]->category=="NPRD")?'selected':''?> value="NPRD">Non Produk</option>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3" id="product_type" style="visibility:<?=($billboard[0]->category=="PRD")?'visible':''?>">
                    <div class="col-md-3">
                        <label class="control-label">Tipe Produk</label>
                    </div>
                    <div class="col-md-9">
                        <select name="product_type" class="form-control">
                            <option <?=($billboard[0]->product_type=="RKAL")?'selected':''?> value="RKAL">Rokok / Minuman Ber Alkohol</option>
                            <option <?=($billboard[0]->product_type=="NRKAL")?'selected':''?> value="NRKAL">Bukan Rokok/ Minuman Ber Alkohol</option>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Jenis Reklame</label>
                    </div>
                    <div class="col-md-9">
                        <select class="from-control" name="billboard_type" data-toggle="select" required onchange="jenis_reklame(this.value)" id="billboard_type">
                            <option value="0"></option>
                            <?php foreach($types as $t){ ?>
                            <option <?=($billboard[0]->billboard_type==$t->code)?'selected':''?> value="<?=$t->code?>"><?=$t->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Kota</label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" data-toggle="select" name="city_code" onchange="changeCity(this.value)" id="city" required>
                            <?php foreach($cities as $s){ ?>
                            <option <?=($billboard[0]->city_code==$s->code)?'selected':''?> value="<?=$s->code?>"><?=$s->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Kecamatan</label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" data-toggle="select" name="district_id" onchange="changeDistrict(this.value)" id="district" required>
                            <?php foreach($districts as $s){ ?>
                            <option <?=($billboard[0]->district_id==$s->id)?'selected':''?> value="<?=$s->id?>"><?=$s->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Jalan</label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" data-toggle="select" name="street_id" onchange="changeStreet(this.value)" id="street" required>
                            <?php foreach($streets as $s){ ?>
                            <option <?=($billboard[0]->street_id==$s->id)?'selected':''?> value="<?=$s->id?>"><?=$s->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <div id="map"></div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Latitude, Longitude</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="latitude_longitude" value="<?=$billboard[0]->latitude?>,<?=$billboard[0]->longitude?>" id="latlong" required="">
                    </div>
                </div>
                <div class="row mt-3" id="hide-street_class">
                    <div class="col-md-3">
                        <label class="control-label">Area Pemasangan</label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" name="street_class" onchange="getNsr()" id="street_class" readonly required="">
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Alamat Pemasangan</label>
                    </div>
                    <div class="col-md-9">
                        <textarea class="form-control" name="install_address" id="install-address" rows="4" required readonly><?=$billboard[0]->address?></textarea>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Detail Lokasi</label>
                    </div>
                    <div class="col-md-9">
                        <textarea class="form-control" name="detail_location" id="" rows="4" required><?=$billboard[0]->detail_location?></textarea>
                    </div>
                </div>
                
                <div class="row mt-5">
                    <div class="col-md-12">
                        <a class="btn btn-primary nextBtn pull-right" type="button" style="float: right">Next</a>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-heading">
                 <h3 class="panel-title">Data Objek Reklame</h3>
            </div>
            <div class="panel-body">
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Tahun Tarif</label>
                    </div>
                    <div class="col-md-9">
                        <select name="year_rate" class="form-control" data-toggle="select" id="year_rate" onchange="getNsr()" required>
                            <option value="0"></option>
                            <?php foreach($rates as $r) { ?>
                            <option selected value="<?=$r->rate?>"><?=$r->year?> | <?=$r->rate?>%</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Status Tanah</label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" name="land_status" data-toggle="select" id="land_status" required>
                            <option value="0"></option>
                            <?php foreach($land_status as $s){ ?>
                                <option <?=($billboard[0]->land_status==$s->code)?'selected':''?> value="<?=$s->code?>"><?=$s->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label"> Letak Reklame </label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" name="placement_billboard" data-toggle="select" id="placement_billboard" required>
                            <option value="0"></option>
                            <?php foreach($placement_billboard as $s){ ?>
                                <option <?=($billboard[0]->placement_billboard==$s->code)?'selected':''?> value="<?=$s->code?>"><?=$s->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Melekat Pada Bangunan</label>
                    </div>
                    <div class="col-md-9">
                        <select name="attached_to_building" class="form-control" id="attached_to_building">
                            <option <?=($billboard[0]->attached_to_building==0)?'selected':''?> value="0">Tidak</option>
                            <option <?=($billboard[0]->attached_to_building==1)?'selected':''?> value="1">Ya</option>
                        </select>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Tanggal Pasang</label>
                    </div>
                    <div class="col-md-9">
                        <input type="date" name="install_date" value="<?=$billboard[0]->install_date?>" class="form-control" id="install_date" onchange="getNsr()"  onkeyup="getNsr()" placeholder="Tanggal Pasang" required>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Tanggal Selesai</label>
                    </div>
                    <div class="col-md-9">
                        <input type="date" name="finish_date" value="<?=$billboard[0]->finish_date?>" class="form-control" id="finish_date" onchange="getNsr()"  onkeyup="getNsr()" placeholder="Tanggal Selesai" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Lama Pemasangan (hari)</label>
                    </div>
                    <div class="col-md-9">
                        <input type="number" name="install_duration"  class="form-control" placeholder="" value="<?=$billboard[0]->install_duration?>" id="install_duration" onchange="getNsr()" readonly="" required>
                    </div>
                </div>
                
                
                <!--menampilkan detik-->
                <div class="row mt-3" for="hide-time_duration">
                    <div class="col-md-3">
                        <label class="control-label">Waktu Tayang</label>
                    </div>
                    <div class="col-md-3">
                        <input type="time" name="start_time" class="form-control" placeholder="" id="t1" onchange="getTimeDuration()" value="<?=@$billboard[0]->start_time?>" onkeyup="getTimeDuration()">
                    </div>
                    <div class="col-md-3">
                        <input type="time" name="end_time" class="form-control" placeholder="" id="t2" onchange="getTimeDuration()" value="<?=@$billboard[0]->end_time?>" onkeyup="getTimeDuration()">
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="" id="tt" readonly>
                    </div>
                </div>
                <!--end menampilkan detik-->
                
                
                <div class="row mt-3" for="hide-time_duration">
                    <div class="col-md-3">
                        <label class="control-label">Durasi</label>
                    </div>
                    <div class="col-md-9">
                        <input type="number" name="time_duration" value="<?=@$billboard[0]->time_duration?>" class="form-control" placeholder="" id="time_duration" onchange="getNsr()">
                    </div>
                </div>
                
                
                
                <div class="row mt-3" for="hide-total">
                    <div class="col-md-3">
                        <label class="control-label"> Jumlah </label>
                    </div>
                    <div class="col-md-9">
                        <input type="number" name="total" class="form-control" value="<?=$billboard[0]->total?>" placeholder="" id="total" onchange="getNsr()">
                    </div>
                </div>
                
                
                <div class="row mt-5">
                    <div class="col-md-12">
                        <a class="btn btn-primary nextBtn pull-right" type="button" style="float: right">Next</a>
                    </div>
                </div>
                
                
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-4">
            <div class="panel-heading">
                 <h3 class="panel-title">Atribut Reklame</h3>
            </div>
            <div class="panel-body">
                
                
                <!--<div class="row mt-3">-->
                <!--    <div class="col-md-3">-->
                <!--        <label class="control-label">Sudut Pandang</label>-->
                <!--    </div>-->
                <!--    <div class="col-md-9">-->
                <!--        <select class="form-control" name="view_point" required onchange="viewpoint(this.value)">-->
                <!--            <option value="0"></option>-->
                <!--            <?php foreach($viewpoints as $s){ ?>-->
                <!--            <option <?=($billboard[0]->view_point==$s->code)?'selected':''?> value="<?=$s->code?>"><?=$s->name?></option>-->
                <!--            <?php } ?>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                
                
                <!--<div class="row mt-3" after="attach_viewpoint">-->
                <!--    <div class="col-md-3">-->
                <!--        <label class="control-label"> Tampilan </label>-->
                <!--    </div>-->
                <!--    <div class="col-md-9">-->
                <!--        <select class="form-control" data-toggle="select" name="billboard_design[]" jml=3 id="pilihtampilan" multiple="multiple" onchange="addfileviewpoint()">-->
                <!--            <option <?=(strpos($billboard[0]->billboard_design, 'depan') !== false)?'selected':''?> value="depan">Depan</option>-->
                <!--            <option <?=(strpos($billboard[0]->billboard_design, 'belakang') !== false)?'selected':''?> value="belakang">Belakang</option>-->
                <!--            <option <?=(strpos($billboard[0]->billboard_design, 'kanan') !== false)?'selected':''?> value="kanan">Kanan</option>-->
                <!--            <option <?=(strpos($billboard[0]->billboard_design, 'kiri') !== false)?'selected':''?> value="kiri">Kiri</option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                
                
                
                <!--<div class="row mt-3">-->
                <!--    <div class="col-md-3">-->
                <!--        <label class="control-label"> Desain Reklame </label>-->
                <!--    </div>-->
                <!--    <div class="col-md-9">-->
                <!--        <div class="row mb-2">-->
                <!--            <div class="col-md-3">-->
                <!--                Tampak Depan-->
                <!--            </div>-->
                            
                <!--            <div class="col-md-3">-->
                <!--                Tampak Belakang-->
                <!--            </div>-->
                            
                <!--            <div class="col-md-3">-->
                <!--                Tampak Kiri-->
                <!--            </div>-->
                            
                <!--            <div class="col-md-3">-->
                <!--                Tampak Kanan-->
                <!--            </div>-->
                <!--        </div>-->
                <!--        <div class="row">-->
                <!--            <div class="col-md-3">-->
                <!--                <input type="file" class="dropify" name="tampakdepan" data-height="240">-->
                <!--            </div>-->
                            
                            <!--<div class="col-md-3">-->
                                <!--<input type="file" class="dropify" name="tampakbelakang" data-height="240">-->
                            <!--</div>-->
                            
                <!--            <div class="col-md-3">-->
                <!--                <input type="file" class="dropify" name="tampakkiri" data-height="240">-->
                <!--            </div>-->
                            
                <!--            <div class="col-md-3">-->
                <!--                <input type="file" class="dropify" name="tampakkanan" data-height="240">-->
                <!--            </div>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Sudut Pandang</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" name="view_point" required onchange="viewpoint(this.value)">
                                <?php foreach($viewpoints as $s){ ?>
                                <option <?=($billboard[0]->view_point==$s->code)?'selected':''?> value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row nt-3">
                    <div class="col-md-3">
                        Ukuran
                    </div>
                    <div class="col-md-3 text-right">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text" style="color:#000">P</span>
                          </div>
                          <input class="form-control" type="number" name="length" value="" onkeyup="getNsr()" value="<?=$billboard[0]->length?>" placeholder="panjang (m)" id="length" required>
                        </div>
                    </div>
                    <div class="col-md-3 text-right">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text" style="color:#000">L</span>
                          </div>
                          <input class="form-control" type="number" name="width" value="" onkeyup="getNsr()" value="<?=$billboard[0]->size?>" placeholder="lebar (m)" id="width" required>
                        </div>
                    </div>
                    <div class="col-md-3 text-right">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text" style="color:#000">T</span>
                          </div>
                          <input type="number" class="form-control" name="height" type="text" onkeyup="getNsr()" value="<?=$billboard[0]->height?>" placeholder="Tinggi Media" required id="height">
                        </div>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Luas</label>
                    </div>
                    <div class="col-md-9">
                        <input type="number" class="form-control" name="size" type="text" placeholder="luas m/2" value="<?=$billboard[0]->size?>" id="size" readonly required>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label class="control-label">Deskripsi Reklame</label>
                    </div>
                    <div class="col-md-9">
                        <textarea class="form-control" name="billboard_text" rows="3"><?=$billboard[0]->billboard_text?></textarea>
                    </div>
                </div>
                
                
                <div class="row mt-5">
                    <div class="col-md-12">
                        <a class="btn btn-primary nextBtn pull-right" type="button" style="float: right">Next</a>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        <div class="panel panel-primary setup-content" id="step-5">
            <div class="panel-heading">
                 <h3 class="panel-title">Nilai Sewa Reklame</h3>
            </div>
            <div class="panel-body">
                
                <?php if($this->session->userdata('third_person')==1){ ?>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Nilai Kontrak</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" name="contract_value" value="<?=$billboard[0]->contract_value?>" type="text" placeholder="Nilai Kontrak Reklame" id="contract_value"  onkeyup="changeConttract()">
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">NSR</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" name="nsr" type="text" value="<?=$billboard[0]->nsr?>" placeholder="Nilai Sewa Reklame" id="nsr" readonly="" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Pajak</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" name="tax" type="text" <?=$billboard[0]->tax?> placeholder="Total pajak" id="tax" readonly="" required>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12">
                        <button class="btn btn-info pull-right m-2" type="submit" id="simpan" style="float: right" value=1 name="simpan"> Simpan </button>
                    </div>
                </div>
                
            </div>
        </div>
        
    </form>
</div>




</div>
</div>
</div>
</div>

    
</div>

<script type="text/javascript">
    var geocoder;
    var map;
    function init() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(<?=$billboard[0]->latitude?>,<?=$billboard[0]->longitude?>);
        
        map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 17,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDoubleClickZoom: true,
        });
        
        // Update lat/long value of div when anywhere in the map is clicked    
        google.maps.event.addListener(map,'click',function(event) {                
            document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
        });
           
        var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          title: latitude + ', ' + longitude 
        });    
        
        // Update lat/long value of div when the marker is clicked
        marker.addListener('click', function(event) {              
            document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
        });
        
        // Create new marker on double click event on the map
        google.maps.event.addListener(map,'dblclick',function(event) {
            var marker = new google.maps.Marker({
              position: event.latLng, 
              map: map, 
              title: event.latLng.lat()+', '+event.latLng.lng()
            });
            
            // Update lat/long value of div when the marker is clicked
            marker.addListener('click', function() {
              document.getElementById('latlong').value = event.latLng.lat()+','+event.latLng.lng();
            });            
        });
    }
    
    function codeAddress(address) {
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: results[0].geometry.location
            });
          } else {
            alert("Geocode was not successful for the following reason: " + status);
          }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmOv0twOwgE0VlvmWnmtb1B8f1KYnbR9E&callback=init"
    async defer></script>

<script>
    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
            allWells.hide();
            $('#step-1').show();
    
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-danger').addClass('btn-info');
                $item.addClass('btn-danger');
                allWells.hide();
                $target.show();
                // $target.find('input:eq(0)').focus();
            }
        });
    
        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                // curInputs = curStep.find("input[type='text'], input[type='url']"),
                curInputs = curStep.find("input[type='text'],input[type='number'], select, textarea"),
                isValid = true;
    
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
            
            if($("#step-5").is(":visible")){
                showNsr();
            }
            
        });
        $('div.setup-panel div a.btn-danger').trigger('click');
        
        $.get("<?=base_url()?>lists/streetClassByStreet/<?=$billboard[0]->street_id?>", function(res){
            $('#street_class').append(res)
        });
        
        
    });
    
    
    
    function getNsr(){
        let cat = $('#category').val()
        let billboard_type= $('#billboard_type').val()
        let billboard_type_id = $('#billboard_type option:selected').attr('id')
        let type = $('#street_class').val()
        let street_class_id = $('#street_class option:selected').attr('id')
        let length= $('#length').val()
        let width= $('#width').val()
        let height= $('#height').val()
        let time_duration= $('#time_duration').val()
        let duration= $('#install_duration').val()
        let total= $('#total').val()
        let size=length*width;
        
        if(cat=="PRD"){
            $('#product_type').show()
        }else{
            $('#product_type').hide()
        }
        
        $("#size").val(size);
            $.ajax({
                url: "<?=base_url()?>registration/calculator",
                type: "post",
                data: {
                    'category'          : cat,
                    'billboard_type'    : billboard_type,
                    'billboard_type_id' : billboard_type_id,
                    'street_class'      : type,
                    'street_class_id'   : street_class_id,
                    'length'            : length,
                    'width'             : width,
                    'install_duration'  : duration,
                    'time_duration'     : time_duration,
                    'total'             : total,
                },
                success: function (res) {
                    $("#nsr").val(res);
                    getTax(res);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
                }
            });
        getTotalDay()
        // if($("#nsr").val() == '') {
        //     $("button[id='submit']").attr('disabled','disabled');
        // } else {
        //     if(billboard_type == 'STK' && $("#nsr").val() >= 1000000) {
        //         $("button[id='submit']").removeAttr('disabled');
        //     }
        //     if (billboard_type == 'SLB' && $("#nsr").val() >= 10000000) {
        //         $("button[id='submit']").removeAttr('disabled');
        //     }
        //     if(billboard_type != 'STK' || billboard_type != 'SLB') {
        //         $("button[id='submit']").removeAttr('disabled');
        //     }
        // }
    }
    // $("button[id='submit']").attr('disabled','disabled');
    
    
    function getTax(nsr){
        let rate=$('#year_rate').val()
        $('#tax').val((rate/100)*nsr)
    }
    
    
    function getTotalDay(){
        let first=$('#install_date').val()
        let last=$('#finish_date').val()
        first=new Date(first)
        last=new Date(last)
        let diff = last.getTime() - first.getTime();
        diff = diff / (1000 * 3600 * 24);
        $('#install_duration').val(diff)
    }
    
    function checkApplicant(val){
        if(val=='new'){
            document.getElementById('new-applicant-box').style.display='block'
        }else{
            document.getElementById('new-applicant-box').style.display='none'
        }
    }
    
    function changeCity(val){
        $.get("<?=base_url()?>lists/districtByCity/"+val, function(res){
            res=JSON.parse(res)
            $("#district").empty();
            $("#district").append(new Option('',''));
            res.forEach(function(el){
                $("#district").append(new Option(el.name,el.id));
            })
        });
    }
    
    function changeDistrict(val){
        $.get("<?=base_url()?>lists/streetListByDistrict/"+val, function(res){
            res=JSON.parse(res)
            $("#street").empty();
            $("#street").append(new Option('',''));
            res.forEach(function(el){
                $("#street").append(new Option(el.name,el.id));
            })
        });
    }
    function changeStreet(val){
        $.get("<?=base_url()?>lists/streetClassByStreet/"+val, function(res){
            $('#street_class').html('');
            $('#street_class').append(res)
        });
        $.get("<?=base_url()?>lists/streetClassByStreetName/"+val, function(data){
            codeAddress(data)
        });
        $.get("<?=base_url()?>lists/streetAddress/"+val, function(res){
            $('#install-address').val(res)
        });
    }
    
    function jenis_reklame(e)  {
        if(e == 'SR' || e == 'FLM' || e == 'LED') {
            $("div[for='hide-time_duration']").show();
            $("#time_duration").attr('required','required');
        } else {
            $("#time_duration").removeAttr('required');
            $("div[for='hide-time_duration']").hide();
        }
        
        if(e == 'PRG' || e == 'SLB') {
            $("div[for='hide-total']").show();
            $("#total").attr('required','required');
        } else {
            $("#total").removeAttr('required');
            $("div[for='hide-total']").hide();
        }
    }
    $("div[for='hide-time_duration']").hide();
    $("div[for='hide-total']").hide();
    
    
    
    function seett(e) {
        $('#pilihtampilan').select2({
            maximumSelectionLength: e
        });
    }
    
    function viewpoint(e) {
        seett(e.substr(-1));
    }
    
    
    function addfileviewpoint() {
        var x = $('#pilihtampilan').val()+'';
        var y = x.split(",");
        $('.hre').remove();
        var width = 12/y.length;
        
        var arah = '';
        var file = '';
        for(i=1; i <= y.length; i++) {
            arah += `
                    <div class="col-md-`+width+`">
                        <input type="file" class="dropify" name="tampak[]" data-height="240">
                    </div>
                `;
        }
        var as = `
                    <div class="row mt-3 hre">
                        <div class="col-md-3"> <label class="control-label"> Desain Reklame </label> </div>
                        <div class="col-md-9">
                            <div class="row">
                            `+arah+`
                            </div>
                        </div>
                    </div>
        `;
        
        
        $("[after='attach_viewpoint']").after(as);
        
        
        $('.dropify').dropify('refresh')
        
    }
    
    
    
    
    
                
                
    // function viewpoint(e) {
    //     // alert(e.substr(-1));
    //     $('.hre').remove();
    //     var width = 12/e.substr(-1);
    //     alert(width);
        
    //     var arah = '';
    //     var file = '';
    //     for(i=1; i <= e.substr(-1); i++) {
    //         arah += `
    //                 <div class="col-md-`+width+`">
    //                     <input type="file" class="dropify" name="tampakbelakang" data-height="240">
    //                 </div>
    //             `;
    //     }
        
    //     var as = `
    //                 <div class="row mt-3 hre">
    //                     <div class="col-md-3"> <label class="control-label"> Desain Reklame </label> </div>
    //                     <div class="col-md-9">
    //                         <div class="row">
    //                         `+arah+`
    //                         </div>
    //                     </div>
    //                 </div>
    //     `;
        
    //     $("[after='attach_viewpoint']").after(as);
    //     $('.dropify').dropify('refresh')
    // }
    
    
    
   
    
    
    
    function isbdu(e) {
        if(e == 'BDU') {
            $(".bdu").show();
        } else {
            $(".bdu").hide();
        }
    } $(".bdu").hide();
    
    function showNsr(){
        let cat = $('#category').val()
        let b_type= $('#billboard_type').val()
        let street_class = $('#street_class').val()
        let length= $('#length').val()
        let width= $('#width').val()
        let size=length*width
        let height= $('#height').val()
        let p_type= $('#product_type').val()
        let location= $('#placement_billboard').val()
        let days= $('#install_duration').val()
        let time= $('#time_duration').val()
        let total= $('#total').val()
        if(p_type=='RKAL'){
            p_type=1
        }else{
            p_type=0
        }
        let url='lists/getnsr?area='+street_class+'&type='+b_type+'&category='+cat+'&cigarette='+p_type+'&location='+location+'&size='+size+'&height='+height+'&total_days='+days+'&total_seconds='+time+'&total='+total
        console.log(url)
        $.get('<?=base_url()?>'+url,function(res){
            $('#nsr').val(res)
            getTax()
        })
    }
    
    function getTax(){
        let nsr=$('#nsr').val()
        let total=$('#contract_value').val()
        $.get('<?=base_url()?>lists/gettax?nsr='+nsr+'&contract='+total,function(res){
            $('#tax').val(res)
        })
    }
    
    function changeConttract(){
        getTax()
    }
    
    
    
    function getTimeDuration() {
        var t1 = document.getElementById('t1').value;
        var t2 = document.getElementById('t2').value;
        var dur = document.getElementById('install_duration').value;
        var tt1 = t1.split(':');
        var ttt1 = tt1[1].split(' ');
        var tt2 = t2.split(':');
        var ttt2 = tt2[1].split(' ');
        var menit = ttt2[0] - ttt1[0];
        var jam = tt2[0] - tt1[0];
        if (menit < 0) {
            menit = 60 + menit;
            jam = jam - 1;
        }
        var second = (jam * 3600) + (menit * 60);
        document.getElementById('tt').value = jam + " jam " + menit + " menit, atau " + second + " Second";
        document.getElementById('time_duration').value = second * dur;
    }
    
    
    
        var e = document.getElementById('billboard_type').value;
        if(e == 'SR' || e == 'FLM' || e == 'LED') {
            $("div[for='hide-time_duration']").show();
            $("#time_duration").attr('required','required');
        } else {
            $("#time_duration").removeAttr('required');
            $("div[for='hide-time_duration']").hide();
        }
        
    
</script>