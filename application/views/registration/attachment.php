<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
        <form method="POST" action="<?=base_url()?>registration/create" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'billboard_design_box','billboard_design_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Desain Reklame</label>
                                    <input type="hidden" class="form-control" name="billboard_design" id="billboard_design_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="billboard_design_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="billboard_design" class="custom-file-input" id="billboard_design">
                                            <label class="custom-file-label" for="billboard_design">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'location_front_box','location_front_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Peta Titik Lokasi (depan)</label>
                                    <input type="hidden" class="form-control" name="loc_front" id="location_front_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="location_front_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="loc_front" class="custom-file-input" id="loc_front">
                                            <label class="custom-file-label" for="loc_front">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'location_left_box','location_left_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Peta Titik Lokasi (kiri)</label>
                                    <input type="hidden" class="form-control" name="loc_left" id="location_left_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="location_left_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="loc_left" class="custom-file-input" id="loc_left">
                                            <label class="custom-file-label" for="loc_left">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'location_right_box','location_right_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Peta Titik Lokasi (kiri)</label>
                                    <input type="hidden" class="form-control" name="loc_right" id="location_right_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="location_right_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="loc_right" class="custom-file-input" id="loc_right">
                                            <label class="custom-file-label" for="loc_right">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'aggreement_not_installed_box','aggreement_not_installed_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Pernyataan belum terpasang</label>
                                    <input type="hidden" class="form-control" name="aggreement_not_installed" id="aggreement_not_installed_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="aggreement_not_installed_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="aggreement_not_installed" class="custom-file-input" id="aggreement_not_installed">
                                            <label class="custom-file-label" for="aggreement_not_installed">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'pbb_box','pbb_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Pernyataan belum terpasang</label>
                                    <input type="hidden" class="form-control" name="pbb" id="pbb_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="pbb_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="pbb" class="custom-file-input" id="pbb">
                                            <label class="custom-file-label" for="pbb">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'power_of_attorney_box','power_of_attorney_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Kuasa</label>
                                    <input type="hidden" class="form-control" name="aggreement_not_installed" id="power_of_attorney_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="power_of_attorney_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="power_of_attorney" class="custom-file-input" id="power_of_attorney">
                                            <label class="custom-file-label" for="power_of_attorney">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="custom-toggle">
                                        <input type="checkbox" onchange="changeDesign(this,'eligibility_permission_box','eligibility_permission_text')" checked>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="doc" data-label-on="file"></span>
                                    </label><br>
                                    <label class="form-control-label" for="exampleFormControlInput1">Surat Kuasa</label>
                                    <input type="hidden" class="form-control" name="eligibility_permission" id="eligibility_permission_text">
                                    <div class="dropzone dropzone-single mb-3" data-toggle="dropzone" data-dropzone-url="<?=base_url()?>registration" id="eligibility_permission_box">
                                        <div class="fallback">
                                            <div class="custom-file">
                                            <input type="file" name="eligibility_permission" class="custom-file-input" id="eligibility_permission">
                                            <label class="custom-file-label" for="eligibility_permission">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="dz-preview dz-preview-single">
                                            <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="" alt="..." data-dz-thumbnail>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="reset" class="btn btn-warning">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<script>
function testShow(val){
    console.log(val.value)
}
    function changeDesign(val, inputFile, inputText){
        let x=document.getElementById('billboard_design')
        console.log(x)
        if(val.checked){
            document.getElementById(inputText).value=""
            document.getElementById(inputFile).style.visibility='visible'
        }else{
            document.getElementById(inputText).value="file"
            document.getElementById(inputFile).style.visibility='hidden'
        }
    }
</script>