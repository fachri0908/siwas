<style>    
    #map { 
        height: 600px;    
        width: 100%;            
    }
    .modal-full {
        min-width: 97%;
        margin: 20px 20px 20px 20px;
    }
</style>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <button type="button" onclick="showFilter()" style="width: 100% !important" class="btn btn-info"> <i class="fa fa-map"></i> Filter Lokasi</button>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="illegalModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h1 class="w-100 text-center">Detail Reklame Illegal</h1>
            <button type="button" class="close" onclick="$('#illegalModal').modal('hide')">&times;</button>
        </div>
        <div class="modal-body" style="margin-top: -40px !important">
            <div class="row">
                <div class="col-md-6">
                    <div class="table-responsive py-4" style="font-size: 18px !important">
                        <h1>Data Lokasi</h1>
                        <table class="table" id="datatable-buttons">
                            <tr>
                                <td width="30%">Kota</td>
                                <td>: <span id="illegal-city"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Kecamatan</td>
                                <td>: <span id="illegal-district"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Jalan</td>
                                <td>: <span id="illegal-street"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive py-4" style="font-size: 15px">
                        <h1>Teks Reklame</h1>
                        <table class="table" id="datatable-buttons">
                            <tr>
                                <td width="30%">Jenis Billboard</td>
                                <td>: <span id="illegal-type"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Teks</td>
                                <td>: <span id="illegal-billboard-text"></span></td>
                            </tr>
                        </table>
                        <div id="illegal-image-box">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>



<div class="modal animated fadeIn" id="modal-filter">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h1 class="modal-title">Filter Lokasi</h1>
        </div>
        <div class="modal-body" style="margin-top: -20px">
            <form class="form-horizontal">
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Nomor Formulir</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="no-formulir-filter">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Status</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" data-toggle="select" id="status-filter">
                                <option value=""></option>
                                <option value="1">Disetujui</option>
                                <option value="2">Tidak Disetujui</option>
                                <option value="0">Menunggu Persetujuan</option>
                                <option value="3">Kadaluwarsa</option>
                                <option value="4">Ilegal</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Kota</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" data-toggle="select" onchange="changeCity(this.value)" id="city-filter" required>
                                <option value=""></option>
                                <?php foreach($cities as $s){ ?>
                                <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Kecamatan</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" data-toggle="select" id="district-filter" required>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Jenis Reklame</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" data-toggle="select" id="billboard-type-filter" required>
                                <option value=""></option>
                                <?php foreach($billboard_types as $s){ ?>
                                    <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Jenis Produk</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" data-toggle="select" id="product-filter" required>
                                <option value=""></option>
                                <?php foreach($categories as $s){ ?>
                                    <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-control-label">Letak Reklame</label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select class="form-control" data-toggle="select" id="placement-billboard-filter" required>
                                <option value=""></option>
                                <?php foreach($placement_billboard as $s){ ?>
                                    <option value="<?=$s->code?>"><?=$s->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer" style="margin-top: -40px">
            <button type="button" class="btn btn-info" onclick="filter()" > Filter Lokasi</button></button>
            <button type="button" class="btn btn-danger" onclick="$('#modal-filter').modal('hide')">Tutup</button>
        </div>
    </div>
  </div>
</div>

<div class="modal" id="modal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h1 class="w-100 text-center">Nomor Formulir : <span id="formulir"></span></h1>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" style="margin-top: -40px !important">
            <div class="row">
                <div class="col-md-6">
                    <div class="table-responsive py-4" style="font-size: 18px !important">
                        <h1>Biodata</h1>
                        <table class="table" id="datatable-buttons">
                            <tr>
                                <td width="30%">Tanggal Verfikasi</td>
                                <td>: <span id="date"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Nama Pemohon</td>
                                <td>: <span id="name"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Jenis Billboard</td>
                                <td>: <span id="type"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Jenis Produk</td>
                                <td>: <span id="product"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Letak Reklame</td>
                                <td>: <span id="location"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Status Tanah</td>
                                <td>: <span id="status"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Lokasi Penempatan</td>
                                <td>: <span id="place"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Batas Ijin</td>
                                <td>: <span id="expired"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Alamat</td>
                                <td>: <span id="address"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Kecamatan</td>
                                <td>: <span id="district"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Sudut Pandang</td>
                                <td>: <span id="viewpoint"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive py-4" style="font-size: 15px">
                        <h1>Teks Reklame</h1>
                        <table class="table" id="datatable-buttons">
                            <tr>
                                <td width="30%">Panjang</td>
                                <td>: <span id="length"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Lebar</td>
                                <td>: <span id="width"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Tinggi</td>
                                <td>: <span id="height"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Luas</td>
                                <td>: <span id="size"></span></td>
                            </tr>
                            <tr>
                                <td width="30%">Teks</td>
                                <td>: <span id="text"></span></td>
                            </tr>
                        </table>
                    </div>
                    <div id="image-box"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="margin-top: -40px !important">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function changeCity(val){
        $.get("<?=base_url()?>lists/districtByCity/"+val, function(res){
            res=JSON.parse(res)
            $("#district-filter").empty();
            $("#district-filter").append(new Option('',''));
            res.forEach(function(el){
                $("#district-filter").append(new Option(el.name,el.id));
            })
        });
    }
    function filter(){
        $("#preloader").show();
        var formulir = $('#no-formulir-filter').val();
        var status = $('#status-filter').val();
        var city = $('#city-filter').val();
        var district = $('#district-filter').val();
        var type = $('#billboard-type-filter').val();
        var product = $('#product-filter').val();
        var placement = $('#placement-billboard-filter').val();
        
        var params = {
            formulir : formulir,
            city : city,
            district : district,
            type : type,
            product : product,
            placement : placement
        }
        
        var lat     = -6.1822265;
        var long    = 106.8014451;
        
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(lat, long),
        });

        $.ajax({
            type: "POST",
            url: "<?=base_url()?>pointing/point_filter",
            data: params,
            dataType: 'json',
            success: function(data){
                $("#preloader").hide();
                let result = JSON.stringify(data);
                let rs = $.parseJSON(result);
                Object.keys(rs).forEach(function(key) {
                    let id      = rs[key].id;
                    let name    = rs[key].name;
                    let company_name    = rs[key].company_name;
                    let address = rs[key].address;
                    let lat     = parseFloat(rs[key].latitude);
                    let long    = parseFloat(rs[key].longitude);
                    let status  = rs[key].approval_status;
                    let type    = rs[key].type;
                    let image   = '';
                    
                    if(type == 'legal'){
                        if(status == 0){
                            image = '<?=base_url()?>assets/img/icons/location/location-waiting.png';
                        } else if(status == 1){
                            image = '<?=base_url()?>assets/img/icons/location/location-active.png';
                        } else if(status == 3) {
                            image = '<?=base_url()?>assets/img/icons/location/location-expired.png';
                        }
                    } else {
                        image = '<?=base_url()?>assets/img/icons/location/location-ilegal.png';
                    }
                    var markerIcon = {
                        url: image,
                        scaledSize: new google.maps.Size(25, 25),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(32,65),
                        labelOrigin: new google.maps.Point(40,33)
                    };
                    
                    var marker = new google.maps.Marker({
                        animation: google.maps.Animation.DROP,
                        position: {lat: lat, lng: long},
                        map: map,
                        icon: markerIcon,
                        id: id
                    });
                    
                    if(type == 'legal'){
                        attach(marker,'legal');
                    }else{
                        attach(marker,'illegal');
                    }
                })
            }
        });
        $('#modal-filter').modal('hide')
    }
    function initMaps() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: new google.maps.LatLng(-6.1822265, 106.8014451),
        });

        setMarkers(map);
    }
    function setMarkers(map) {
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>pointing/points",
            dataType: 'json',
            success: function(data){
                let result = JSON.stringify(data);
                let rs = $.parseJSON(result);
                Object.keys(rs).forEach(function(key) {
                    let id      = rs[key].id;
                    let name    = rs[key].name;
                    let company_name    = rs[key].company_name;
                    let address = rs[key].address;
                    let lat     = parseFloat(rs[key].latitude);
                    let long    = parseFloat(rs[key].longitude);
                    let status  = rs[key].approval_status;
                    let type    = rs[key].type;
                    let image   = '';
                    
                    console.log(rs);
                    if(type == 'legal'){
                        if(status == 0){
                            image = '<?=base_url()?>assets/img/icons/location/location-waiting.png';
                        } else if(status == 1){
                            image = '<?=base_url()?>assets/img/icons/location/location-active.png';
                        } else {
                            image = '<?=base_url()?>assets/img/icons/location/location-expired.png';
                        }
                    } else {
                        image = '<?=base_url()?>assets/img/icons/location/location-ilegal.png';
                    }
                    var markerIcon = {
                        url: image,
                        scaledSize: new google.maps.Size(25, 25),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(32,65),
                        labelOrigin: new google.maps.Point(40,33)
                    };
                    
                    var marker = new google.maps.Marker({
                        animation: google.maps.Animation.DROP,
                        position: {lat: lat, lng: long},
                        map: map,
                        icon: markerIcon,
                        id: id
                    });
                    
                    if(type == 'legal'){
                        attach(marker,'legal');
                    }else{
                        attach(marker,'illegal');
                    }
                })
            }
        });
    }
    function attach(marker, type) {
        if(type=='legal'){
            marker.addListener('click', function() {
                showModal(marker.id)
            });
        }else{
            marker.addListener('click', function() {
                showIllegalModal(marker.id)
            });
        }
    }
    function showModal(id){
        $('#modal').modal('show');
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>pointing/details/"+id,
            dataType: 'json',
            success: function(data){
                $('#formulir').html(data[0].code);
                $('#date').html(data[0].created_at);
                $('#name').html(data[0].name.toUpperCase());
                $('#type').html(data[0].type_name);
                $('#product').html(data[0].category_name);
                $('#location').html(data[0].place_name);
                $('#status').html(data[0].land_name);
                $('#place').html(data[0].street_name);
                $('#expired').html(data[0].install_date+' s/d '+data[0].finish_date);
                $('#address').html(data[0].address);
                $('#district').html(data[0].district_name);
                $('#viewpoint').html(data[0].view_point_name);
                $('#length').html(data[0].length);
                $('#width').html(data[0].width);
                $('#height').html(data[0].height);
                $('#size').html(data[0].size);
                $('#text').html(data[0].billboard_text);
                getImage(data[0].code);
            }
        });
    }
    
    function getImage(code){
        $.get( "<?=base_url()?>lists/getImage/"+code, function( res ) {
           $('#image-box').html(res)
        });
    }
    
    function showIllegalModal(id){
        $('#illegalModal').modal('show');
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>pointing/illegaldetails/"+id,
            dataType: 'json',
            success: function(data){
                $('#illegal-city').html(data[0].city_name);
                $('#illegal-district').html(data[0].district_name);
                $('#illegal-street').html(data[0].street_name);
                // $('#illegal-address').html(data[0].address);
                $('#illegal-billboard-text').html(data[0].billboard_text);
                $('#illegal-type').html(data[0].type_name);
                getIllegalImage(data[0].picture);
            }
        });
    }
    
    function getIllegalImage(url){
        $.get( "<?=base_url()?>lists/getIllegalImage/"+url, function( res ) {
           $('#illegal-image-box').html(res)
        });
    }
    function showFilter(){
        $('#modal-filter').modal('show');
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmOv0twOwgE0VlvmWnmtb1B8f1KYnbR9E&callback=initMaps"
    async defer></script>