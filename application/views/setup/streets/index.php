<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0"><?=$districtname?></h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
          <!-- Form groups used in grid -->
          <form method="post" id="add-street-form" action="<?=base_url()?>setup/addstreet/<?=$districtid?>">
            <div id="streets-box">
                <div class="row street-item" id="street-1">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Nama Jalan</label>
                            <input type="text" class="form-control" name="name[]">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kelas Jalan</label>
                            <select name="street_class_code[]" class="form-control">
                                <?php foreach($streetclass as $sc){ ?>
                                <option value="<?=$sc->code?>"><?=$sc->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 text-right">
                        <div class="form-group">
                            <label>Aksi</label>
                            <button onclick="addMoreStreet()" type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
        
            <form method="POST" id="edit-street-form" style="display:none">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="form-control-label" for="example3cols1Input">Nama Jalan</label>
                    <input type="text" class="form-control" name="name" id="edit_name" placeholder="Nama" required="">
                  </div>
                </div>
                
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="form-control-label" for="example3cols1Input">Kelas Jalan</label>
                    <select name="street_class_code" id="edit_street_class_code" class="form-control">
                        <?php foreach($streetclass as $ct) { ?>
                        <option value="<?=$ct->code?>"><?=$ct->name?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
                
              </div>
              
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-warning" onclick="cancelStreet()">Cancel</button>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>

      <div class="card">
            <!-- Card header -->
        <!--<div class="card-header">-->
        <!--  <h3 class="mb-0">Data Tipe Reklame</h3>-->
        <!--</div>-->
        
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <?php $x=1; foreach($streetclass as $c) { ?>
              <li class="nav-item">
                <a class="nav-link <?=($x==1)?'active':''?>" id="tab-<?=$x?>" data-toggle="tab" href="#district<?=$x?>" role="tab" aria-controls="district<?=$x?>" aria-selected="<?=($x==0)?'true':''?>"><?=$c->name?></a>
              </li>
              <?php $x++; } ?>
            </ul>
            
            
            <div class="tab-content" id="myTabContent">
              <?php $x=1; foreach($streetclass as $c) { ?>
              <div class="tab-pane fade <?=($x==1)?'show active':''?>" id="district<?=$x?>" role="tabpanel" aria-labelledby="district<?=$x?>-tab">
                <div class="table-responsive py-4">
                  <table class="table table-flush table-hover">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>Nama Jalan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $y=1; foreach($streets as $r){
                            if($r->street_class_code == $c->code){
                        ?>
                          <tr>
                            <td width="5%"><?=$y++?></td>
                            <td><?=$r->name?></td>
                            <td width="10%">
                                <button class="btn btn-primary btn-sm" onclick="editStreet('<?=$r->id?>','<?=$r->name?>','<?=$r->street_class_code?>')"><i class="fa fa-edit"></i></button>
                                <a href="<?=base_url()?>setup/districtdetail/<?=$districtid?>?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>
                      <?php }} ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php $x++; } ?>
            </div>
            
            
        </div>
        
        
      </div>
</div>

<script>
    function edit(id, name,street_class){
        document.getElementById('form-type').setAttribute('action','<?=base_url()?>setup/districtdetail/<?=$districtid?>?update='+id);
        document.getElementById('street_class_code').value=street_class
        document.getElementById('name').value=name
        document.getElementById('name').focus()
    }
    function cancel(){
        document.getElementById('form-type').setAttribute('action','');
    }
    function addMoreStreet(){
        let c=$('.street-item').length+1
        $('#streets-box').append('<div class="row street-item" id="street-'+c+'">'+
            '<div class="col-md-5">'+
                '<div class="form-group">'+
                    '<input type="text" class="form-control" name="name[]">'+
                '</div>'+
            '</div>'+
            '<div class="col-md-4">'+
                '<div class="form-group">'+
                    '<select name="street_class_code[]" class="form-control">'+
                        '<?php foreach($streetclass as $sc){ ?>'+
                        '<option value="<?=$sc->code?>"><?=$sc->name?></option>'+
                        '<?php } ?>'+
                    '</select>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-1 text-right">'+
                '<div class="form-group">'+
                    '<button onclick="removeStreet('+c+')" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>'+
                '</div>'+
            '</div>'+
        '</div>');
    }
    
    function editStreet(id, name,street_class){
        document.getElementById('add-street-form').style.display='none'
        document.getElementById('edit-street-form').style.display='block'
        document.getElementById('edit-street-form').setAttribute('action','<?=base_url()?>setup/districtdetail/<?=$districtid?>?update='+id)
        document.getElementById('edit_street_class_code').value=street_class
        document.getElementById('edit_name').value=name
        document.getElementById('edit_name').focus()
    }
    function cancelStreet(){
        document.getElementById('add-street-form').style.display='block'
        document.getElementById('edit-street-form').style.display='none'
    }
    function removeStreet(id){
        $('#street-'+id).remove()
    }
</script>

