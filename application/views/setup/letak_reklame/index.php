


<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <!--<h3 class="mb-0">Setup Sudut Pandang</h3>-->
        </div>
        
        <!-- Card body -->
        <div class="card-body">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
          <!-- Form groups used in grid -->
          
          
        <form method="POST" id="form-letak_reklame">
          <div class="row">
            <div class="col-md-1">
                <label class="form-control-label" for="example3cols1Input">Code</label>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="code" name="code" placeholder="Code" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1">
                <label class="form-control-label" for="example3cols1Input">Nama</label>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="name" name="name" placeholder="Nama" required>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
              </div>
            </div>
          </div>
          
        </form>
        </div>
      </div>
      

      <div class="card">
            <!-- Card header -->
        <!--<div class="card-header">-->
        <!--  <h3 class="mb-0">Data Sudut pandang</h3>-->
        <!--</div>-->
        
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-buttons">
            <thead class="thead-light">
                
              <tr>
                <th>No</th>
                <th>Code</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
              
            </thead>
            <tbody>
             <?php $x = 1; foreach($placement_billboard as $r){ ?>
              <tr>
                <td width="5%"><?=$x++?></td>
                <td><?=$r->code?></td>
                <td><?=$r->name?></td>
                <td width="10%">
                    <button class="btn btn-primary btn-sm" onclick="edit('<?=$r->id?>','<?=$r->code?>','<?=$r->name?>')"><i class="fa fa-edit"></i></button>
                    <a href="<?=base_url()?>setup/letak_reklame?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
             <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
</div>

<script>
    function edit(id, code, name){
        document.getElementById('form-letak_reklame').setAttribute('action','<?=base_url()?>setup/letak_reklame?update='+id);
        document.getElementById('code').value=code
        document.getElementById('name').value=name
        document.getElementById('code').focus()
    }
    function cancel(){
        document.getElementById('form-letak_reklame').setAttribute('action','');
    }
</script>