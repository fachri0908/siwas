<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <!--<h3 class="mb-0">Setup NSR</h3>-->
        </div>
        <!-- Card body -->
        <div class="card-body">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
            
            
            
            
          <!-- Form groups used in grid -->
          <form method="POST" id="form-nsr_led">
              
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Kelas Jalan</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="street_class" id="street_class" class="form-control"  required="">
                        <option value=""> - Pilih Kelas Jalan - </option>
                        <?php foreach($street_class as $bt) { ?>
                            <option value="<?=$bt->code?>"><?=$bt->name?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              
              <div class="row">
                  <div class="col-md-2">
                    <label class="form-control-label"> Ukuran ( dalam M<sup>2</sup>) </label>
                  </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <input type="number" class="form-control" name="start_size" id="start_size" placeholder="Mulai dari ukuran" required="">
                  </div>
                </div>
                    <label class="form-control-label mt-2"> - </label>
                <div class="col-md-2">
                  <div class="form-group">
                    <input type="number" class="form-control" name="end_size" id="end_size" placeholder="Akhir dari ukuran" required="">
                  </div>
                </div>
                
              </div>
              
              <!--<div class="row">-->
              <!--    <div class="col-md-2">-->
              <!--      <label class="form-control-label"> Ukuran Akhir </label>-->
              <!--    </div>-->
              <!--  <div class="col-md-4">-->
              <!--    <div class="form-group">-->
              <!--      <input type="number" class="form-control" name="end_size" id="end_size" placeholder="Akhir dari ukuran" required="">-->
              <!--    </div>-->
              <!--  </div>-->
              <!--</div>-->
              
              
              <div class="row">
                  <div class="col-md-2">
                    <label class="form-control-label"> Tarif </label>
                  </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="number" class="form-control" name="tarif" id="tarif" placeholder="Tarif" required="">
                  </div>
                </div>
              </div>
              
              
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Satuan</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="uom" id="uom" class="form-control" required="">
                        <option value=""> - Pilih Satuan - </option>
                        <!--<option value=""> - M<sup>2</sup> - </option>-->
                        <?php foreach($uom as $uom) { ?>
                            <option value="<?=$uom->uom_name?>"><?=$uom->uom_name?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              
              
              
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>





      <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <?php $no = 1; foreach($street_class as $bt) { ?>
                    <li class="nav-item">
                        <a class="nav-link <?=($no == 1)?'active':'';?>" id="product-tab" data-toggle="tab" href="#<?=$bt->code?>" role="tab" aria-controls="product" aria-selected="true"><?=$bt->name?></a>
                    </li>
                <?php $no++; } ?>
            </ul>
            
            
            
            <div class="tab-content" id="myTabContent">
            <?php $no = 1; foreach($street_class as $bt) { ?>
                <div class="tab-pane fade show <?=($no == 1)?'active':'';?>" id="<?=$bt->code?>" role="tabpanel" aria-labelledby="product-tab">
                    <div class="table-responsive py-4">
                          <table class="table table-flush">
                            <thead class="thead-light">
                              <tr>
                                <th>No</th>
                                <th> Ukuran </th>
                                <th>Tarif</th>
                                <th width="10%">Aksi</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                            <?php $n = 1; foreach($nsr[$bt->code] as $rss) { ?>
                                <tr>
                                    <td><?=$n?></td>
                                    <td><?=$rss->start_size?> M<sup>2</sup> - <?=$rss->end_size?> M<sup>2</td>
                                    <td><?=$rss->price?> / <?=$rss->uom?></td>
                                    <td width="10%">
                                        <button class="btn btn-primary btn-sm" onclick="edit('<?=$rss->id?>','<?=$rss->street_class?>','<?=$rss->start_size?>','<?=$rss->end_size?>','<?=$rss->price?>','<?=$rss->uom?>')"><i class="fa fa-edit"></i></button>
                                        <a href="<?=base_url()?>setup/nsr_led?delete=<?=$rss->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php $n++; } ?>
                               
                            </tbody>
                          </table>
                    </div>
              </div>
             <?php $no++; } ?>
            </div>
        </div>
      </div>
</div>

<script>
    
    function edit(id, street_class, start_size, end_size, price, uom){
        
        document.getElementById('form-nsr_led').setAttribute('action','<?=base_url()?>setup/nsr_led?update='+id);
        
        document.getElementById('street_class').value=street_class
        document.getElementById('start_size').value=start_size
        document.getElementById('end_size').value=end_size
        document.getElementById('tarif').value=price
        document.getElementById('uom').value=uom
        
    }
    function cancel(){
        document.getElementById('form-nsr_led').setAttribute('action','');
    }
</script>

