<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Harga per kelas</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
          <!-- Form groups used in grid -->
          <form method="POST" id="form-type">
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Kelas Jalan</label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control" name="street_class_id" id="class" required>
                            <option value=""> Pilih </option>
                            <?php
                            $data = $this->db->query("select * from street_class")->result();
                            foreach($data as $ar){?>
                                <option value="<?=$ar->id?>"><?=$ar->name?></option>
                            <?php } ?>
                        </select> 
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Keterangan</label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" rows="4" name="description" id="description"></textarea>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>

      <div class="card">
            <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Harga per kelas</h3>
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-buttons">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Tipe</th>
                <th>Kelas</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php $x=1; foreach($prices as $r){ ?>
                  <tr>
                    <td width="5%"><?=$x++?></td>
                    <td><?=$r->type_name?></td>
                    <td><?=$r->class_name?></td>
                    <td><a data-toggle="tooltip" data-placement="top" title="<?=$r->description?>"><?php if($r->description){ echo substr($r->description, 0, 40).'...'; } ?></a></td>
                    <td width="10%">
                        <button class="btn btn-primary btn-sm" onclick="edit('<?=$r->id?>','<?=$r->billboard_type_id?>','<?=$r->street_class_id?>','<?=$r->description?>')"><i class="fa fa-edit"></i></button>
                        <a href="<?=base_url()?>setup/prices?id=<?=$r->billboard_type_id?>&delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
</div>

<script>
    function currency(vl){
        if(vl == ''){
            var nStr = 0;
        } else {
            var nStr = parseFloat(vl.replace(/,/g, ''));
        }
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        $('#price').val(x1 + x2);
    }
    function edit(id, type_id, class_id, desc){
        document.getElementById('form-type').setAttribute('action','<?=base_url()?>setup/prices?id='+type_id+'&update='+id);
        document.getElementById('class').value=class_id
        document.getElementById('description').value=desc
    }
    function cancel(){
        document.getElementById('form-type').setAttribute('action','');
    }
    function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

</script>

