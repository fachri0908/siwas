<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Setup NJOR</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
          <!-- Form groups used in grid -->
          <form method="POST" id="form-njor">
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label" >Jenis reklame</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="type" class="form-control" id="type">
                        <option></option>
                        <?php foreach($types as $t){ ?>
                        <option value="<?=$t->id?>"><?=$t->name?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label" for="example3cols2Input">Harga dasar ukuran (m<sup>2</sup>)</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" class="form-control" name="size_price" id="size_price" placeholder="Harga dasar ukuran">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label" for="example3cols2Input">Harga dasar ketinggian (m<sup>2</sup>)</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" class="form-control" name="height_price" id="height_price" placeholder="Harga dasar ketinggian">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <button class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
                  </div>
                </div>
              </div>
         </form>
        </div>
      </div>

      <div class="card">
            <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Data NJOR</h3>
          <!-- <p class="text-sm mb-0">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p> -->
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-buttons">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Jenis Reklame</th>
                <th>Harga Dasar Ukuran</th>
                <th>Harga Dasar Ketinggian</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
             <?php $x=1; foreach($njors as $r){ ?>
              <tr>
                <td width="5%"><?=$x++?></td>
                <td><?=$r->name?></td>
                <td><?=$r->size_price?> /m<sup>2</sup></td>
                <td><?=$r->height_price?> /m</td>
                <td width="10%">
                    <button class="btn btn-primary btn-sm" onclick="edit('<?=$r->id?>','<?=$r->type?>','<?=$r->size_price?>','<?=$r->height_price?>')"><i class="fa fa-edit"></i></button>
                    <a href="<?=base_url()?>setup/njor?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
             <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
</div>
<script>
    function edit(id, type,size,height){
        document.getElementById('form-njor').setAttribute('action','<?=base_url()?>setup/njor?update='+id);
        document.getElementById('type').value=type
        document.getElementById('size_price').value=size
        document.getElementById('height_price').value=height
        document.getElementById('height_price').focus()
    }
    function cancel(){
        document.getElementById('form-njor').setAttribute('action','');
    }
</script>