<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <!--<h3 class="mb-0">Setup NSR</h3>-->
        </div>
        <!-- Card body -->
        <div class="card-body">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
          <!-- Form groups used in grid -->
          <form method="POST" id="form-nsr">
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label" for="example3cols1Input">Tipe</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="type" id="type" class="form-control">
                        <option value="PRD">Produk</option>
                        <option value="NPRD">Non Produk</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label" for="example3cols1Input">Kelas Jalan</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="street_class" id="street_class" class="form-control">
                        <?php foreach($streetclass as $sc){ ?>
                        <option value="<?=$sc->code?>"><?=$sc->name?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-2">
                    <label class="form-control-label" for="example3cols2Input">Tarif (per m<sup>2</sup> per hari)</label>
                  </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="number" class="form-control" name="rate" id="rate" placeholder="Tarif" required="">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>

      <div class="card">
            <!-- Card header -->
        <!--<div class="card-header">-->
        <!--  <h3 class="mb-0">Data Tarif Pajak</h3>-->
        <!--</div>-->
        
            <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="product-tab" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="true">Produk</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="non-product-tab" data-toggle="tab" href="#non-product" role="tab" aria-controls="non-product" aria-selected="false">Non Produk</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="product" role="tabpanel" aria-labelledby="product-tab">
                  <div class="table-responsive py-4">
                      <table class="table table-flush">
                        <thead class="thead-light">
                          <tr>
                            <th>No</th>
                            <th>Tipe</th>
                            <th>Kelas Jalan</th>
                            <th>Tarif</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $x=1; foreach($nsr as $r){ 
                                if($r->type=='PRD'){
                            ?>
                              <tr>
                                <td width="5%"><?=$x++?></td>
                                <td><?=($r->type=="PRD")?'Produk':'Non Produk'?></td>
                                <td><?=$r->name?></td>
                                <td><?=$r->rate?></td>
                                <td width="10%">
                                    <button class="btn btn-primary btn-sm" onclick="edit('<?=$r->id?>','<?=$r->type?>','<?=$r->street_class?>','<?=$r->rate?>')"><i class="fa fa-edit"></i></button>
                                    <a href="<?=base_url()?>setup/nsr?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                              </tr>
                          <?php } }?>
                        </tbody>
                      </table>
                    </div>
              </div>
              <div class="tab-pane fade" id="non-product" role="tabpanel" aria-labelledby="non-product-tab">
                  <div class="table-responsive py-4">
                      <table class="table table-flush">
                        <thead class="thead-light">
                          <tr>
                            <th>No</th>
                            <th>Tipe</th>
                            <th>Kelas Jalan</th>
                            <th>Tarif</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php $x=1; foreach($nsr as $r){ 
                            if($r->type=='NPRD'){
                            ?>
                              <tr>
                                <td width="5%"><?=$x++?></td>
                                <td><?=($r->type=="NPRD")?'Produk':'Non Produk'?></td>
                                <td><?=$r->name?></td>
                                <td><?=$r->rate?></td>
                                <td width="10%">
                                    <button class="btn btn-primary btn-sm" onclick="edit('<?=$r->id?>','<?=$r->type?>','<?=$r->street_class?>','<?=$r->rate?>')"><i class="fa fa-edit"></i></button>
                                    <a href="<?=base_url()?>setup/nsr?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                              </tr>
                          <?php } } ?>
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>
            </div>
      </div>
</div>

<script>
    
    function edit(id,type, streetclass,rate){
        document.getElementById('form-nsr').setAttribute('action','<?=base_url()?>setup/nsr?update='+id);
        document.getElementById('type').value=type
        document.getElementById('street_class').value=streetclass
        document.getElementById('rate').value=rate
        document.getElementById('rate').focus()
    }
    function cancel(){
        document.getElementById('form-nsr').setAttribute('action','');
    }
</script>

