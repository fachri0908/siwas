<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Setup Tipe Reklame</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
            <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php } ?>
          <!-- Form groups used in grid -->
          <form method="POST" id="form-type">
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Kode</label>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="code" id="code" placeholder="Kode" required="">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Nama</label>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nama" required="">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Tarif</label>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" onkeyup="currency(this)" name="price" id="price" value="0" placeholder="Tarif" required="">
                    </div>
                </div>
                <div style="width: 5px">
                    <label class="form-control-label"><h1>/</h1></label>
                </div>
                <div class="col-md-3" style="margin-left: -5px">
                    <div class="form-group">
                        <select class="form-control" name="uom" id="uom" required>
                            <option value=""> Unit </option>
                            <?php
                            $data = $this->db->query("select * from uom")->result();
                            foreach($data as $ar){?>
                                <option value="<?=$ar->uom_name?>"><?=$ar->uom_name?></option>
                            <?php } ?>
                        </select>  
                    </div>
                </div>
                
                
                <div class="col-md-3">
                    <div class="form-group">
                        <label><input type="checkbox" value="1" name="is_single_price" id="check-lokasi" onclick="penempatan()"> Lokasi Penempatan</label>
                    </div>
                </div>
              </div>
              
              
              <div class="lok"></div>
              
              
              
              
              
              
              
              
              <div class="row">
                  
                <div class="col-md-2 penempatan" style="display: none">
                    <label class="form-control-label">Penempatan </label>
                </div>
                
                
                <div class="col-md-2 penempatan" style="display: none">
                    <div class="form-group">
                        <?php
                        $data = $this->db->query("select * from street_class")->result();
                        foreach($data as $ar){?>
                            <select class="form-control" style="margin-top: 10px" name="street_class_id[]" readonly>
                                <option value="<?=$ar->id?>"><?=$ar->name?></option>
                            </select>
                        <?php } ?>
                    </div>
                </div>
                
                
                
                <div class="col-md-1 penempatan" style="display: none">
                    <div class="form-group">
                        <?php
                        $data = $this->db->query("select * from street_class")->result();
                        foreach($data as $ar){?>
                            <input type="text" class="form-control req" onkeyup="currency(this)" style="margin-top: 10px" name="start_size[]" value="0" placeholder="">
                        <?php } ?>
                    </div>
                </div>
                
                <div class="penempatan" style="width: 5px; display: none">
                    <?php
                    $data = $this->db->query("select * from street_class")->result();
                    foreach($data as $ar){?>
                        <label class="form-control-label" style="margin-top: 2px"><h1>-</h1></label>
                    <?php } ?>
                </div>
                
                <div class="col-md-1 penempatan" style="display: none">
                    <div class="form-group">
                        <?php
                        $data = $this->db->query("select * from street_class")->result();
                        foreach($data as $ar){?>
                            <input type="text" class="form-control req" onkeyup="currency(this)" style="margin-top: 10px" name="end_size[]" value="0" placeholder="">
                        <?php } ?>
                    </div>
                </div>
                
                
                <div class="col-md-2 penempatan" style="display: none">
                    <div class="form-group">
                        <?php
                        $data = $this->db->query("select * from street_class")->result();
                        foreach($data as $ar){?>
                            <input type="text" class="form-control req" onkeyup="currency(this)" style="margin-top: 10px" name="price_class[]" value="0" placeholder="Tarif">
                        <?php } ?>
                    </div>
                </div>
                
                <div class="penempatan" style="width: 5px; display: none">
                    <?php
                    $data = $this->db->query("select * from street_class")->result();
                    foreach($data as $ar){?>
                        <label class="form-control-label" style="margin-top: 2px"><h1>/</h1></label>
                    <?php } ?>
                </div>
                
                
                <div class="col-md-3 penempatan" style="margin-left: -5px; display: none">
                    <div class="form-group">
                        <?php
                        $data = $this->db->query("select * from street_class")->result();
                        foreach($data as $ar){?>
                            <select class="form-control req" style="margin-top: 10px" name="price_uom[]">
                                <option value=""> Unit </option>
                                <?php
                                $data = $this->db->query("select * from uom")->result();
                                foreach($data as $ar){?>
                                    <option value="<?=$ar->uom_name?>"><?=$ar->uom_name?></option>
                                <?php } ?>
                            </select>  
                        <?php }?>
                    </div>
                </div>
                
              </div>
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              <div class="row">
                <div class="col-md-2">
                    <label class="form-control-label">Keterangan</label>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                        <textarea class="form-control" rows="4" name="description" id="description"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-warning" onclick="cancel()">Cancel</button>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>

      <div class="card">
            <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Data Tipe Reklame</h3>
          <!-- <p class="text-sm mb-0">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p> -->
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-buttons">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Tarif</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php $x=1; foreach($types as $r){ ?>
                  <tr>
                    <td width="5%"><?=$x++?></td>
                    <td><?=$r->code?></td>
                    <td><?=$r->name?></td>
                    <td>Rp. <?=number_format($r->price)?> / <?=$r->uom?> </td>
                    <td><a data-toggle="tooltip" data-placement="top" title="<?=$r->description?>"><?php if($r->description){ echo substr($r->description, 0, 40).'...'; } ?></a></td>
                    <td width="10%" align="center">
                        <?php if($r->price == 0){?>
                            <a href="#" onclick="showModal(<?=$r->id?>,'<?=$r->name?>')" data-toggle="modal" data-target="#modal" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                        <?php } ?>
                        <button class="btn btn-primary btn-sm" onclick="edit('<?=$r->id?>','<?=$r->code?>','<?=$r->name?>','<?=$r->price?>','<?=$r->uom?>','<?=$r->description?>')"><i class="fa fa-edit"></i></button>
                        <a href="<?=base_url()?>setup/type?delete=<?=$r->id?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    <!-- Footer -->
    <footer class="footer pt-0">
      <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
          <div class="copyright text-center text-lg-left text-muted">
              &copy; 2019 
          </div>
          </div>
      </div>
    </footer>
</div>
<div class="modal" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Detail - <span id="name"></span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div id="detail-lok"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>

<script>
    function showModal(id, name){
        $.ajax({
            type: "POST",
            // url: "<?=base_url()?>setup/penempatan/"+id,
            url: "<?=base_url()?>setup/penempatandev/"+id,
            success: function(data){
                console.log(data)
                $('#detail-lok').html(data);
                $('#modal #name').html(name);
                $('#modal .label-penempatan').remove();
                
                $('#modal .col-md-2').addClass('col-md-3').removeClass('col-md-2');
                $('#modal .size').addClass('col-md-2').removeClass('col-md-1');
                $('#modal .price').addClass('col-md-2').removeClass('col-md-3');
                $('#modal .uom').addClass('col-md-2').removeClass('col-md-3');
                $('#modal .slas').css({ 'width':'15px', 'margin':'0 10px'});
                
                
                $('#modal .form-control').attr( "disabled","disabled");
            }
        });
    }
    function currency(th){
        if(th.value == ''){
            var nStr = 0;
        } else {
            var nStr = parseFloat(th.value.replace(/,/g, ''));
        }
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        th.value = x1 + x2;
    }
    
    function penempatan(){
        if($('#check-lokasi').is(':checked')){
            $('.penempatan').css('display','block');
            $('#price').attr( "disabled", "disabled");
            $('#uom').attr( "disabled","disabled");
            $('#price').val('0');
            $('#uom').val('');
            $('.req').attr("required","required");
        } else {
            $('.penempatan').css('display','none');
            $('#price').removeAttr( "disabled");
            $('#uom').removeAttr( "disabled");
            $('.req').removeAttr("required");
        }
        $('.lok').html('');
    }
    
    
    
    
    function edit(id, code, name, price, uom, desc){
        // document.getElementById('form-type').setAttribute('action','<?=base_url()?>setup/type?update='+id);
        document.getElementById('form-type').setAttribute('action','<?=base_url()?>setup/typedev?update='+id);
        
        $('.penempatan').css('display','none');
        
        if(price == '0'){
            $('#check-lokasi').prop("checked", true);
            $('#price').attr( "disabled", "disabled");
            $('#uom').attr( "disabled","disabled");
            $('.req2').attr("required","required");
            $('.req').removeAttr("required");
            $.ajax({
                type: "POST",
                // url: "<?=base_url()?>setup/penempatan/"+id,
                url: "<?=base_url()?>setup/penempatandev/"+id,
                success: function(data){
                    $('.lok').html(data);
                }
            });
        } else {
            $('#price').removeAttr( "disabled");
            $('#uom').removeAttr( "disabled");
            $('.req').removeAttr("required");
            $('.lok').html('');
        }
        
        document.getElementById('code').value=code
        document.getElementById('name').value=name
        document.getElementById('price').value=addCommas(price)
        document.getElementById('uom').value=uom
        document.getElementById('description').value=desc
        document.getElementById('code').focus()
    }
    
    
    
    
    
    
    function cancel(){
        document.getElementById('form-type').setAttribute('action','');
        $('#check-lokasi').removeAttr("checked");
        $('#price').removeAttr( "disabled");
        $('#uom').removeAttr( "disabled");
        $('.penempatan').css('display','none');
        $('.req').removeAttr("required");
        $('.lok').html('');
    }
    function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

</script>

