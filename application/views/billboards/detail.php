<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    
    <div class="row">
        <div class="col-lg-4">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Data Pemohon</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label" for="">Nama</label>
                            <input type="hidden" name="code" value=''>
                            <input type="text" name="nama" class="form-control form-control-small" id="" placeholder="Name" value='' readonly>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label" for="">Alamat</label>
                            <input type="text" name="address" class="form-control form-control-small" id="" placeholder="Alamat" value='' readonly>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label" for="">Hp</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="HP" value='' readonly>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label" for="">NPWP</label>
                            <div class="col-lg-12"> <img height=180 width=333 src="<?=base_url()?>assets/image-upload/npwp/"> </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlInput1">KTP</label>
                            <div class="col-lg-12"> <img height=180 width=333 src="<?=base_url()?>assets/image-upload/ktp/"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        
                        <div class="form-group">
                            <label class="form-control-label">Kategori</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Kategori" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Jenis Reklame</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Jenis Reklame" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Sudut Pandang</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Sudut Pandang" value='' readonly>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="form-control-label">Kota</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Kota" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Kecamatan</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Kecamatan" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Jalan</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Jalan" value='' readonly>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label">Area Pemasangan</label>
                            <input type="text" name="phone_number" class="form-control form-control-small" id="" placeholder="Area Pemasangan" value='' readonly>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label class="form-control-label">Alamat Pemasangan</label>
                            <textarea class="form-control" name="install_address" id="" rows="4" placeholder="Alamat Pemasangan" readonly></textarea>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Panjang</label>
                                <input class="form-control" type="text" name="length" placeholder="Panjang" value='' readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label">Lebar</label>
                                <input class="form-control" type="text" name="width" placeholder="Lebar" value='' readonly>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label">Luas m<sup>2</sup></label>
                            <input type="text" class="form-control" name="size" type="text" placeholder="Luas m/2" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Tinggi Media</label>
                            <input type="text" class="form-control" name="height" type="text" placeholder="Tinggi Media" value='' readonly>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Permohonan</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label" for="exampleFormControlSelect1">Tahun Tarif</label>
                            <input type="text" class="form-control" name="height" type="text" placeholder="Tahun Tarif" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="">Tanggal Pasang</label>
                            <input type="date" name="install_date" class="form-control" id="" placeholder="Tanggal Pasang" value="" readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="">Tanggal Selesai</label>
                            <input type="date" name="finish_date" class="form-control" id="" placeholder="Tanggal Selesai"  value="" readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="">Lama Pemasangan (hari)</label>
                            <input type="text" name="install_duration" class="form-control" id="" placeholder="Lama Pemasangan (hari)"  value="" readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">NSR</label>
                            <input type="text" class="form-control" name="nsr" type="text" placeholder="Nilai Sewa Reklame" value='' readonly>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Pajak</label>
                            <input type="text" class="form-control" name="nsr" type="text" placeholder="Nilai Sewa Reklame" value='' readonly>
                        </div>
                        <div class="form-group">
                            <!--<button type="button" class="btn btn-secondary" onclick="history.go(-1)"> Back </button>-->
                            <!--<a href="<?=base_url()?>billboards/edit/<?=$this->uri->segment('3')?>"><button type="button" class="btn btn-primary" onclick="history.go(-1)"> Edit </button></a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--</form>-->
</div>

