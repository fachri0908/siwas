<div class="container-fluid mt--6">
        <?php if($this->session->flashdata('success')){ ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php } ?>
        
        
      <div class="card">
            <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Status Permohonan Pajak Reklame</h3>
          <!-- <p class="text-sm mb-0">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p> -->
        </div>
        <div class="card-body">
            <a href="<?=base_url()?>billboards/detail/<?=$billboard[0]->code?>" class="btn btn-primary btn-sm" ><i class="fa fa-eye"></i> Lihat Detail Reklame</a>
            <br><br>
            <div class="timeline timeline-one-side" data-timeline-content="axis" data-timeline-axis-style="dashed">
                <?php $x=1; foreach($levels as $l){ ?>
                <?php $appr=$this->builder->getRecordByCond('approvals',['billboard_code'=>$billboard[0]->code,'level'=>$l->level]);
                    $icon='history';
                    $badge='secondary';
                    $status='Menunggu Pengecekan';
                    $receive='Menunggu Pengecekan';
                    $approve='Menunggu Pengecekan';
                    $text='dark';
                    if(count($appr)>0){
                        if($appr[0]->approve_status==0){
                            $badge='secondary';
                            $icon='history';
                            $status='Menunggu Pengecekan';
                            $text='dark';
                        }else if($appr[0]->approve_status==1){
                            $badge='success';
                            $icon='check';
                            $status='Disetujui';
                            $text='success';
                        }else{
                            $badge='danger';
                            $icon='times';
                            $status='Ditolak';
                            $text='danger';
                        }
                        $receive=date('d F Y', strtotime($appr[0]->receive_at));
                        if($appr[0]->approved_at != NULL){
                            $approve=date('d F Y', strtotime($appr[0]->approved_at));
                        }else{
                            $approve='Menunggu Pengecekan';
                        }
                    }
                ?>
                
                <div class="timeline-block">
                  <span class="timeline-step badge-<?=$badge?>">
                    <i class="fa fa-<?=$icon?>"></i>
                  </span>
                  <div class="timeline-content">
                    <div class="d-flex justify-content-between pt-1">
                      <div>
                        <span class="text-<?=$text?> text-sm font-weight-bold"><?=$l->name?> (<?=$approve?>)</span>
                      </div>
                      <div class="text-right">
                        <small class="text-muted"><i class="fas fa-clock-o"></i><?=$receive?></small>
                      </div>
                    </div>
                    <h6 class="text-sm mt-1 mb-2"><?=$status?></h6>
                    <?php if(count($appr)>0 && $this->session->userdata('user_position')==$l->approver_position_code && $appr[0]->approve_status==0){ ?>
                        <a href="<?=base_url()?>billboards/approve/<?=$l->level?>?code=<?=$billboard[0]->code?>" class="btn btn-success btn-sm" onclick="return confirm('Are you sure you want to confirm this billboard?')"><i class="fa fa-check"></i></a>
                        <a href="<?=base_url()?>billboards/reject/<?=$l->level?>?code=<?=$billboard[0]->code?>" class="btn btn-warning btn-sm" onclick="return confirm('Are you sure you want to reject this billboard?')"><i class="fa fa-times"></i></a>
                    <?php } ?>
                  </div>
                </div>
                <?php } ?>
            </div>
        </div>
        
        
      </div>
</div>



<div class="modal" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        
        <div class="modal-header">
            <h4 class="modal-title">Detail - <span id="name"></span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
            <div id="detail-reklame"></div>
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>


<script>
        
    // function showModal(id,code) {
    //     $.ajax({
    //         type: "POST",
    //         url: "http://smiwebportal.com/siwas/billboards/detail/"+id,
    //         success: function(data){
    //             $('.modal-header').find('#name').text(code);
    //             $('.modal-body').find('#detail-reklame').html(data);
    //             $('#modal').modal();
    //         }
    //     });
    // }
</script>