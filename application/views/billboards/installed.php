<div class="container-fluid mt--6">
    
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    
    
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Reklame</h3>
                        <a class="btn btn-primary float-right btn-sm" href="<?=base_url()?>registration">Permohonan Baru</a>
                    </div>
                    
                    <div class="row mt-2 ml-2">
                        <div class="col-md-12">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                                <a data-toggle="tab" role="tab" href="#approve-tab" class="btn btn-secondary active">Semua Reklame</a>
                                <a data-toggle="tab" role="tab" href="#expired-tab" class="btn btn-secondary"> Masa Tenggang </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <!--diterima-->
                        <div class="tab-pane fade show active" id="approve-tab" role="tabpanel">
                            <div class="table-responsive py-4">
                              <table class="table table-flush" id="datatable-nobuttons">
                                <thead class="thead-light">
                                  <tr>
                                    <th>No</th>
                                    <th>Kode Reklame</th>
                                    <th>Tipe Reklame</th>
                                    <th>Teks reklame</th>
                                    <th>QR</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1; foreach($billboards as $b){ ?>
                                      <tr>
                                        <td width="5%"><?=$x++?></td>
                                        <td><?=$b->code?></td>
                                        <td><?php $this->builder->getNameByCond('billboard_types',['code'=>$b->billboard_type])?></td>
                                        <td><?=$b->billboard_text?></td>
                                        <td>
                                            <img src="<?=base_url()?>assets/image-upload/qr/<?=$b->code?>.png">
                                        </td>
                                        <td width="10%">
                                            <a class="btn btn-primary btn-sm" href="<?=base_url()?>applicants/billboarddetail/<?=$b->code?>"><i class="fa fa-eye"></i> Detail</a>
                                            <a class="btn btn-success btn-sm" href="<?=base_url()?>extensions/request/<?=$b->code?>"><i class="fa fa-handshake"></i> Ajukan Perpanjangan</a>
                                        </td>
                                      </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <!--masa tenggang-->
                        <div class="tab-pane fade" id="expired-tab" role="tabpanel">
                            <div class="table-responsive py-4">
                              <table class="table table-flush" id="datatable-nobuttons">
                                <thead class="thead-light">
                                  <tr>
                                    <th>No</th>
                                    <th>Kode Reklame</th>
                                    <th>Tipe Reklame</th>
                                    <th>Teks reklame</th>
                                    <th>Persyaratan</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1; foreach($billboards as $b){
                                        // if($b->approval_status==3 || $b->finish_date < date('Y-m-d')){
                                        if($b->finish_date < date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-d')))) || $b->finish_date < date('Y-m-d', strtotime('-1 month', strtotime(date('Y-m-d'))))){
                                        ?>
                                      <tr>
                                        <td width="5%"><?=$x++?></td>
                                        <td><?=$b->code?></td>
                                        <td><?php $this->builder->getNameByCond('billboard_types',['code'=>$b->billboard_type])?></td>
                                        <td><?=$b->billboard_text?></td>
                                        <td>
                                            <?php $p=$this->builder->checkCompletenes($b->code); ?>
                                            <small class="text-center"> Persyaratan <?=$p->uploaded?> / <?=$p->total?> (<?=number_format($p->uploaded/$p->total*100)?>%)</small>
                                            <div class="progress progress-xs my-2 bg-dark">
                                              <div class="progress-bar bg-success" style="width: <?=number_format($p->uploaded/$p->total*100)?>%"></div>
                                            </div>
                                        </td>
                                        <td width="10%">
                                            <a class="btn btn-primary btn-sm" href="<?=base_url()?>applicants/billboarddetail/<?=$b->code?>"><i class="fa fa-eye"></i> Detail</a>
                                            <a class="btn btn-success btn-sm" href="<?=base_url()?>extensions/request/<?=$b->code?>"><i class="fa fa-handshake"></i> Ajukan Perpanjangan</a>
                                        </td>
                                      </tr>
                                  <?php } } ?>
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
