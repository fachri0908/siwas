<div class="container-fluid mt--6">
       <!-- Table -->
       <div class="row">
         <div class="col">
           <div class="card">
             <!-- Card header -->
             <div class="card-header">
               <h3 class="mb-0">List Reklame</h3>
             </div>
             <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlSelect1">Wilayah</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>DKI Jakarta (All)</option>
                        <option>Kab. Kepulauan Seribu</option>
                        <option>Kota Jakarta Barat</option>
                        <option>Kota Jakarta Pusat</option>
                        <option>Kota Jakarta Selatan</option>
                        <option>Kota Jakarta Timur</option>
                        <option>Kota Jakarta Utara</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" style="background: #4fd69c; color: #fff; font-size: 13px; padding: 10px; margin-left-10px;" href="#aman" role="tab" data-toggle="tab">
                        <i class="fa fa-check"></i> Aman</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="background: #fc7c5f; color: #fff; font-size: 13px; padding: 10px" href="#akan-berakhir" role="tab" data-toggle="tab">
                            <i class="fa fa-info"></i> Akan Berakhir</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="background:#f75676; color: #fff; font-size: 13px; padding: 10px" href="#berakhir" role="tab" data-toggle="tab">
                            <i class="fa fa-calendar"></i> Sudah Berakhir</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="aman">
                        <div class="table-responsive py-4">
                            <table class="table table-flush" id="datatable-basic">
                                <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th>PROTOKOL A</th>
                                    <th>PROTOKOL B</th>
                                    <th>PROTOKOL C</th>
                                    <th>EKONOMI 1</th>
                                    <th>EKONOMI 2</th>
                                    <th>EKONOMI 3</th>
                                    <th>LINKUNGAN</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td><b>125</b></td>
                                        <td><b>276</b></td>
                                        <td><b>318</b></td>
                                        <td><b>334</b></td>
                                        <td><b>266</b></td>
                                        <td><b>386</b></td>
                                        <td><b>366</b></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <tr>
                                    <td>Papan/billboard/videotron/megatron</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>14</td>
                                    <td>30</td>
                                    <td>10</td>
                                    <td>50</td>
                                    <td>45</td>
                                </tr>
                                <tr>
                                    <td>Kain</td>
                                    <td>20</td>
                                    <td>30</td>
                                    <td>23</td>
                                    <td>54</td>
                                    <td>40</td>
                                    <td>32</td>
                                    <td>18</td>
                                </tr>
                                <tr>
                                    <td>Melekat, stiker</td>
                                    <td>15</td>
                                    <td>21</td>
                                    <td>60</td>
                                    <td>23</td>
                                    <td>10</td>
                                    <td>34</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>Selebaran</td>
                                    <td>19</td>
                                    <td>15</td>
                                    <td>34</td>
                                    <td>76</td>
                                    <td>15</td>
                                    <td>9</td>
                                    <td>95</td>
                                </tr>
                                <tr>
                                    <td>Berjalan, termasuk pada kendaraan</td>
                                    <td>10</td>
                                    <td>5</td>
                                    <td>9</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>99</td>
                                    <td>98</td>
                                </tr>
                                <tr>
                                    <td>Udara</td>
                                    <td>5</td>
                                    <td>10</td>
                                    <td>64</td>
                                    <td>34</td>
                                    <td>23</td>
                                    <td>18</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>Apung</td>
                                    <td>7</td>
                                    <td>65</td>
                                    <td>12</td>
                                    <td>17</td>
                                    <td>76</td>
                                    <td>24</td>
                                    <td>26</td>
                                </tr>
                                <tr>
                                    <td>Suara</td>
                                    <td>11</td>
                                    <td>15</td>
                                    <td>67</td>
                                    <td>45</td>
                                    <td>23</td>
                                    <td>28</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>Film/slide</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>17</td>
                                    <td>20</td>
                                    <td>23</td>
                                    <td>27</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>Peragaan</td>
                                    <td>20</td>
                                    <td>90</td>
                                    <td>18</td>
                                    <td>24</td>
                                    <td>34</td>
                                    <td>65</td>
                                    <td>16</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="akan-berakhir">
                        <div class="table-responsive py-4">
                            <table class="table table-flush" id="datatable-basic">
                                <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th>PROTOKOL A</th>
                                    <th>PROTOKOL B</th>
                                    <th>PROTOKOL C</th>
                                    <th>EKONOMI 1</th>
                                    <th>EKONOMI 2</th>
                                    <th>EKONOMI 3</th>
                                    <th>LINKUNGAN</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td><b>125</b></td>
                                        <td><b>276</b></td>
                                        <td><b>318</b></td>
                                        <td><b>334</b></td>
                                        <td><b>266</b></td>
                                        <td><b>386</b></td>
                                        <td><b>366</b></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <tr>
                                    <td>Papan/billboard/videotron/megatron</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>14</td>
                                    <td>30</td>
                                    <td>10</td>
                                    <td>50</td>
                                    <td>45</td>
                                </tr>
                                <tr>
                                    <td>Kain</td>
                                    <td>20</td>
                                    <td>30</td>
                                    <td>23</td>
                                    <td>54</td>
                                    <td>40</td>
                                    <td>32</td>
                                    <td>18</td>
                                </tr>
                                <tr>
                                    <td>Melekat, stiker</td>
                                    <td>15</td>
                                    <td>21</td>
                                    <td>60</td>
                                    <td>23</td>
                                    <td>10</td>
                                    <td>34</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>Selebaran</td>
                                    <td>19</td>
                                    <td>15</td>
                                    <td>34</td>
                                    <td>76</td>
                                    <td>15</td>
                                    <td>9</td>
                                    <td>95</td>
                                </tr>
                                <tr>
                                    <td>Berjalan, termasuk pada kendaraan</td>
                                    <td>10</td>
                                    <td>5</td>
                                    <td>9</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>99</td>
                                    <td>98</td>
                                </tr>
                                <tr>
                                    <td>Udara</td>
                                    <td>5</td>
                                    <td>10</td>
                                    <td>64</td>
                                    <td>34</td>
                                    <td>23</td>
                                    <td>18</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>Apung</td>
                                    <td>7</td>
                                    <td>65</td>
                                    <td>12</td>
                                    <td>17</td>
                                    <td>76</td>
                                    <td>24</td>
                                    <td>26</td>
                                </tr>
                                <tr>
                                    <td>Suara</td>
                                    <td>11</td>
                                    <td>15</td>
                                    <td>67</td>
                                    <td>45</td>
                                    <td>23</td>
                                    <td>28</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>Film/slide</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>17</td>
                                    <td>20</td>
                                    <td>23</td>
                                    <td>27</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>Peragaan</td>
                                    <td>20</td>
                                    <td>90</td>
                                    <td>18</td>
                                    <td>24</td>
                                    <td>34</td>
                                    <td>65</td>
                                    <td>16</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="berakhir">
                        <div class="table-responsive py-4">
                            <table class="table table-flush" id="datatable-basic">
                                <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th>PROTOKOL A</th>
                                    <th>PROTOKOL B</th>
                                    <th>PROTOKOL C</th>
                                    <th>EKONOMI 1</th>
                                    <th>EKONOMI 2</th>
                                    <th>EKONOMI 3</th>
                                    <th>LINKUNGAN</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td><b>TOTAL</b></td>
                                        <td><b>125</b></td>
                                        <td><b>276</b></td>
                                        <td><b>318</b></td>
                                        <td><b>334</b></td>
                                        <td><b>266</b></td>
                                        <td><b>386</b></td>
                                        <td><b>366</b></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <tr>
                                    <td>Papan/billboard/videotron/megatron</td>
                                    <td>10</td>
                                    <td>11</td>
                                    <td>14</td>
                                    <td>30</td>
                                    <td>10</td>
                                    <td>50</td>
                                    <td>45</td>
                                </tr>
                                <tr>
                                    <td>Kain</td>
                                    <td>20</td>
                                    <td>30</td>
                                    <td>23</td>
                                    <td>54</td>
                                    <td>40</td>
                                    <td>32</td>
                                    <td>18</td>
                                </tr>
                                <tr>
                                    <td>Melekat, stiker</td>
                                    <td>15</td>
                                    <td>21</td>
                                    <td>60</td>
                                    <td>23</td>
                                    <td>10</td>
                                    <td>34</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>Selebaran</td>
                                    <td>19</td>
                                    <td>15</td>
                                    <td>34</td>
                                    <td>76</td>
                                    <td>15</td>
                                    <td>9</td>
                                    <td>95</td>
                                </tr>
                                <tr>
                                    <td>Berjalan, termasuk pada kendaraan</td>
                                    <td>10</td>
                                    <td>5</td>
                                    <td>9</td>
                                    <td>11</td>
                                    <td>12</td>
                                    <td>99</td>
                                    <td>98</td>
                                </tr>
                                <tr>
                                    <td>Udara</td>
                                    <td>5</td>
                                    <td>10</td>
                                    <td>64</td>
                                    <td>34</td>
                                    <td>23</td>
                                    <td>18</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>Apung</td>
                                    <td>7</td>
                                    <td>65</td>
                                    <td>12</td>
                                    <td>17</td>
                                    <td>76</td>
                                    <td>24</td>
                                    <td>26</td>
                                </tr>
                                <tr>
                                    <td>Suara</td>
                                    <td>11</td>
                                    <td>15</td>
                                    <td>67</td>
                                    <td>45</td>
                                    <td>23</td>
                                    <td>28</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>Film/slide</td>
                                    <td>13</td>
                                    <td>14</td>
                                    <td>17</td>
                                    <td>20</td>
                                    <td>23</td>
                                    <td>27</td>
                                    <td>19</td>
                                </tr>
                                <tr>
                                    <td>Peragaan</td>
                                    <td>20</td>
                                    <td>90</td>
                                    <td>18</td>
                                    <td>24</td>
                                    <td>34</td>
                                    <td>65</td>
                                    <td>16</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         </div>
       </div>
     </div>