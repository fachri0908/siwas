

<?php 

    $billboard_id                   = (isset($billboard[0]->id))? $billboard[0]->id : '';
    
    $applicant_code                 = (isset($billboard[0]->applicant_code))? $billboard[0]->applicant_code : '';
    $applicant_name                 = (isset($billboard[0]->applicant_name))? $billboard[0]->applicant_name : '';
    $applicant_phone_number         = (isset($billboard[0]->applicant_phone_number))? $billboard[0]->applicant_phone_number : '';
    $applicant_address              = (isset($billboard[0]->applicant_address))? $billboard[0]->applicant_address : '';
    $applicant_npwp                 = (isset($billboard[0]->applicant_npwp))? $billboard[0]->applicant_npwp : '';
    $applicant_ktp                  = (isset($billboard[0]->applicant_ktp))? $billboard[0]->applicant_ktp : '';
    
    $billboard_kategory             = (isset($billboard[0]->category))? $billboard[0]->category : '';
    $billboard_type_name            = (isset($billboard[0]->billboard_type_name))? $billboard[0]->billboard_type_name : '';
    $billboard_view_point_name      = (isset($billboard[0]->billboard_view_point_name))? $billboard[0]->billboard_view_point_name : '';
    $billboard_street_class_name    = (isset($billboard[0]->billboard_street_class_name))? $billboard[0]->billboard_street_class_name : '';
    $billboard_address              = (isset($billboard[0]->address))? $billboard[0]->address : '';
    $billboard_length               = (isset($billboard[0]->length))? $billboard[0]->length : '';
    $billboard_width                = (isset($billboard[0]->width))? $billboard[0]->width : '';
    $billboard_height               = (isset($billboard[0]->height))? $billboard[0]->height : '';
    $billboard_size                 = (isset($billboard[0]->size))? $billboard[0]->size : '';
    $billboard_nsr                  = (isset($billboard[0]->nsr))? $billboard[0]->nsr : '';
    $billboard_year_rate            = (isset($billboard[0]->year_rate))? $billboard[0]->year_rate : '';
    $billboard_install_date         = (isset($billboard[0]->install_date))? $billboard[0]->install_date : '';
    $billboard_finish_date          = (isset($billboard[0]->finish_date))? $billboard[0]->finish_date : '';
    $billboard_install_duration     = (isset($billboard[0]->install_duration))? $billboard[0]->install_duration : '';
    
?>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    
                    <div class="card-header">
                        <h3 class="mb-0">Data Pemohon</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-5"> Nama </div>
                                            <div class="col-lg-7"> : <?=$applicant_name?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> No. HP </div>
                                            <div class="col-lg-7"> : <?=$applicant_phone_number?> </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-5"> Alamat </div>
                                            <div class="col-lg-7"> : <?=$applicant_address?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-5"> Kategori </div>
                                            <div class="col-lg-7"> : <?=$billboard_kategory?> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> Jenis Reklame </div>
                                            <div class="col-lg-7"> : <?=$billboard_type_name?> </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-5"> Sudut Pandang </div>
                                            <div class="col-lg-7"> : <?=$billboard_view_point_name?> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> Area Pemasangan </div>
                                            <div class="col-lg-7"> : <?=$billboard_street_class_name?> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> Alamat Pemasangan </div>
                                            <div class="col-lg-7"> : <?=$billboard_address?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-5"> Panjang </div>
                                            <div class="col-lg-7"> : <?=$billboard_length?> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> Lebar </div>
                                            <div class="col-lg-7"> : <?=$billboard_width?> </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-5"> Tinggi Media </div>
                                            <div class="col-lg-7"> : <?=$billboard_height?> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> Luas </div>
                                            <div class="col-lg-7"> : <?=$billboard_size?> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5"> NSR </div>
                                            <div class="col-lg-7"> : <?=$billboard_nsr?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Permohonan</h3>
                    </div>
                    <div class="card-body">
                        <table width="100%" style="text-align:center">
                            <tr>
                                <th>
                                    Tarif Tahun
                                </th>
                                <th>
                                    Tanggal Pasang
                                </th>
                                <th>
                                    Tanggal Selesai
                                </th>
                                <th>
                                    Lama Pemasangan
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <?=$billboard_year_rate?>
                                </td>
                                <td>
                                    <?=$billboard_install_date?>
                                </td>
                                <td>
                                    <?=$billboard_finish_date?>
                                </td>
                                <td>
                                    <?=$billboard_install_duration?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    
                    <div class="card-header"></div>
                    <div class="row">
                        <div class="col-md-12 p-4 ml-4">
                            <!--<button type="button" class="btn btn-default" onclick="history.go(-1)"> Back </button>-->
                            <!--<a href="<?=base_url()?>billboards/edit/<?=$billboard_id?>"><button type="button" class="btn btn-primary"> Edit </button></a>-->
                        </div>
                      
                    </div>
                                
                </div>
            </div>
        </div>
    </div>
    
</div>

