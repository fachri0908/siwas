<div class="container-fluid mt--6">
    <?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <span class="alert-icon"><i class="ni ni-like-2"></i></span>
      <span class="alert-text"><strong>Sukses!</strong> <?=$this->session->flashdata('success')?></span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-basic">
                            <tr>
                                <th>Pemohon</th><td>:</td><td><?=$billboard[0]->code?></td>
                            </tr>
                            <tr>
                                <th>Lokasi Pemasangan</th><td>:</td><td><?=$billboard[0]->street_class?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card-wrapper">
                <div class="card">
                    <form method="POST">
                    <div class="card-header">
                        <h3 class="mb-0">Informasi Reklame</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label">Metode Pembayaran</label>
                            <select class="from-control" name="payment_method" data-toggle="select">
                                <option value="CSH">Cash</option>
                                <option value="CRD">Kartu Kredit</option>
                                <option value="DBT">Kartu Debit</option>
                                <option value="TRF">Transfer Bank</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Total Bayar</label>
                            <input type="text" class="form-control" name="total" placeholder="total bayar" value="<?=$billboard[0]->nsr*25/100?>">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

