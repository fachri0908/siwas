
                      
<?php
function limit_words($string, $word_limit){
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}
?>  

<div class="container-fluid mt--6">
   
   <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Daftar Pemberitahuan</h3>
                            <a class="btn btn-primary btn-sm float-right" href="<?=base_url()?>announcement/form">Tambah Pemberitahuan</a>
                    </div>
                    
                    <div class="row mx-2 mt-2">
                        <div class="col-md-12">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist" style="border-bottom:0">
                                <a data-toggle="tab" role="tab"  href="#active" class="btn btn-secondary active">Pemberitahuan Aktif</a>
                                <a data-toggle="tab" role="tab" href="#nonactive" class="btn btn-secondary">Pemberitahuan Tidak Aktif</a>
                            </div>
                        </div>
                    </div>
         
                    
                    
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="active" role="tabpanel">
                            <div class="table-responsive py-4">
                                <table class="table table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Judul Pemberitahuan</th>
                                            <th>Isi Pemberitahuan</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                        <?php $no = 1; foreach($announcement as $an) { ?>
                                            <?php if($an->announcement_status == 1) { ?>
                                            <tr>
                                                <td>
                                                    <?=$no?>
                                                </td>
                                                <td>
                                                    <?=$an->announcement_title?>
                                                </td>
                                                <td>
                                                    <?=limit_words($an->announcement_content, 8)?>
                                                    
                                                </td>
                                                
                                                <td>
                                                    <a href="#" class="btn btn-info btn-sm" onclick="showmodalannouncement('<?=$an->announcement_id?>')">View</a>
                                                    <a href="<?=base_url()?>announcement/form/<?=$an->announcement_id?>" class="btn btn-success btn-sm">Edit</a>
                                                    <a href="<?=base_url()?>announcement/delete/<?=$an->announcement_id?>" onclick="return confirm('Hapus ?')" class="btn btn-danger btn-sm">Delete</a>
                                                </td>
                                                
                                            </tr>
                                        <?php $no++; }  } ?>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        
                        <div class="tab-pane fade show" id="nonactive" role="tabpanel">
                            <div class="table-responsive py-4">
                               <table class="table table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Judul Pemberitahuan</th>
                                            <th>Isi Pemberitahuan</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                        <?php $no = 1; foreach($announcement as $an) { ?>
                                            <?php if($an->announcement_status == 0) { ?>
                                            <tr>
                                                <td>
                                                    <?=$no?>
                                                </td>
                                                <td>
                                                    <?=$an->announcement_title?>
                                                </td>
                                                <td>
                                                    <?=limit_words($an->announcement_content, 8)?>
                                                </td>
                                                
                                                <td>
                                                    <a href="#" class="btn btn-info btn-sm" onclick="showmodalannouncement('<?=$an->announcement_id?>')">View</a>
                                                    <a href="<?=base_url()?>announcement/form/<?=$an->announcement_id?>" class="btn btn-success btn-sm">Edit</a>
                                                    <a href="<?=base_url()?>announcement/delete/<?=$an->announcement_id?>" onclick="return confirm('Hapus ?')" class="btn btn-danger btn-sm">Delete</a>
                                                </td>
                                                
                                            </tr>
                                        <?php $no++; }  } ?>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
   
      
      
</div>




<div class="modal" id="modalannouncement">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"> PEMBERITAHUAN </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 mb-2" for="title">
                    <h2 class="text-center"> </h2>
                    <p class="text-center mt-n2 text-dark" style="font-size: 15px"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" for="content">
                    <p></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>



<script type="text/javascript">
    function showmodalannouncement(e) {
        $.ajax({
            type: "POST",
            url: "<?=base_url()?>applicants/announcement_detail",
            data : {
                'id' : e,  
            },
            dataType: 'json',
            success: function(data){
                var haris = ['Minggu', 'Senin','Selasa','Rabu','Kamis','Jum&#39;at','Sabtu'];
                var bulans = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                var dd = new Date(data.created_at).getDate();
                var mm = new Date(data.created_at).getMonth();
                var yy = new Date(data.created_at).getYear();
                var hari = new Date(data.created_at).getDay();
                var tahun = (yy < 1000) ? yy + 1900 : yy;
                $(".modal-body [for='title'] > h2").html(":: " + data.announcement_title + " ::");
                $(".modal-body [for='title'] > p").html("Dibuat pada : " + haris[hari]+', '+dd+' '+bulans[mm]+' '+tahun);
                $(".modal-body [for='content']").html(data.announcement_content);
                $("#modalannouncement").modal("show");
            }
        });
    }
</script>
