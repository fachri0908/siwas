
<div class="container-fluid mt--6">
   
   <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 float-left">Tambah Pemberitahuan Baru</h3>
                        <button class="btn btn-primary btn-sm float-right" onclick="history.go(-1)">Back</button>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            
                            <!--<div class="row">-->
                            <!--    <div class="col-md-2">-->
                            <!--        <label class="form-control-label">Perihal</label>-->
                            <!--    </div>-->
                            <!--    <div class="col-md-10">-->
                            <!--        <div class="form-group">-->
                            <!--            <div class="form-group">-->
                            <!--                <select class="form-control" name="announcement_status" data-toggle="select" required>-->
                            <!--                    <option value="Pemberitahuan">Pemberitahuan</option>-->
                            <!--                    <option value="Promo">Promo</option>-->
                            <!--                    <option value="Diskon">Diskon</option>-->
                            <!--                </select>-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            
                            
                            <!--<div class="row">-->
                            <!--    <div class="col-md-2">-->
                            <!--        <label class="form-control-label">Tanggal</label>-->
                            <!--    </div>-->
                            <!--    <div class="col-md-10">-->
                            <!--        <div class="form-group">-->
                            <!--            <input type="text" name="announcement_title" class="form-control form-control-small datepicker" placeholder="Judul Pemberitahuan" value="<?=@$announcement->announcement_title?>" required>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            
                            
                            
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="form-control-label">Judul Pemberitahuan </label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <input type="text" name="announcement_title" class="form-control form-control-small" placeholder="Judul Pemberitahuan" value="<?=@$announcement->announcement_title?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="form-control-label">Isi Pemberitahuan</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <textarea class="form-control" name="announcement_content" rows="6" placeholder="Isi Pemberitahuan" required><?=@$announcement->announcement_content?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="form-control-label">Status Pemberitahuan</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <select class="form-control" name="announcement_status" data-toggle="select" required>
                                                <option value="1" <?=(isset($announcement) && @$announcement->announcement_status == 1)?'selected':''?>>Aktif</option>
                                                <option value="0" <?=(isset($announcement) && @$announcement->announcement_status == 0)?'selected':''?>>Non - Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" name="announcement">Simpan</button>
                                    <button type="reset" class="btn btn-warning">Batal</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
      
      
</div>

