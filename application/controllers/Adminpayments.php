<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpayments extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	    $data['payments']=$this->builder->getAll('payments');
        $data['content'] = $this->load->view('payments/admin/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Tagihan','parent'=>'Tagihan','child'=>'Daftar Tagihan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function detail($code){
        $data['payment']=$this->builder->getRecordByCond('payments', ['code'=>$code]);
        $data['content'] = $this->load->view('payments/admin/detail', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Tagihan','parent'=>'Tagihan','child'=>'Daftar Tagihan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function confirm($code){
        $data=array(
            'status'=>'PAID'
        );
        $this->db->where('code', $code);
        if($this->db->update('payments', $data)){
            $this->session->set_flashdata('success','Pembayaran berhasil dikonfirmasi');
            redirect('adminpayments/detail/'.$code);
        }else{
            redirect('adminpayments/detail/'.$code);
        }
    }
    
}
