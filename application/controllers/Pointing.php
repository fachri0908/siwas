<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pointing extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        // if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	    $data['billboard_types']=$this->builder->getAll('billboard_types');
	    $data['categories']=$this->builder->getAll('categories');
	    $data['placement_billboard']=$this->builder->getAll('placement_billboard');
	    $data['cities']=$this->builder->getAll('city');
        $data['content'] = $this->load->view('pointing/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Peta Reklame','parent'=>'Reklame','child'=>'Peta Reklame','icon'=>'fa fa-map-marker text-red');
        $this->load->view('layouts/index', $data);
    }
    public function details($id=''){
        header('Content-Type: application/json');
        $data = $this->db->query("select a.*, b.name, b.company_name, c.name as type_name, d.name as category_name, e.name as place_name, f.name as land_name, g.name as street_name,
            h.name as district_name, i.name as view_point_name from billboards a 
            inner join applicants b on a.applicant_code=b.code
            inner join billboard_types c on a.billboard_type=c.code
            inner join categories d on a.category=d.code
            inner join placement_billboard e on a.placement_billboard=e.code
            inner join land_status f on a.land_status=f.code
            inner join streets g on a.street_id=g.id
            inner join districts h on a.district_id=h.id
            inner join view_points i on a.view_point=i.code
            and a.id='$id'")->result();
        print json_encode($data);
    }
    
    public function illegaldetails($id=''){
        header('Content-Type: application/json');
        $data = $this->builder->raw("select i.*,city.name as city_name,districts.name as district_name, streets.name as street_name, b.name as type_name from illegal_billboards i, city,districts,streets, billboard_types b where i.city_code=city.code and i.district_id=districts.id and i.street_id=streets.id and i.billboard_type=b.code and i.id='$id'");
        print json_encode($data);
    }
    public function points(){
        header('Content-Type: application/json');
        $data = $this->db->query("select 'legal' as type, a.id, a.address,a.latitude, a.longitude,a.approval_status, b.name, b.company_name from billboards a, applicants b where a.applicant_code=b.code and a.approval_status !=2 and a.deleted_at is null union all
            select 'ilegal' as type, a.id, concat(b.name,' ',c.name, ' ',d.name ) as address, latitude, longitude,'' as approval_status, '' as name , '' as company_name from illegal_billboards a left join city b on a.city_code=b.code left join districts c on a.district_id=c.id left join streets d on a.street_id=d.id where a.deleted_at is null")->result();
        print json_encode($data);
    }
    public function point_filter(){
        header('Content-Type: application/json');
        
        $formulir   = $this->input->post('formulir');
        $status     = $this->input->post('status');
        $city       = $this->input->post('city');
        $district   = $this->input->post('district');
        $type       = $this->input->post('type');
        $product    = $this->input->post('product');
        $placement  = $this->input->post('placement');
        
        $where = '';
        if($formulir){
            $where .= "and a.code='$formulir'";
        }
        if($status){
            $where .= "and a.approval_status='$city'";
        }
        if($city){
            $where .= "and a.city_code='$city'";
        }
        if($district){
            $where .= "and a.district_id='$district'";
        }
        if($type){
            $where .= "and a.billboard_type='$type'";
        }
        if($product){
            $where .= "and a.category='$product'";
        }
        if($placement){
            $where .= "and a.placement_billboard='$placement'";
        }
        
        if($status == 4){ // ilegal
            $data = $this->db->query("select 'ilegal' as type, a.id, concat(b.name,' ',c.name, ' ',d.name ) as address, latitude, longitude,'' as approval_status, '' as name , '' as company_name from illegal_billboards a left join city b on a.city_code=b.code left join districts c on a.district_id=c.id left join streets d on a.street_id=d.id where a.deleted_at is null")->result();
        } else {
            $data = $this->db->query("select 'legal' as type, a.id, a.address,a.latitude, a.longitude,a.approval_status, b.name, b.company_name from billboards a, applicants b where a.applicant_code=b.code and a.approval_status !=2 and a.deleted_at is null $where union all
                select 'ilegal' as type, a.id, concat(b.name,' ',c.name, ' ',d.name ) as address, latitude, longitude,'' as approval_status, '' as name , '' as company_name from illegal_billboards a left join city b on a.city_code=b.code left join districts c on a.district_id=c.id left join streets d on a.street_id=d.id where a.deleted_at is null")->result();
        }
        print json_encode($data);
    }
    
}
