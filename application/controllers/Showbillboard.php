<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Showbillboard extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
    }
    public function index(){
        $billboardcode=$this->input->get('code');
        $billboard = $this->builder->getRecordByCond('billboards',['code'=>$billboardcode]);
        $data['billboard']=$billboard;
        $data['attachment']=$this->builder->getRecordByCond('registration_attachments',['billboard_code'=>$billboardcode]);
        $data['applicant'] = $this->builder->getRecordByCond('applicants', ['code'=>$billboard[0]->applicant_code]);
        $this->load->view('layouts/show-billboard', $data);
    }
}
