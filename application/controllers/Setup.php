<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setup extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        $this->isPost=$this->input->method(TRUE);
        // $this->userId=$this->session->userdata('id');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
    
	public function viewpoint(){
        //update and insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('view_points',$data,'setup/viewpoint');
            }else{
                $data=array(
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                    'created_by'=>1
                );
                $this->builder->store('view_points',$data,'setup/viewpoint');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('view_points', $this->input->get('delete'),'setup/viewpoint');
        }
        //default view
        $data['viewpoints']=$this->builder->getAll('view_points');
        $data['content'] = $this->load->view('setup/viewpoint/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Sudut Pandang','parent'=>'Setup','child'=>'Sudut Pandang','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    


    public function rates(){
        //update and insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $rate=array(
                    'id'=>$this->input->get('update'),
                    'year'=>$this->input->post('year'),
                    'rate'=>$this->input->post('rate'),
                );
                $this->builder->update('rates',$rate,'setup/rates');
            }else{
                $rate=array(
                    'year'=>$this->input->post('year'),
                    'rate'=>$this->input->post('rate'),
                    'created_by'=>1
                );
                $this->builder->store('rates',$rate,'setup/rates');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('rates', $this->input->get('delete'),'setup/rates');
        }
        //default view
        $data['rates']=$this->builder->getAll('rates');
        $data['content'] = $this->load->view('setup/rates/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Rates','parent'=>'Tarif','child'=>'Data Tarif','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }

    public function njor(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'type'=>$this->input->post('type'),
                    'size_price'=>$this->input->post('size_price'),
                    'height_price'=>$this->input->post('height_price'),
                );
                $this->builder->update('njor',$data,'setup/njor');
                
            }else{
                $data=array(
                    'type'=>$this->input->post('type'),
                    'size_price'=>$this->input->post('size_price'),
                    'height_price'=>$this->input->post('height_price'),
                    'created_by'=>1
                );
                $this->builder->store('njor',$data,'setup/njor');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('njor', $this->input->get('delete'),'setup/njor');
        }
        //default view
        $data['njors']=$this->db->query('select njor.id, njor.type, njor.size_price,njor.height_price,type.name from njor, billboard_types type where njor.type=type.id and njor.deleted_at is null')->result();
        $data['types']=$this->builder->getAll('billboard_types');
        $data['content'] = $this->load->view('setup/njor/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Rates','parent'=>'Tarif','child'=>'Data Tarif','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function nsr(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'type'=>$this->input->post('type'),
                    'street_class'=>$this->input->post('street_class'),
                    'rate'=>$this->input->post('rate'),
                );
                $this->builder->update('nsr',$data,'setup/nsr');
                
            }else{
                $data=array(
                    'type'=>$this->input->post('type'),
                    'street_class'=>$this->input->post('street_class'),
                    'rate'=>$this->input->post('rate'),
                    'created_by'=>1
                );
                $this->builder->store('nsr',$data,'setup/nsr');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('nsr', $this->input->get('delete'),'setup/nsr');
        }
        //default view
        $data['nsr']=$this->db->query("select nsr.*,street_class.name from nsr,street_class where nsr.street_class=street_class.code and nsr.deleted_at is null")->result();
        $data['streetclass']=$this->builder->getAll('street_class');
        $data['content'] = $this->load->view('setup/nsr/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'NSR','parent'=>'NSR','child'=>'Data NSR','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
     public function nsr_led(){
        // update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'street_class'=>$this->input->post('street_class'),
                    'start_size'=>$this->input->post('start_size'),
                    'end_size'=>$this->input->post('end_size'),
                    'price'=>$this->input->post('tarif'),
                    'uom'=>$this->input->post('uom'),
                );
                $this->builder->update('nsr_price',$data,'setup/nsr_led');
                
            }else{
                $data=array(
                    'street_class'=>$this->input->post('street_class'),
                    'start_size'=>$this->input->post('start_size'),
                    'end_size'=>$this->input->post('end_size'),
                    'price'=>$this->input->post('tarif'),
                    'uom'=>$this->input->post('uom'),
                    'created_by'=>1
                );
                $this->builder->store('nsr_price',$data,'setup/nsr_led');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('nsr_price', $this->input->get('delete'),'setup/nsr_led');
        }
        
        
        //default view
        $data['nsr_price']=$this->db->query("select * from nsr_price left join billboard_types on nsr_price.billboard_type = billboard_types.code left join street_class on nsr_price.street_class = street_class.code where nsr_price.deleted_at is null")->result();
        // print_r($data['nsr_price']);
        $data['street_class']=$this->builder->getAll('street_class');
        $data['uom']=$this->builder->getAll('uom');
        foreach($data['street_class'] as $rs) {
            $data['nsr'][$rs->code] = $this->db->query("select * from nsr_price where street_class = '{$rs->code}' and deleted_at is null")->result();
        }
        $data['content'] = $this->load->view('setup/nsr_led/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'NSR LED','parent'=>'NSR LED','child'=>'Data NSR LED','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
    
    
    public function type(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                if($this->input->post('is_single_price') == 1){
                    $data=array(
                        'id'=>$this->input->get('update'),
                        'code'=>$this->input->post('code'),
                        'name'  =>$this->input->post('name'),
                        'price' => 0,
                        'uom'=> '',
                        'description'=>$this->input->post('description'),
                    );
                    $this->db->where('id', $this->input->get('update'));
                    $this->db->update('billboard_types', $data);
                    
                    $this->db->where('billboard_type_id', $this->input->get('update'));
                    $this->db->delete('price_class');
                    
                    $street_class_id    = $this->input->post('street_class_id');
                    $price_class        = str_replace(array(',','.'),'',$this->input->post('price_class'));
                    $price_uom          = $this->input->post('price_uom');
                    for($i = 0; $i < count($street_class_id); $i++){
                        if($street_class_id[$i] != '' && $price_class[$i] != 0){
                            $data = array(
                                'billboard_type_id' => $this->input->get('update'),
                                'street_class_id'   => $street_class_id[$i],
                                'uom'               => $price_uom[$i],
                                'price'             => $price_class[$i],
                                'created_at'        => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('price_class', $data);
                        }
                    }
                } else {
                    $data=array(
                        'id'=>$this->input->get('update'),
                        'code'=>$this->input->post('code'),
                        'name'=>$this->input->post('name'),
                        'price' => str_replace(array(',','.'),'',$this->input->post('price')),
                        'uom'=>$this->input->post('uom'),
                        'description'=>$this->input->post('description'),
                    );
                    $this->db->where('id', $this->input->get('update'));
                    $this->db->update('billboard_types', $data);
                }
                redirect('setup/type');
                
            }else{
                if($this->input->post('is_single_price') == 1){
                    $data=array(
                        'code'=>$this->input->post('code'),
                        'name'=>$this->input->post('name'),
                        'price'=> 0,
                        'uom'=> '',
                        'description'=>$this->input->post('description'),
                        'created_by'=>1
                    );
                    $this->db->insert('billboard_types', $data);
                    $id = $this->db->insert_id();
                    
                    $street_class_id    = $this->input->post('street_class_id');
                    $price_class        = str_replace(array(',','.'),'',$this->input->post('price_class'));
                    $price_uom          = $this->input->post('price_uom');
                    for($i = 0; $i < count($street_class_id); $i++){
                        if($street_class_id[$i] != '' && $price_class[$i] != 0){
                            $data = array(
                                'billboard_type_id' => $id,
                                'street_class_id'   => $street_class_id[$i],
                                'uom'               => $price_uom[$i],
                                'price'             => $price_class[$i],
                                'created_at'        => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('price_class', $data);
                        }
                    }  
                } else {
                    $data=array(
                        'code'=>$this->input->post('code'),
                        'name'=>$this->input->post('name'),
                        'price'=> str_replace(array(',','.'),'',$this->input->post('price')),
                        'uom'=>$this->input->post('uom'),
                        'description'=>$this->input->post('description'),
                        'created_by'=>1
                    );
                    $this->db->insert('billboard_types', $data);
                    $id = $this->db->insert_id();
                }
                
                redirect('setup/type');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('billboard_types', $this->input->get('delete'),'setup/type');
        }
        //default view
        $data['types']=$this->builder->getAll('billboard_types');
        $data['content'] = $this->load->view('setup/billboard_types/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Tipe Reklame','parent'=>'Tipe Reklame','child'=>'Data Tipe Reklame','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    public function penempatan($id=''){
        echo '<div class="row"><div class="col-md-2 penempatan label-penempatan">
                    <label class="form-control-label">Penempatan</label>
                </div><div class="col-md-3 penempatan">
                    <div class="form-group">';
                        $data = $this->db->query("select a.id, a.name from street_class a inner join price_class b on a.id=b.street_class_id where b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select a.id, a.name from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                            echo '<select class="form-control" style="margin-top: 10px" name="street_class_id[]" readonly>
                                <option value="'.$ar->id.'">'.$ar->name.'</option>
                            </select>';
                        }
                    echo '</div>
                </div>
                <div class="col-md-3 penempatan">
                    <div class="form-group">';
                        $data = $this->db->query("select b.price from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select b.price from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                           echo '<input type="text" class="form-control req2" onkeyup="currency(this)" style="margin-top: 10px" name="price_class[]" value="'.number_format($ar->price).'" placeholder="Tarif">';
                        }
                    echo '</div>
                </div>
                <div class="penempatan" style="width: 5px;">';
                    $data = $this->db->query("select a.id, a.name from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                    if(empty($data)){
                        $data = $this->db->query("select a.id, a.name from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                    }
                    foreach($data as $ar){
                        echo '<label class="form-control-label" style="margin-top: 2px"><h1>/</h1></label>';
                    }
                echo '</div>
                <div class="col-md-3 penempatan" style="margin-left: -5px;">
                    <div class="form-group">';
                        $data = $this->db->query("select b.uom from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select b.uom from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                            echo '<select class="form-control req2" style="margin-top: 10px" name="price_uom[]">
                                <option value=""> Unit </option>';
                                $data2 = $this->db->query("select * from uom")->result();
                                foreach($data2 as $arr){
                                    $selected = '';
                                    if($ar->uom == $arr->uom_name){ $selected = 'selected'; }
                                    echo '<option value="'.$arr->uom_name.'" '.$selected.'>'.$arr->uom_name.'</option>';
                                }
                            echo '</select>';
                        }
                    echo '</div>
                </div></div>';
    }
    
    
    
    
    
    
    public function penempatandev($id=''){
        
        echo '<div class="row"><div class="col-md-2 penempatan label-penempatan">
                    <label class="form-control-label">Penempatan</label>
                </div>
                
                
                <div class="col-md-2 penempatan ">
                    <div class="form-group">';
                        $data = $this->db->query("select a.id, a.name from street_class a inner join price_class b on a.id=b.street_class_id where b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select a.id, a.name from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                            echo '<select class="form-control" style="margin-top: 10px" name="street_class_id[]" readonly>
                                <option value="'.$ar->id.'">'.$ar->name.'</option>
                            </select>';
                        }
                    echo '</div>
                </div>
                
                
                
                <div class="col-md-1 penempatan size">
                    <div class="form-group">';
                        $data = $this->db->query("select b.start_size from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select b.price from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                           echo '<input type="text" class="form-control req2" onkeyup="" style="margin-top: 10px" name="start_size[]" value="'.$ar->start_size.'" placeholder="">';
                        }
                    echo '</div>
                </div>
                
                
                <div class="penempatan slas" style="width: 5px;">';
                    $data = $this->db->query("select a.id, a.name from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                    if(empty($data)){
                        $data = $this->db->query("select a.id, a.name from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                    }
                    foreach($data as $ar){
                        echo '<label class="form-control-label" style="margin-top: 2px"><h1>-</h1></label>';
                    }
                echo '</div>
                
                
                
                <div class="col-md-1 penempatan size">
                    <div class="form-group">';
                        $data = $this->db->query("select b.end_size from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select b.price from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                           echo '<input type="text" class="form-control req2" onkeyup="currency(this)" style="margin-top: 10px" name="end_size[]" value="'.$ar->end_size.'" placeholder="">';
                        }
                    echo '</div>
                </div>
                
                
                
                
                <div class="col-md-2 penempatan price">
                    <div class="form-group">';
                        $data = $this->db->query("select b.price from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select b.price from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                           echo '<input type="text" class="form-control req2" onkeyup="currency(this)" style="margin-top: 10px" name="price_class[]" value="'.number_format($ar->price).'" placeholder="Tarif">';
                        }
                    echo '</div>
                </div>
                
                
                
                <div class="penempatan slas" style="width: 5px;">';
                    $data = $this->db->query("select a.id, a.name from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                    if(empty($data)){
                        $data = $this->db->query("select a.id, a.name from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                    }
                    foreach($data as $ar){
                        echo '<label class="form-control-label" style="margin-top: 2px"><h1>/</h1></label>';
                    }
                echo '</div>
                
                
                
                <div class="col-md-3 penempatan uom" style="margin-left: -5px;">
                    <div class="form-group">';
                        $data = $this->db->query("select b.uom from street_class a, price_class b where a.id=b.street_class_id and b.billboard_type_id='$id' order by a.id")->result();
                        if(empty($data)){
                            $data = $this->db->query("select b.uom from street_class a left join price_class b on a.id=b.street_class_id order by a.id")->result();
                        }
                        foreach($data as $ar){
                            echo '<select class="form-control req2" style="margin-top: 10px" name="price_uom[]">
                                <option value=""> Unit </option>';
                                $data2 = $this->db->query("select * from uom")->result();
                                foreach($data2 as $arr){
                                    $selected = '';
                                    if($ar->uom == $arr->uom_name){ $selected = 'selected'; }
                                    echo '<option value="'.$arr->uom_name.'" '.$selected.'>'.$arr->uom_name.'</option>';
                                }
                            echo '</select>';
                        }
                    echo '</div>
                    
                    
                </div></div>';
    }
    
    
    
    
	public function prices(){
        //update and insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'billboard_type_id' => $this->input->get('id'),
                    'street_class_id'   => $this->input->post('street_class_id'),
                    'description'       => $this->input->post('description'),
                );
                $this->builder->update('price_class',$data,'setup/prices?id='.$this->input->get('id').'');
            }else{
                $data=array(
                    'billboard_type_id' => $this->input->get('id'),
                    'street_class_id'   => $this->input->post('street_class_id'),
                    'description'       => $this->input->post('description'),
                    'created_by'        => 1
                );
                $this->builder->store('price_class',$data,'setup/prices?id='.$this->input->get('id').'');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('price_class', $this->input->get('delete'),'setup/prices?id='.$this->input->get('id').'');
        }
        $id = $this->input->get('id');
        //default view
        $data['prices'] = $this->db->query("select a.*, b.name as type_name, c.name as class_name from price_class a, billboard_types b, street_class c where a.billboard_type_id='$id' and a.billboard_type_id=b.id and a.street_class_id=c.id and a.deleted_at=''")->result();
        $data['content'] = $this->load->view('setup/prices/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Harga per kelas','parent'=>'Setup','child'=>'Harga per kelas','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function street_class(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('street_class',$data,'setup/street_class');
                
            }else{
                $data=array(
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                    'created_by'=>1
                );
                $this->builder->store('street_class',$data,'setup/street_class');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('street_class', $this->input->get('delete'),'setup/street_class');
        }
        //default view
        $data['streetclass']=$this->builder->getAll('street_class');
        $data['content'] = $this->load->view('setup/street_class/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Kelas','parent'=>'Kelas','child'=>'Data Kelas','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function category(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                    'description'=>$this->input->post('description'),
                );
                $this->builder->update('categories',$data,'setup/category');
                
            }else{
                $data=array(
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                    'description'=>$this->input->post('description'),
                    'created_by'=>1
                );
                $this->builder->store('categories',$data,'setup/category');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('categories', $this->input->get('delete'),'setup/category');
        }
        //default view
        $data['types']=$this->builder->getAll('categories');
        $data['content'] = $this->load->view('setup/categories/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Jenis','parent'=>'Jenis','child'=>'Data Jenis Reklame','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function city(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('city',$data,'setup/city');
                
            }else{
                $data=array(
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                    'created_by'=>1
                );
                $this->builder->store('city',$data,'setup/city');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('city', $this->input->get('delete'),'setup/city');
        }
        //default view
        $data['cities']=$this->builder->getAll('city');
        $data['content'] = $this->load->view('setup/city/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Kota','parent'=>'Kota','child'=>'Data Kota','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function district(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'name'=>$this->input->post('name'),
                    'city_code'=>$this->input->post('city_code'),
                );
                $this->builder->update('districts',$data,'setup/district');
                
            }else{
                $data=array(
                    'name'=>$this->input->post('name'),
                    'city_code'=>$this->input->post('city_code'),
                    'created_by'=>1
                );
                $this->builder->store('districts',$data,'setup/district');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('districts', $this->input->get('delete'),'setup/district');
        }
        //default view
        $data['districts']=$this->builder->raw("select districts.*,(select count(streets.id) from streets where district_id=districts.id and streets.deleted_at is null) as counter from districts where districts.deleted_at is null");
        $data['cities']=$this->builder->getAll('city');
        $data['streetclass']=$this->builder->getAll('street_class');
        $data['content'] = $this->load->view('setup/districts/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Kecamatan','parent'=>'Kota','child'=>'Data Kecamatan','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function street(){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'street_class_code'=>$this->input->post('street_class_code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('streets',$data,'setup/district');
                
            }else{
                $data=array(
                    'district_id'=>$this->input->post('district_id'),
                    'name'=>$this->input->post('name'),
                    'street_class_code'=>$this->input->post('street_class_code'),
                    'created_by'=>1
                );
                $this->builder->store('streets',$data,'setup/street');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('streets', $this->input->get('delete'),'setup/district');
        }
        //default view
        $data['streets']=$this->builder->getAll('streets');
        $data['districts']=$this->builder->getAll('districts');
        $data['streetclass']=$this->builder->getAll('street_class');
        $data['content'] = $this->load->view('setup/streets/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Jalan','parent'=>'Kecamatan','child'=>'Data Jalan','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function districtdetail($id){
        //update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'street_class_code'=>$this->input->post('street_class_code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('streets',$data,'setup/districtdetail/'.$id);
                
            }else{
                $data=array(
                    'district_id'=>$id,
                    'name'=>$this->input->post('name'),
                    'street_class_code'=>$this->input->post('street_class_code'),
                    'created_by'=>1
                );
                $this->builder->store('streets',$data,'setup/districtdetail/'.$id);
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('streets', $this->input->get('delete'),'setup/districtdetail/'.$id);
        }
        //default view
        // $data['streets']=$this->db->query("select streets.*, street_class.name as area_name from streets,street_class where streets.street_class_code=street_class.code and streets.deleted_at is null")->result();
        $data['streets']=$this->builder->getrecordByCond("streets",['district_id'=>$id]);
        $data['streetclass']=$this->builder->getAllOrder('street_class','created_at','ASC');
        $data['districtid']=$id;
        $districtname=$this->db->query("select name from districts where id='$id'")->result();
        $data['districtname']=$districtname[0]->name;
        $data['content'] = $this->load->view('setup/streets/index', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Jalan','parent'=>'Kecamatan','child'=>'Data Jalan','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function addstreetbackup($id){
        $x=0;
        foreach($this->input->post('name') as $name){
            $data=array(
                'district_id'=>$id,
                'name'=>$name,
                'street_class_code'=>$this->input->post('street_class_code')[$x],
                'created_by'=>1
            );
            if($name!=''){
                $this->db->insert('streets',$data);
            }
            $x++;
        }
        
        redirect('setup/district');
    }
    
    public function addstreet($id){
        $x=0;
        foreach($this->input->post('name') as $name){
            $data=array(
                'district_id'=>$id,
                'name'=>$name,
                'street_class_code'=>$this->input->post('street_class_code')[$x],
                'created_by'=>1
            );
            if($name!=''){
                $this->db->insert('streets',$data);
            }
            $x++;
        }
        $this->session->set_flashdata('success','Berhasil Menambahkan Data');
        redirect('setup/districtdetail/'.$id);
    }
    
    
    
    public function status_tanah(){
        //update or insert
        
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('land_status',$data,'setup/status_tanah');
                
            }else{
                $data=array(
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->store('land_status',$data,'setup/status_tanah');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('land_status', $this->input->get('delete'),'setup/status_tanah');
        }
        
        //default view
        $data['land_status']=$this->builder->getAll('land_status');
        $data['content'] = $this->load->view('setup/status_tanah/index', $data, TRUE);
        $data['breadcrumb'] =array('title'=>'Status Tanah','parent'=>'Status Tanah','child'=>'Data Status Tanah','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
    
    public function uom(){
        
        
        // update or insert
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'uom_code'=>$this->input->post('code'),
                    'uom_name'=>$this->input->post('name'),
                );
                $this->builder->update('uom',$data,'setup/uom');
                
            }else{
                $data=array(
                    'uom_code'=>$this->input->post('code'),
                    'uom_name'=>$this->input->post('name'),
                );
                $this->builder->store('uom',$data,'setup/uom');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('uom', $this->input->get('delete'),'setup/uom');
        }
        
        //default view
        
        $data['uom']=$this->builder->getAll('uom');
        $data['content'] = $this->load->view('setup/uom/index', $data, TRUE);
        $data['breadcrumb'] =array('title'=>'UOM','parent'=>'UOM','child'=>'Data UOM','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
    public function letak_reklame(){
        //update or insert
        
        if($this->isPost=="POST"){
            if($this->input->get('update')!=''){
                $data=array(
                    'id'=>$this->input->get('update'),
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->update('placement_billboard',$data,'setup/letak_reklame');
                
            }else{
                $data=array(
                    'code'=>$this->input->post('code'),
                    'name'=>$this->input->post('name'),
                );
                $this->builder->store('placement_billboard',$data,'setup/letak_reklame');
            }
        }
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('placement_billboard', $this->input->get('delete'),'setup/letak_reklame');
        }
        
        //default view
        $data['placement_billboard']=$this->builder->getAll('placement_billboard');
        $data['content'] = $this->load->view('setup/letak_reklame/index', $data, TRUE);
        $data['breadcrumb'] =array('title'=>'Status Tanah','parent'=>'Status Tanah','child'=>'Data Status Tanah','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
    public function typedev(){
        
        //update or insert
        if($this->isPost=="POST"){
            
            // update
            if($this->input->get('update')!=''){
                
                // array
                if($this->input->post('is_single_price') == 1){
                    $data=array(
                        'id'=>$this->input->get('update'),
                        'code'=>$this->input->post('code'),
                        'name'  =>$this->input->post('name'),
                        'price' => 0,
                        'uom'=> '',
                        'description'=>$this->input->post('description'),
                    );
                    $this->db->where('id', $this->input->get('update'));
                    $this->db->update('billboard_types', $data);
                    
                    $this->db->where('billboard_type_id', $this->input->get('update'));
                    $this->db->delete('price_class');
                    
                    $street_class_id    = $this->input->post('street_class_id');
                    $price_class        = str_replace(array(',','.'),'',$this->input->post('price_class'));
                    $price_uom          = $this->input->post('price_uom');
                    $start_size         = $this->input->post('start_size');
                    $end_size           = $this->input->post('end_size');
                    
                    for($i = 0; $i < count($street_class_id); $i++){
                        if($street_class_id[$i] != '' && $price_class[$i] != 0){
                            $data = array(
                                'billboard_type_id' => $this->input->get('update'),
                                'street_class_id'   => $street_class_id[$i],
                                'uom'               => $price_uom[$i],
                                'price'             => $price_class[$i],
                                'start_size'        => $start_size[$i],
                                'end_size'          => $end_size[$i],
                                'created_at'        => date('Y-m-d H:i:s')
                            );
                            
                            $this->db->insert('price_class', $data);
                        }
                    }
                // not array
                } else {
                    $data=array(
                        'id'=>$this->input->get('update'),
                        'code'=>$this->input->post('code'),
                        'name'=>$this->input->post('name'),
                        'price' => str_replace(array(',','.'),'',$this->input->post('price')),
                        'uom'=>$this->input->post('uom'),
                        'description'=>$this->input->post('description'),
                    );
                    $this->db->where('id', $this->input->get('update'));
                    $this->db->update('billboard_types', $data);
                }
                redirect('setup/typedev');
                
                
            // insert
            }else{
                
                // array
                if($this->input->post('is_single_price') == 1){
                    $data=array(
                        'code'=>$this->input->post('code'),
                        'name'=>$this->input->post('name'),
                        'price'=> 0,
                        'uom'=> '',
                        'description'=>$this->input->post('description'),
                        'created_by'=>1
                    );
                    
                    $this->db->insert('billboard_types', $data);
                    $id = $this->db->insert_id();
                    
                    $street_class_id    = $this->input->post('street_class_id');
                    $price_class        = str_replace(array(',','.'),'',$this->input->post('price_class'));
                    $price_uom          = $this->input->post('price_uom');
                    $start_size         = $this->input->post('start_size');
                    $end_size           = $this->input->post('end_size');
                    
                    for($i = 0; $i < count($street_class_id); $i++){
                        if($street_class_id[$i] != '' && $price_class[$i] != 0){
                            $data = array(
                                'billboard_type_id' => $id,
                                'street_class_id'   => $street_class_id[$i],
                                'uom'               => $price_uom[$i],
                                'price'             => $price_class[$i],
                                'start_size'        => $start_size[$i],
                                'end_size'          => $end_size[$i],
                                'created_at'        => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('price_class', $data);
                        }
                    }  
                    
                // not array
                } else {
                    $data=array(
                        'code'=>$this->input->post('code'),
                        'name'=>$this->input->post('name'),
                        'price'=> str_replace(array(',','.'),'',$this->input->post('price')),
                        'uom'=>$this->input->post('uom'),
                        'description'=>$this->input->post('description'),
                        'created_by'=>1
                    );
                    $this->db->insert('billboard_types', $data);
                    $id = $this->db->insert_id();
                }
                
                redirect('setup/typedev');
            }
        }
        
        //delete
        if($this->input->get('delete')!=''){
            $this->builder->destroy('billboard_types', $this->input->get('delete'),'setup/type');
        }
        //default view
        $data['types']=$this->builder->getAll('billboard_types');
        $data['content'] = $this->load->view('setup/billboard_types/dev', $data, TRUE);
        
        $data['breadcrumb'] =array('title'=>'Tipe Reklame','parent'=>'Tipe Reklame','child'=>'Data Tipe Reklame','icon'=>'ni ni-settings text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
}
