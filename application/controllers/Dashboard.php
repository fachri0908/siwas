<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Dashboard extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('mdynamics');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
    
    
    
    
    
	public function index(){
	    $tahun = [ date('Y') - 0, date('Y') - 1, date('Y') - 2];
        $bulan = ["Januari","Februari","Maret","April","Mai","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        foreach ($tahun as $th) {
            $data['data_label'][$th] = '';
            $data['jumlah'][$th] = '';
            $bln = 1;
            foreach ($bulan as $bl) {
                $data_  = count($this->db->query("select * from billboards where SUBSTRING(created_at,1,4) = $th and SUBSTRING(created_at,6,2) = $bln")->result());
                $data__ = $this->db->query("select sum(nsr) as total from billboards where SUBSTRING(created_at,1,4) = $th and SUBSTRING(created_at,6,2) = $bln")->result()[0]->total;
              $text=array('','','RB','JT','ML','TR');
              $input=explode('.',number_format($data__, 0,',','.'));
              $data__ = $input[0].(count($input)>1?'.'.round($input[1]/10):'').' '.$text[count($input)];
                $data['data_label'][$th]    .= $data_ . ',';
                $data['jumlah'][$th]        .= $data__ . ',';
                $bln++;
            }
            $data['data_label'][$th] = substr($data['data_label'][$th], 0, -1);
            $data['jumlah'][$th] = substr($data['jumlah'][$th], 0, -1);
        }
        
	   // $data['menunggu_persetujuan']   = count($this->db->query("select * from billboards where approval_status = 0")->result());
	    $data['menunggu_persetujuan']   = count($this->db->query("select billboards.*, applicants.name as applicant_name, street_class.name as area_name from billboards,applicants,street_class where billboards.applicant_code=applicants.code and billboards.street_class=street_class.code and billboards.is_draft=0 and billboards.deleted_at is null")->result());
	    $data['disetujui']              = count($this->db->query("select * from billboards where approval_status = 1")->result());
	    $data['ditolak']                = count($this->db->query("select * from billboards where approval_status = 2")->result());
	    $data['ilegal']                 = count($this->db->query("select illegal_billboards.*,streets.name as street_name from illegal_billboards,streets where illegal_billboards.street_id=streets.id and illegal_billboards.deleted_at is null")->result());
	    
	    
	    
        
	    $data['street_class'] = $this->db->query("select * from street_class where deleted_at is null")->result();
	    foreach($data['street_class'] as $sc) {
	        $data['count_street_class'][$sc->code] = count($this->db->query("select * from billboards where street_class = '{$sc->code}' and deleted_at is null")->result());
	        $num = $data['count_street_class'][$sc->code] / count($data['street_class']) * 100;
	        $data['percentase_street_class'][$sc->code] = round($num, 2) . '%';
	    }
	    
	   $data['all_billboard'] = $this->db->query("select * from billboards where deleted_at is null")->result();
	   $data['top_three'] = $this->db->query("SELECT *, COUNT(*) AS ttl from billboards a left join billboard_types b on a.billboard_type = b.code where a.deleted_at is null group by billboard_type order by ttl DESC limit 3")->result();
        
        
        $data['breadcrumb'] = array('title'=>'Dashboard','parent'=>'Dashboard','child'=>'','icon'=>'fa fa-list');
        $data['content'] = $this->load->view('dashboard/index', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }







    function dev() {
        
        // ditolak
        // $data = $this->db->query("select * from billboards where approval_status = 2")->result();
        
        // ilegal
        // $data = $this->db->query("select illegal_billboards.*,streets.name as street_name from illegal_billboards,streets where illegal_billboards.street_id=streets.id and illegal_billboards.deleted_at is null")->result();
        
        // $data = count($this->db->query("select * from billboards where SUBSTRING(created_at,1,4) = 2019 and SUBSTRING(created_at,6,2) = 10 ")->result());
        
        // print_r($data);
        
        $tahun = [ date('Y') - 0, date('Y') - 1, date('Y') - 2];
        $bulan = ["Januari","Februari","Maret","April","Mai","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        
        foreach ($tahun as $th) {
            
            $data['data_label'][$th] = '';
            $data['jumlah'][$th] = '';
            $bln = 1;
            
            foreach ($bulan as $bl) {
                $data_  = count($this->db->query("select * from billboards where SUBSTRING(created_at,1,4) = $th and SUBSTRING(created_at,6,2) = $bln")->result());
                $data__ = $this->db->query("select sum(nsr) as total from billboards where SUBSTRING(created_at,1,4) = $th and SUBSTRING(created_at,6,2) = $bln")->result()[0]->total;
                
              $text=array('','','RB','JT','ML','TR');
              $input=explode('.',number_format($data__, 0,',','.'));
              $data__ = $input[0].(count($input)>1?'.'.round($input[1]/10):'').' '.$text[count($input)];
                    
                $data['data_label'][$th]    .= $data_ . ',';
                $data['jumlah'][$th]        .= $data__ . ',';
                $bln++;
            }
            
            $data['data_label'][$th] = substr($data['data_label'][$th], 0, -1);
            $data['jumlah'][$th] = substr($data['jumlah'][$th], 0, -1);
        }
        
        foreach ($tahun as $th) {
            echo json_encode(explode(',', $data['data_label'][$th]));
            echo json_encode(explode(',', $data['jumlah'][$th]));
            echo "<hr>";
        }
        
    }


    public function compare(){
        $data['breadcrumb'] = array('title'=>'Dashboard','parent'=>'Dashboard','child'=>'target','icon'=>'fa fa-list');
        $data['content'] = $this->load->view('dashboard/compare', '', TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    
}
