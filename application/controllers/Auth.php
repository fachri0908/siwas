<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('bcrypt');
        $this->load->model('builder');
    }
    
	public function index(){
	    if($this->session->userdata('is_login')){
            redirect('');
        }
        if(isset($_POST['login'])){
            $email      = $this->input->post('email');
            $password   = $this->input->post('password');
            $user = $this->db->query("select * from users where email = '$email'")->result();
            //if user exist
            if(count($user)>0){
                if ($this->bcrypt->check_password($password, $user[0]->password)){
                    	// Password does match stored password.
                    
                    $userid=$user[0]->id;
                    //if login user
                    if($user[0]->role=='usr'){
                        if($user[0]->validated_at==NULL){
                            $this->session->set_flashdata('flash','
                                <div class="alert alert-warning alert-dismissable fade show">
                                    <span class="alert-text"><strong>Gagal!</strong> akun belum aktivasi<br>ikuti tautan di email anda untuk aktivasi akun. <a href="'.base_url().'auth/resendverification">tidak mendapat email..?</span>
                                </div>');
                            redirect('auth');
                        }else{
                            $apps = $this->db->query("select * from applicants where user_id = '$userid'")->result();
                            if($apps[0]->approved_at==NULL){
                                $approoved=0;
                            }else{
                                $approoved=1;
                            }
                            $userdata = array(
                                'user_id'       => $user[0]->id,
                                'applicant_id'  => $apps[0]->id,
                                'user_name'     => $apps[0]->name,
                                'user_email'    => $user[0]->email,
                                'user_code'     => $apps[0]->code,
                                'approoved'     => $approoved,
                                'is_login'      => TRUE,
                                'role'          => 'usr',
                                'picture'       => $apps[0]->profile_picture,
                                'third_person'  => $apps[0]->third_person
                            );
                            $this->session->set_userdata($userdata);
                            redirect('applicants/dashboard');
                        }
                    }else{
                        //if login is admin
                        $adm = $this->db->query("select * from employees where user_id = '$userid'")->result();
                        $userdata = array(
                            'user_id'       => $user[0]->id,
                            'user_code'     => $adm[0]->code,
                            'user_position' => $adm[0]->position_code,
                            'user_name'     => $user[0]->email,
                            'user_email'    => $user[0]->email,
                            'is_login'      => TRUE,
                            'role'          => 'adm'
                        );
                        $this->session->set_userdata($userdata);
                        redirect('');
                    }
                }else{
                	$this->session->set_flashdata('flash','
                        <div class="alert alert-danger alert-dismissable fade show">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h2>Password Salah</h2> Please try again.
                        </div>');
                    redirect('auth');
                }
            }else{
                $this->session->set_flashdata('flash','
                    <div class="alert alert-danger alert-dismissable fade show">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2>Akun tidak ditemukan</h2> coba lagi.
                    </div>');
                redirect('auth');
            }
        }
        $this->load->view('layouts/login');
    }
    public function register(){
        if($this->session->userdata('is_login')){
            redirect('');
        }
        $this->load->library('libemail');
        if($_POST){
            $str=md5(rand());
            $datausr=array(
                'email'=>$this->input->post('email'),
                'password'=>$this->bcrypt->hash_password($this->input->post('password')),
                'token'=>$str,
                'role'=>'usr'
            );
            if($this->db->insert('users',$datausr)){
                $data=array(
                    'user_id'=>$this->db->insert_id(),
                    'code'=>$this->builder->getNewApplicantCode(),
                    'name'=>$this->input->post('name'),
                    'address'=>$this->input->post('address'),
                    'phone_number'=>$this->input->post('phone_number'),
                    'third_person'=>$this->input->post('third_person'),
                );
                if($this->db->insert('applicants',$data)){
                    $emaildata['usertoken']=$str;
                    $html = $this->load->view('emailtemplates/confirmemail',$emaildata, TRUE);
                    $this->libemail->send_email($this->input->post('email'),'Aktivasi akun SIWAS',$html);
                    $this->session->set_flashdata('success','Berhasil Mendaftar, ikuti tautan pada email anda untuk aktivasi akun');
                    redirect('auth');
                }else{
                    $this->session->set_flashdata('failed','Gagal Menambahkan Data');
                    redirect('auth/register');
                }
            }
        }
        $this->load->view('layouts/register');
    }
    
    
    public function logout(){
        $this->session->sess_destroy();
        redirect('auth');
    }
    
    
	public function index_backup(){
	    if($this->session->userdata('logged_in')=='true'){
            redirect('/schedule');
        }
        if(isset($_POST['login'])){
            $email=$this->input->post('email');
            $user = $this->db->query("select * from users where user_email ='$email'")->result();
            if(count($user)>0){
                $password = $this->input->post('password');
                $user_password = $user[0]->user_password;
                if(password_verify($password,$user_password)){
                    $userdata = array(
                        'user_id'       => $user[0]->user_id,
                        'user_name'     => $user[0]->user_fullname,
                        'user_email'    => $user[0]->user_email,
                        'logged_in'      => 'true'
                    );
                    $this->session->set_userdata($userdata);
                    redirect('booking');
                } else {
                    $this->session->set_flashdata('flash','<div class="alert alert-warning alert-dismissable">
                    <span class="alert-text"><strong>Gagal!</strong> akun belum aktivasi<br>ikuti tautan di email anda untuk aktivasi akun.<br> <a href="'.base_url().'auth/resendverification">tidak mendapat email..?</span>
                    </div>');
                    redirect('auth?salah');
                }
            }else{
                $this->session->set_flashdata('flash','<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h3>Email atau password</h3>
                        silahkan coba lagi
                    </div>');
                redirect('auth?salah=true');
            }
        }
        $this->load->view('layouts/login');
    }
    
    public function checkemail(){
        $email=$this->input->get('email');
        $check=$this->db->query("select email from users where email='$email'")->result();
        if(count($check)>0){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    public function verify($token){
        $check=$this->db->query("select id from users where token ='$token'")->result();
        if(count($check)>0){
            $id=$check[0]->id;
            $this->db->where('id',$id);
            $this->db->update('users',array('validated_at'=>date('Y-m-d H:i:s')));
            $this->session->set_flashdata('success','Akun anda telah aktif, silahkan login untuk masuk ke dashboard');
            redirect("auth");
        }else{
            $this->session->set_flashdata('flash','<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span class="alert-text"><strong>Gagal!</strong> Token tidak valid</span>
                    </div>');
            redirect('auth');
        }
    }
    
    public function resendverification(){
        if($this->session->userdata('is_login')){
            redirect('');
        }
        $this->load->view('layouts/resendverification');
    }
    
    public function forgetpassword(){
        if($this->session->userdata('is_login')){
            redirect('');
        }
        $this->load->view('layouts/resetpassword');
    }
    
    public function sendresetlink(){
        $this->load->library('libemail');
        $email=$this->input->post('email');
        $check=$this->db->query("select email,token from users where email ='$email'")->result();
        if(count($check)>0){
            $emaildata['usertoken']=$check[0]->token;
            $html = $this->load->view('emailtemplates/resetpassword',$emaildata, TRUE);
            $this->libemail->send_email($this->input->post('email'),'Reset password SIWAS',$html);
            $this->session->set_flashdata('success','link reset password telah dikirim ke email anda');
            redirect("auth/resendverification");
        }else{
            $this->session->set_flashdata('flash','<div class="alert alert-danger alert-dismissable">
                    <span class="alert-text"><strong>Gagal!</strong> akun tidak ditemukan</span>
                    </div>');
            redirect('auth/forgetpassword');
        }
    }
    
    public function changepassword(){
        $token=$this->input->get('token');
        if($_POST){
            $email=$this->input->post('email');
            $check=$this->db->query("select id from users where token ='$token' and email='$email'")->result();
            if(count($check)>0){
                $this->db->where('id', $check[0]->id);
                $this->db->update('users',array('password'=>$this->bcrypt->hash_password($this->input->post('password'))));
                $this->session->set_flashdata('success','Password telah diperbarui, silahkan login untuk masuk ke dashboard');
                redirect("auth");
            }else{
                $this->session->set_flashdata('flash','<div class="alert alert-danger alert-dismissable">
                        <span class="alert-text"><strong>Gagal!</strong> akun tidak ditemukan</span>
                        </div>');
                redirect('auth/forgetpassword');
            }
        }
        $this->load->view('layouts/changepassword');
    }
    
    public function reverify(){
        $this->load->library('libemail');
        $email=$this->input->post('email');
        $check=$this->db->query("select email,token from users where email ='$email'")->result();
        if(count($check)>0){
            $emaildata['usertoken']=$check[0]->token;
            $html = $this->load->view('emailtemplates/confirmemail',$emaildata, TRUE);
            $this->libemail->send_email($this->input->post('email'),'Aktivasi akun SIWAS',$html);
            $this->session->set_flashdata('success','link aktivasi sudah dikirim ke email anda');
            redirect("auth/resendverification");
        }else{
            $this->session->set_flashdata('flash','<div class="alert alert-danger alert-dismissable">
                    <span class="alert-text"><strong>Gagal!</strong> akun tidak ditemukan atau sudah diverifikasi</span>
                    </div>');
            redirect('auth/resendverification');
        }
    }
}
