<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculator extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	    $data['rates']=$this->builder->getAll('rates');
	    $data['street_class']=$this->builder->getAll('street_class');
	    $data['types']=$this->builder->getAll('billboard_types');
	    $data['viewpoints']=$this->builder->getAll('view_points');
	    $data['nsr']=$this->builder->getAll('nsr');
        $data['content'] = $this->load->view('calculator/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Kalkulator','parent'=>'Kalkulator','child'=>'hitung','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function getNsr(){
        $cat=$this->input->get('cat');
        $class=$this->input->get('class');
        $rate=$this->builder->raw("select rate from nsr where type='$cat' and street_class='$class'");
        if(count($rate)>0){
            echo $rate[0]->rate;
        }else{
            echo '-';
        }
    }
    
    function getTax(){
        $year=$this->input->get('year');
        $rate=$this->builder->raw("select rate from rates where year='$year'");
        echo $rate[0]->rate;
    }
}
