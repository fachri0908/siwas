<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lists extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        // $this->userId=$this->session->userdata('id');
    }
    
    
	public function streetByDistrict($id){
        $data['streets'] = $this->db->query("select streets.*, street_class.name as area_name from streets,street_class where streets.street_class_code=street_class.code and streets.district_id='$id' and streets.deleted_at is null order by created_at")->result();
        $this->load->view('tables/streets',$data);
    }
    
    public function districtByCity($code){
        $data= $this->db->query("select id,name from districts where city_code='$code' and deleted_at is null")->result();
        echo json_encode($data);
    }
    
    public function streetListByDistrict($id){
        $data= $this->db->query("select id,name from streets where district_id='$id' and deleted_at is null")->result();
        echo json_encode($data);
    }
    public function streetClassByStreet($id){
        $data=$this->db->query("select a.street_class_code, b.name from streets a, street_class b where a.street_class_code=b.code and a.id='$id'")->result();
        echo "<option value='".$data[0]->street_class_code."'>".$data[0]->name."</option>";
    }
    public function streetClassByStreetName($id){
        $data=$this->db->query("select name from streets where id='$id'")->result();
        echo $data[0]->name;
    }
    public function streetAddress($id){
        $data=$this->db->query("select c.name as city, b.name as district, a.name as street from streets a, districts b, city c where a.district_id=b.id and b.city_code=c.code and a.id='$id'")->result();
        echo $data[0]->city.', '.$data[0]->district.', '.$data[0]->street;
    }
    
    public function infobillboard($filter){
        $now=date('Y-m-d');
        if($filter==4){
            $data['billboards']=$this->builder->raw("select illegal_billboards.*,streets.name as street_name,billboard_types.name as type from illegal_billboards, streets, billboard_types where illegal_billboards.street_id=streets.id and illegal_billboards.billboard_type=billboard_types.code and illegal_billboards.deleted_at is NULL");
            $this->load->view('tables/illegal-billboard', $data);
        }else{
            if($filter==1){
                $data['billboards']=$this->builder->raw("select billboards.*,billboard_types.name as type from billboards,billboard_types where billboards.billboard_type=billboard_types.code and  date(billboards.finish_date) > '2019-10-11' and billboards.approval_status=1");
            }else if($filter==2){
                $data['billboards']=$this->builder->raw("select billboards.*,billboard_types.name as type from billboards,billboard_types where billboards.billboard_type=billboard_types.code and billboards.approval_status=0 and billboards.is_draft=0");
            }else if($filter==3){
                $data['billboards']=$this->builder->raw("select billboards.*,billboard_types.name as type from billboards,billboard_types where billboards.billboard_type=billboard_types.code and approval_status=2 and is_draft=0");
            }else{
                $data['billboards']=[];
            }
            $this->load->view('tables/info-billboard', $data);
        }
    }
    
    public function billboardmap(){
	    $data['billboard_types']=$this->builder->getAll('billboard_types');
	    $data['categories']=$this->builder->getAll('categories');
	    $data['placement_billboard']=$this->builder->getAll('placement_billboard');
	    $data['cities']=$this->builder->getAll('city');
        $this->load->view('tables/billboard-map', $data);
    }
    
    public function getIllegalImage($url){
        echo "<img src='".base_url()."assets/image-upload/illegal-billboards/".$url."' width='400px'>";
    }
    
    public function getImage($code){
        $images=$this->builder->raw("select file from registration_attachments where billboard_code='$code' and type='BD'");
        foreach($images as $i){
            if($i->file != NULL || $i->file !=''){
                echo "<img src='".base_url()."assets/image-upload/registration_attachments/".$i->file."' width='170px' style='margin-right:5px;margin-bottom:5px'>";
            }
        }
    }
    
    
    
    function getnsr(){
        $area=$this->input->get('area'); //kelas jalan (protokol a, b atau lingkungan dll)
        $type=$this->input->get('type');//tipe reklame
        $category=$this->input->get('category'); //kategori produk non produk
        $isCigarette=$this->input->get('cigarette'); //rokok atau bukan
        $location=$this->input->get('location');//lokasi pemasangan
        $height=$this->input->get('height');//tinggi reklame
        $size=$this->input->get('size');//luas reklame
        $days=$this->input->get('total_days');//jumlah hari pemasangan
        $times=$this->input->get('total_seconds');//jumlah detik penayangan (led/suara/film)
        $slbTotal=$this->input->get('total');
        $month=$this->input->get('total');//jumlah bulan penayangan (udara)
        $totalPrg=$this->input->get('total');//total peragaan(reklame peragaan only)
        
        //base for others
        if($type=='PBM' || $type=='KN'){
            $nsr=$this->db->query("select rate from nsr where street_class='$area' and type='$category'")->result();
            if(count($nsr)==0){
                echo "-".die();
            }
            $nsr=$nsr[0]->rate;
            $nsr=$nsr*$size*$days;
        }else if($type=="LED"){
             //base for led
            $nsr=$this->db->query("select price from nsr_price where start_size <= $size and end_size >= $size")->result();
            $nsr=$nsr[0]->price;
            $nsr=$nsr*$times;
        }else if($type=="STK"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"STK"]);
            $nsr=$nsr[0]->price*$size;
        }else if($type=="SLB"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"BRJ"]);
            $nsr=$nsr[0]->price*$slbTotal;
        }else if($type=="BRJ"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"BRJ"]);
            $nsr=$nsr[0]->price*$size*$days;
        }else if($type=="UDR"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"UDR"]);
            $nsr=$nsr[0]->price;
        }else if($type=="APG"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"APG"]);
            $nsr=$nsr[0]->price;
        }else if($type=="SR"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"SR"]);
            $nsr=$nsr[0]->price;
            $nsr=$nsr * (ceil($times/30));
        }else if($type=="FLM"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"FLM"]);
            $nsr=$nsr[0]->price;
            $nsr=$nsr * (ceil($times/30));
        }else if($type=="PRG"){
            $nsr=$this->builder->getRecordByCond('nsr_rate',['billboard_type_code'=>"PRG"]);
            $nsr=$nsr[0]->price * $totalPrg;
        }
        
        if($isCigarette==1){
            $nsr=@$nsr+(25/100*@$nsr);
        }
        
        if($height>15){
            $mheight=$height-15;//kelebihan 15meter
            $mheight=ceil($mheight/15);
            $nsr=$nsr+(20/100*$mheight*$nsr);
        }
        
        if($location=='INDR'){
            $nsr=$nsr/2;
        }
        
        echo $nsr;
    }
    
    
    
    
    function gettax(){
        $nsr=$this->input->get('nsr');
        $value=$this->input->get('contract');
        if($this->session->userdata('third_person')==1){
            $tax=25/100*$value;
        }else{
            $tax=25/100*$nsr;
        }
        echo $tax;
    }
    
    function getbasensr(){
        $cat=$this->input->get('category');
        $area=$this->input->get('area');
        $data=$this->builder->raw("select rate from nsr where type='$cat' and street_class='$area'");
        if(count($data)>0){
            echo $data[0]->rate;
        }else{
            echo "-";
        }
    }
}
