<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Illegals extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }

    public function index(){
        $data['illegals'] = $this->builder->raw("select illegal_billboards.*,streets.name as street_name from illegal_billboards,streets where illegal_billboards.street_id=streets.id and illegal_billboards.deleted_at is null");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Reklame Ilegal','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('illegals/index', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
     public function create(){
        if($_POST){
            $picture='';
            if(!empty($_FILES['picture']['name'])){
                //upload file
                $ext        = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
                $config['upload_path']          = 'assets/image-upload/illegal-billboards/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10000;
                // $config['file_name']            = "namafile".$ext;
                $config['encrypt_name']         = TRUE;
    
                $this->load->library('upload', $config);
    
                if ( ! $this->upload->do_upload('picture')){
                    $picture='';
                }
                else{
                    $file= $this->upload->data();
                    $picture=$file['file_name'];
                }
            }
            $latlong=explode(",",$this->input->post('latitude_longitude'));
            $lat=$latlong[0];
            $long=$latlong[1];
            $data=array(
                'billboard_text'=>$this->input->post('billboard_text'),
                'billboard_type'=>$this->input->post('billboard_type'),
                'city_code'=>$this->input->post('city_code'),
                'district_id'=>$this->input->post('district_id'),
                'street_id'=>$this->input->post('street_id'),
                'latitude'=>$lat,
                'longitude'=>$long,
                'picture'=>$picture,
                'created_by'=>$this->session->userdata('user_id')
            );
            $this->builder->store('illegal_billboards', $data,'illegals');
        }
        $data['city'] = $this->builder->getAll("city");
        $data['types'] = $this->builder->getAll("billboard_types");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Reklame Ilegal','child'=>'tambah reklame ilegal','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('illegals/form', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function show($id){
        if($_POST){
            $billboard = $this->builder->raw("select picture from illegal_billboards where id='$id'");
            $picture=$billboard[0]->picture;
            if(!empty($_FILES['picture']['name'])){
                //upload file
                $ext        = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
                $config['upload_path']          = 'assets/image-upload/illegal-billboards/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10000;
                // $config['file_name']            = "namafile".$ext;
                $config['encrypt_name']         = TRUE;
    
                $this->load->library('upload', $config);
    
                if ( ! $this->upload->do_upload('picture')){
                    $picture='';
                }
                else{
                    $file= $this->upload->data();
                    $picture=$file['file_name'];
                }
            }
            $latlong=explode(",",$this->input->post('latitude_longitude'));
            $lat=$latlong[0];
            $long=$latlong[1];
            $editdata=array(
                'id'=>$id,
                'billboard_text'=>$this->input->post('billboard_text'),
                'billboard_type'=>$this->input->post('billboard_type'),
                'city_code'=>$this->input->post('city_code'),
                'district_id'=>$this->input->post('district_id'),
                'street_id'=>$this->input->post('street_id'),
                'latitude'=>$lat,
                'longitude'=>$long,
                'picture'=>$picture,
                'created_by'=>$this->session->userdata('user_id')
            );
            $this->builder->update('illegal_billboards',$editdata,'illegals');
        }
        $billboard = $this->builder->raw("select illegal_billboards.*,districts.id as district_id,districts.city_code from illegal_billboards,streets,districts where illegal_billboards.street_id=streets.id and streets.district_id=districts.id and illegal_billboards.id='$id'");
        $citycode=$billboard[0]->city_code;
        $districts=$this->builder->getRecordByCond('districts',['city_code'=>$citycode]);
        $data['illegal']=$billboard;
        $data['districts']=$districts;
        $data['city']=$this->builder->getAll('city');
        $data['streets']=$this->builder->getRecordByCond('streets', ['district_id'=>$billboard[0]->district_id]);
        $data['types'] = $this->builder->getAll("billboard_types");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Reklame Ilegal','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('illegals/edit-form', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
}
