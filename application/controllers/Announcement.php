<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
        $this->isPost=$this->input->method(TRUE);
    }

    public function index(){
        
        $data['announcement'] = $this->db->query("select * from announcement where deleted_at is null")->result();
        $data['breadcrumb'] =array('title'=>'Announcement','parent'=>'Announcement','child'=>'Announcement','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('announcement/index', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    

    public function form($id=""){
        if (isset($_POST['announcement'])) {
            $data = [
                    'announcement_title'     => $this->input->post('announcement_title'),
                    'announcement_content'   => $this->input->post('announcement_content'),
                    'announcement_status'    => $this->input->post('announcement_status'),
                ];
            if($id) {
                $data = array_merge($data, ['updated_at' => date('Y-m-d H:i:s'), 'updated_by' => $this->session->userdata('user_id')]);
                $this->db->update('announcement',$data,['announcement_id'=>$id]);
            } else {
                $data = array_merge($data, ['created_at' => date('Y-m-d H:i:s'), 'created_by' => $this->session->userdata('user_id')]);
                $this->db->insert('announcement',$data);
            }
            redirect('announcement');
        }
        if($id) {
            $data['announcement'] = $this->db->query("select * from announcement where announcement_id = $id")->result()[0];
        }
        $data['breadcrumb'] =array('title'=>'Announcement','parent'=>'Announcement','child'=>' Announcement','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('announcement/create', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    
    public function delete($id=""){
        $this->db->update('announcement',['deleted_at' => date('Y-m-d H:i:s')],['announcement_id'=>$id, 'deleted_by' => $this->session->userdata('user_id')]);
        redirect('announcement');
    }
    
    
    
}
