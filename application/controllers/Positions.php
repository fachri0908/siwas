<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Positions extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	    if($_POST){
            $data=array(
                'code'=>$this->input->post('code'),
                'name'=>$this->input->post('name'),
                'created_by'=>$this->session->userdata('user_id')
            );
            $this->builder->store('positions',$data,'positions');
        }
	    $data['positions']=$this->builder->getAll('positions');
        $data['content'] = $this->load->view('positions/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Jabatan','parent'=>'Jabatan','child'=>'Edit Jabatan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function update($id){
        $data=array(
            'id'=>$id,
            'code'=>$this->input->post('code'),
            'name'=>$this->input->post('name'),
        );
        $this->builder->update('positions',$data,'positions');
    }
    
    public function delete($id){
        $this->builder->destroy('positions', $id,'positions');
    }
}
