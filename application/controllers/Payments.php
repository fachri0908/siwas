<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	    $data['payments']=$this->builder->getRecordByCond('payments', ['applicant_code'=>$this->session->userdata('user_code')]);
        $data['content'] = $this->load->view('payments/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Tagihan','parent'=>'Tagihan','child'=>'Daftar Tagihan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function detail($code){
        $data['payment']=$this->builder->getRecordByCond('payments', ['code'=>$code]);
        $data['content'] = $this->load->view('payments/detail', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Tagihan','parent'=>'Tagihan','child'=>'Daftar Tagihan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function uploadpayment($code){
        $this->load->library('upload'); 
        $dir = "./assets/image-upload/payments/";
        if (!is_dir($dir)) { mkdir($dir, 0777, true); }
        if(!empty($_FILES['file']['name'])){
          $name = 'PAYMENT-'.$code;
          $_FILES['file']['name']        = $_FILES['file']['name'];
          $_FILES['file']['type']        = $_FILES['file']['type'];
          $_FILES['file']['tmp_name']    = $_FILES['file']['tmp_name'];
          $_FILES['file']['error']       = $_FILES['file']['error'];
          $_FILES['file']['size']        = $_FILES['file']['size'];
          $config['upload_path']         = $dir;
          $config['allowed_types']       = 'jpg|jpeg|png|gif';
          $config['max_size']            = '5000';
          $config['file_name']           = $name;
          $this->upload->initialize($config);
          $this->upload->do_upload('file');
          $file_info = $this->upload->data();
          $img = $file_info['file_name'];
          
            $registration_attachments = [
                'file' =>$img,
                'payment_method'=>$this->input->post('payment_method'),
            ];
            $this->db->where('code', $code);
            $this->db->update('payments', $registration_attachments);
            redirect('payments');
        }
    }
    
    
}
