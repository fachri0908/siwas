<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        $this->load->library('bcrypt');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	    $data['employees']=$this->builder->getAll('employees');
        $data['content'] = $this->load->view('employees/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Pegawai','parent'=>'Pegawai','child'=>'Daftar Pegawai','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function create(){
        if($_POST){
            $str=md5(rand());
            $datausr=array(
                'email'=>$this->input->post('email'),
                'password'=>$this->bcrypt->hash_password($this->input->post('password')),
                'token'=>$str,
                'validated_at'=>date('Y-m-d H:i:s'),
                'role'=>'adm'
            );
            
            $userid=$this->builder->storeReturnId('users', $datausr,'employees/create');
            $data=array(
                'user_id'=>$userid,
                'code'=>$this->builder->getNewEmployeeCode(),
                'name'=>$this->input->post('name'),
                'position_code'=>$this->input->post('position_code'),
                'created_by'=>$this->session->userdata('user_id'),
            );
            $this->builder->store('employees',$data,'employees');
        }
        $data['positions']=$this->builder->getAll('positions');
        $data['content'] = $this->load->view('employees/create', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Pegawai','parent'=>'Pegawai','child'=>'tambah pegawai','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
}
