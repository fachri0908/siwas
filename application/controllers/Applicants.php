<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applicants extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('bcrypt');
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
    
    
	public function index(){
	    if($_POST){
	        $data=array(
	            'id'=>$this->session->userdata('applicant_id'),
                'name'=>$this->input->post('name'),
                'address'=>$this->input->post('address'),
                'phone_number'=>$this->input->post('phone_number'),
                'company_name'=>$this->input->post('company_name'),
                'company_address'=>$this->input->post('company_address'),
                'company_phone'=>$this->input->post('company_phone'),
                'company_position'=>$this->input->post('company_position'),
                'company_fax'=>$this->input->post('company_position'),
                'company_npwpd'=>$this->input->post('company_npwpd'),
            );
            $this->builder->update('applicants',$data,'applicants');
	    }
	    $data['applicants']=$this->builder->getRecordByCond('applicants',['id'=>$this->session->userdata('applicant_id')]);
        $data['content'] = $this->load->view('applicants/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Profile','parent'=>'Profile','child'=>'Edit Profile','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
	public function dashboard(){
        $data['is_drat']    = $this->db->query("select count(*) as total_is_draft from billboards where is_draft = 1 and applicant_code = '{$this->session->userdata('user_code')}' and deleted_at is null")->result()[0]->total_is_draft;
        $data['diajukan']   = $this->db->query("select count(*) as total_diajukan from billboards where applicant_code = '{$this->session->userdata('user_code')}' and is_draft = 0 and approval_status = 0 and deleted_at is null")->result()[0]->total_diajukan;
        $data['disetujui']  = $this->db->query("select count(*) as total_disetujui from billboards where applicant_code = '{$this->session->userdata('user_code')}' and approval_status = 1 and deleted_at is null")->result()[0]->total_disetujui;
	    $data['ditolak']    = $this->db->query("select count(*) as total_ditolak from billboards where applicant_code = '{$this->session->userdata('user_code')}' and approval_status = 2 and deleted_at is null")->result()[0]->total_ditolak;
        $x = date('Y-m-d');
        $y = date('Y-m-d', strtotime('-1 month', strtotime($x)));
        $data['soonexpired']   = $this->db->query("select count(*) as total_soonexpired from billboards where applicant_code = '{$this->session->userdata('user_code')}' and approval_status = 1 and finish_date < {$y} and deleted_at is null")->result()[0]->total_soonexpired;
        $data['expired']       = $this->db->query("select count(*) as total_expired from billboards where applicant_code = '{$this->session->userdata('user_code')}' and approval_status = 1 and finish_date < {$x} and deleted_at is null")->result()[0]->total_expired;
        $data['announcement'] = $this->db->query("select * from announcement where announcement_status = 1 and deleted_at is null")->result();
        $data['content'] = $this->load->view('applicants/dashboard', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Dashboard','parent'=>'Dashboard','child'=>'Dashboard','icon'=>'fa fa-list');
        $this->load->view('layouts/index', $data);
	}
	
	
	function announcement_detail() {
        header('Content-Type: application/json');
	    $data = $this->db->query("select * from announcement where announcement_id = {$this->input->post('id')}")->result()[0];
        print json_encode($data);
	}
	
	
	function detail_map_reklame() {
	    $data['bb'] = $this->db->query("
	                    select a.*,
	                        b.name as category_name,
	                        c.name as billboard_type_name,
	                        d.name as view_point_name,
	                        e.name as street_name,
	                        f.name as district_name,
	                        g.name as city_name,
	                        h.name as street_class_name
        	                    from billboards a
        	                        left join categories b on a.category = b.code
        	                        left join billboard_types c on a.billboard_type = c.code
        	                        left join view_points d on a.view_point = d.code
        	                        left join streets e on a.street_id = e.id
        	                        left join districts f on a.district_id = f.id
        	                        left join city g on a.city_code = g.code
        	                        left join street_class h on a.street_class = h.code
            	                        where a.id = {$this->input->post('id')}
        	                                ")->result()[0];
        $data['desain_billboard'] = $this->db->query("select * from registration_attachments where billboard_code = '{$this->input->post('code')}' and type = 'BD'")->result();
	    $this->load->view('applicants/map-modal', $data);
	}
	
	
	
	
    public function points(){
        header('Content-Type: application/json');
        $data = $this->db->query("select * from billboards where applicant_code = '{$this->session->userdata('user_code')}' and deleted_at is null")->result();
        print json_encode($data);
    }
    
    
    
    public function billboard(){
        $applicant_code=$this->session->userdata('user_code');
        $data['billboards']=$this->builder->raw("select id,code,billboard_type,billboard_text,is_draft,approval_status,finish_date, (select count(id) from registration_attachments where billboard_code=billboards.code) as total_atc, (select count(id) from registration_attachments where billboard_code=billboards.code and file is not NULL) as uploaded_atc from billboards where applicant_code ='$applicant_code' and billboards.installed=0 and deleted_at is NULL");
        $data['content'] = $this->load->view('applicants/billboards', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Reklame','parent'=>'Reklame','child'=>'daftar reklame','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function changepassword(){
        if($_POST){
            $old=$this->input->post('old_password');
            $new=$this->bcrypt->hash_password($this->input->post('new_password'));
            $userid=$this->session->userdata('user_id');
            $account=$this->builder->raw("select id,password from users where id='$userid'");
            if ($this->bcrypt->check_password($old, $account[0]->password)){
                $this->db->where('id',$account[0]->id);
                $this->db->update('users',array('password'=>$new));
                $this->session->set_flashdata('success','Password berhasil diubah');
                redirect("applicants/changepassword");
            }else{
                $this->session->set_flashdata('flash','<div class="alert alert-danger alert-dismissable">
                    <span class="alert-text"><strong>Gagal!</strong> Password yang anda inputkan tidak sesuai</span>
                    </div>');
                redirect('applicants/changepassword');
            }
        }
        $data['content'] = $this->load->view('applicants/changepassword', '', TRUE);
        $data['breadcrumb'] = array('title'=>'Profile','parent'=>'Profile','child'=>'ganti password','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function billboarddetail($code){
       $billboard = $this->builder->getRecordByCond('billboards',['code'=>$code]);
        $applcode=$billboard[0]->applicant_code;
        $data['applicant']=$this->builder->getRecordByCond('applicants',['code'=>$applcode]);
        $data['levels']=$this->builder->getAllOrder('approval_levels','level','ASC');
        $data['billboard']=$billboard;
        $data['attachments']=$this->builder->raw("select r.*, atc.name as atc_name,atc.regex from registration_attachments r,attachment_types atc where r.type=atc.code and r.billboard_code='$code'");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Setup','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('applicants/billboard-detail', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function installbillboard($code){
        $this->db->where('code', $code);
        if($this->db->update('billboards', array('installed'=>1))){
            $this->session->set_flashdata('success','Reklame di set terpasang');
            redirect('applicants/billboards');
        }else{
            redirect('applicants/billboards');
        }
    }
    
    public function installed(){
        $applicant_code=$this->session->userdata('user_code');
        $data['billboards']=$this->builder->raw("select id,code,billboard_type,billboard_text,is_draft,approval_status,finish_date, (select count(id) from registration_attachments where billboard_code=billboards.code) as total_atc, (select count(id) from registration_attachments where billboard_code=billboards.code and file is not NULL) as uploaded_atc from billboards where applicant_code ='$applicant_code' and billboards.installed=1 and deleted_at is NULL");
        $data['content'] = $this->load->view('applicants/installed', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Reklame','parent'=>'Reklame Terpasang','child'=>'reklame terpasang','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
}
