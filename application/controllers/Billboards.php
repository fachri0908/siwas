<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billboards extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
        $this->isPost=$this->input->method(TRUE);
    }
    

    public function index(){
        $data['billboards'] = $this->db->query("select billboards.*, applicants.name as applicant_name, street_class.name as area_name from billboards,applicants,street_class where billboards.applicant_code=applicants.code and billboards.street_class=street_class.code and billboards.is_draft=0 and billboards.installed=0 and billboards.deleted_at is null")->result();
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Setup','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('billboards/index', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function installed(){
        $data['billboards'] = $this->db->query("select billboards.*, applicants.name as applicant_name, street_class.name as area_name from billboards,applicants,street_class where billboards.applicant_code=applicants.code and billboards.street_class=street_class.code and billboards.installed=1 and billboards.deleted_at is null")->result();
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Reklame','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('billboards/installed', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    
    
    public function edit($id){
        
        if($_POST) {
            
            
            $billboard=[
                'applicant_code'=>$this->input->post('applicant_code'),
                'category'=>$this->input->post('category'),
                'billboard_type'=>$this->input->post('billboard_type'),
                'view_point'=>$this->input->post('view_point'),
                'street_class'=>$this->input->post('street_class'),
                'city_code'=>$this->input->post('city_code'),
                'district_id'=>$this->input->post('district_id'),
                'street_id'=>$this->input->post('street_id'),
                'address'=>$this->input->post('install_address'),
                'length'=>$this->input->post('length'),
                'width'=>$this->input->post('width'),
                'height'=>$this->input->post('height'),
                'size'=>$this->input->post('size'),
                'year_rate'=>$this->input->post('year_rate'),
                'install_date'=>$this->input->post('install_date'),
                'finish_date'=>$this->input->post('finish_date'),
                'nsr'=>$this->input->post('nsr'),
                'tax'=>$this->input->post('tax'),
            ];
            // print_r($billboard).die();
            $result = $this->updateByKey('billboards', $billboard, ['id'=>$id], 'billboards');
            if($result == 'Ok'){
                $this->session->set_flashdata('success',' Data Berhasil Diubah');
            }else{
                $this->session->set_flashdata('failed','Gagal Merubah Data');
            }
            redirect('billboards');
        }
        
        
        $data['rates']=$this->builder->getAll('rates');
        $data['street_class']=$this->builder->getAll('street_class');
        $data['types']=$this->builder->getAll('billboard_types');
        $data['viewpoints']=$this->builder->getAll('view_points');
        $data['nsr']=$this->builder->getAll('nsr');
	    $data['cities']=$this->builder->getAll('city');
        $data['billboards_record'] = $this->builder->getRecordById('billboards', ['id' => $id]);
        $data['applicants_record'] = $this->builder->getRecordById('applicants', ['code' => $data['billboards_record'][0]->applicant_code]);
	    $data['districts']=$this->builder->getRecordById('districts', ['city_code' => $data['billboards_record'][0]->city_code]);
	    $data['streets']=$this->builder->getRecordById('streets', ['district_id' => $data['billboards_record'][0]->district_id]);
        $data['breadcrumb'] = array('title'=>'Billboards','parent'=>'Billboards','child'=>'Edit Data','icon'=>'ni ni-folder-17 text-green');
        $data['content'] = $this->load->view('billboards/edit', $data, TRUE);
        // $data['content'] = $this->load->view('billboards/form', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    
    
    
    public function create_dev($type){
		if(!empty($_FILES['file']['name'])){
		    $ext        = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		    $path       = 'assets/image-upload/'.$type.'/';
		    $filename   = $this->uri->segment('4');
			$config['upload_path']      = $path;
			$config['allowed_types']    = 'jpg|jpeg|png|gif';
			$config['max_size']         = '1024';
			$config['file_name']        = $filename;
			$this->load->library('upload',$config);
			if (file_exists($path . $filename .'.'. $ext))
            { 
                unlink($path . $filename .'.'. $ext);
                if($this->upload->do_upload('file')){
				    $uploadData = $this->upload->data();
				    $_SESSION[$type] = $filename .'.'. $ext;
			    }
            } 
            else 
            {
                if($this->upload->do_upload('file')){
				    $uploadData = $this->upload->data();
				    $_SESSION[$type] = $filename .'.'. $ext;
			    }
            }
		}
    }
    
    
    public function delete($id){
        $this->builder->destroy('billboards', $id, 'billboards');
    }
    
    
    
    
    
    public function detail($id = ''){
        $data['billboard']  = $this->db->query("select billboards.*, applicants.name as applicant_name, applicants.phone_number as applicant_phone_number, applicants.address as applicant_address,
                                                        applicants.npwp as applicant_npwp, applicants.ktp as applicant_ktp, billboard_types.name as billboard_type_name, view_points.name as billboard_view_point_name,
                                                        street_class.name as billboard_street_class_name
                                                        from  billboards
                                                        join applicants on applicants.code = billboards.applicant_code
                                                        join billboard_types on billboard_types.code = billboards.billboard_type
                                                        join view_points on view_points.code = billboards.view_point
                                                        join street_class on street_class.code = billboards.street_class
                                                        where billboards.id = '$id'")->result();
        $this->load->view('billboards/show', $data);
    }
    
    
    
    
    
    
    public function show($id = ''){
        
        // $data['rates']=$this->builder->getAll('rates');
        // $data['street_class']=$this->builder->getAll('street_class');
        // $data['types']=$this->builder->getAll('billboard_types');
        // $data['viewpoints']=$this->builder->getAll('view_points');
        // $data['nsr']=$this->builder->getAll('nsr');
        // $data['billboards_record'] = $this->builder->getRecordById('billboards', ['id' => $id]);
        // $data['applicants_record'] = $this->builder->getRecordById('applicants', ['code' => $data['billboards_record'][0]->applicant_code]);
        
        $data['breadcrumb'] = array('title'=>'Billboards','parent'=>'Billboards','child'=>'Detail Data','icon'=>'ni ni-folder-17 text-green');
        $data['content'] = $this->load->view('billboards/detail', $data, TRUE);
        $this->load->view('layouts/index', $data);
        
    }
    
    
    
    
    public function show_dev($id){
        $data['billboard']  = $this->db->query("select billboards.*, applicants.name as applicant_name, applicants.phone_number as applicant_phone_number, applicants.address as applicant_address,
                                                        applicants.npwp as applicant_npwp, applicants.ktp as applicant_ktp, billboard_types.name as billboard_type_name, view_points.name as billboard_view_point_name,
                                                        street_class.name as billboard_street_class_name
                                                        from  billboards
                                                        join applicants on applicants.code = billboards.applicant_code
                                                        join billboard_types on billboard_types.code = billboards.billboard_type
                                                        join view_points on view_points.code = billboards.view_point
                                                        join street_class on street_class.code = billboards.street_class
                                                        where billboards.id = '$id'")->result();
        $data['breadcrumb'] = array('title'=>'Billboards','parent'=>'Billboards','child'=>'Detail Data','icon'=>'ni ni-folder-17 text-green');
        $data['content'] = $this->load->view('billboards/show', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    
    
    function updateByKey($table, $data, $key, $redirect){
        $this->db->where($key);
        if($this->db->update($table,$data)){
            return "Ok";
        } else {
            return "Not Ok";
        }
    }
    
    public function pay($code){
        if($this->isPost=="POST"){
            $data=array(
                'billboard_code' => $code,
                'payment_method' => $this->input->post('payment_method'),
                'total'          => $this->input->post('total'),
                'created_by'     => $this->session->userdata('user_id'),
            );
            $this->builder->store('payments',$data,'billboards');
        }
        $data['billboard']=$this->builder->getRecordById('billboards',['code' => $code]);
        $data['breadcrumb'] = array('title'=>'Billboards','parent'=>'Billboards','child'=>'Detail Data','icon'=>'ni ni-folder-17 text-green');
        $data['content'] = $this->load->view('billboards/pay', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function status($code){
        $data['levels']=$this->builder->getAllOrder('approval_levels','level','asc');
        $data['billboard']=$this->builder->getRecordById('billboards',['code' => $code]);
        $data['breadcrumb'] = array('title'=>'Billboards','parent'=>'Billboards','child'=>'Detail Data','icon'=>'ni ni-folder-17 text-green');
        $data['content'] = $this->load->view('billboards/status', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function approve($level){
        $code=$this->input->get('code');
        $data=array(
            'approved_at'=>date('Y-m-d H:i:s'),
            'approved_by'=>$this->session->userdata('user_id'), // nanti akan dihganti jadi employee id
            'approve_status'=>1,
            'remark'=>$this->input->post('approve-remark')
        );
        $this->db->trans_start();
        $this->db->where('billboard_code',$code);
        $this->db->where('level',$level);
        if($this->db->update('approvals', $data)){
            $totalappr=$this->builder->raw("select count(id) as total from approval_levels where deleted_at is null");
            if($totalappr[0]->total >= ($level+1)){
                $newappr=array(
                    'billboard_code'=>$code,
                    'level'=>$level+1,
                    'receive_at'=>date('Y-m-d H:i:s'),
                    'created_by'=>$this->session->userdata('user_id')
                );
                $this->db->insert('approvals',$newappr);
            }
            if($level==$totalappr[0]->total){
                $bb=array(
                    'approval_status'=>1,
                );
                $this->db->where('code',$code);
                $this->db->update('billboards', $bb);
                $this->builder->generatePayment('RGS',$code,date('Y-m-d'));
                $this->builder->generateQr($code);
            }
            $this->db->trans_complete();
            $this->session->set_flashdata('success',' Approval level '.$level.' disetujui');
        }else{
            $this->session->set_flashdata('failed','Gagal Merubah Data');
        }
        redirect('billboards/showdetail/'.$code);
    }
    
    public function reject($level){
        $code=$this->input->get('code');
        $data=array(
            'approved_at'=>date('Y-m-d H:i:s'),
            'approved_by'=>$this->session->userdata('user_id'), // nanti akan dihganti jadi employee id
            'approve_status'=>2,
            'remark'=>$this->input->post('reject-remark')
        );
        $this->db->where('billboard_code',$code);
        $this->db->where('level',$level);
        if($this->db->update('approvals', $data)){
            $bb=array(
                'approval_status'=>2,
            );
            $this->db->where('code',$code);
            $this->db->update('billboards', $bb);
            $this->session->set_flashdata('success',' Approval level '.$level.' ditolak');
        }else{
            $this->session->set_flashdata('failed','Gagal Merubah Data');
        }
        redirect('billboards/showdetail/'.$code);
    }
    
    public function showdetail($code){
        $billboard = $this->builder->getRecordByCond('billboards',['code'=>$code]);
        $applcode=$billboard[0]->applicant_code;
        $data['nsr']=$this->builder->raw("select nsr.*, street_class.name as area_name from nsr,street_class where nsr.street_class=street_class.code and nsr.deleted_at is null");
        $data['applicant']=$this->builder->getRecordByCond('applicants',['code'=>$applcode]);
        $data['levels']=$this->builder->getAllOrder('approval_levels','level','ASC');
        $data['billboard']=$billboard;
        $data['attachments']=$this->builder->raw("select r.*, atc.name as atc_name,atc.regex from registration_attachments r,attachment_types atc where r.type=atc.code and r.billboard_code='$code'");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Setup','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('billboards/billboard-detail', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
}
