<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
    
    
	public function dev(){
	    $data['rates']=$this->builder->getAll('rates');
	    $data['applicants']=$this->builder->getAll('applicants');
	    $data['street_class']=$this->builder->getAll('street_class');
	    $data['types']=$this->builder->getAll('billboard_types');
	    $data['viewpoints']=$this->builder->getAll('view_points');
	    $data['cities']=$this->builder->getAll('city');
	    $data['nsr']=$this->builder->getAll('nsr');
        $data['content'] = $this->load->view('registration/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Registrasi','parent'=>'Registrasi','child'=>'Tambah baru','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
	public function sendregistration($code=''){
	    $appr=array(
            'billboard_code'=>$code,
            'level'=>1,
            'created_by'=>$this->session->userdata('user_id'),
        );
        $this->db->insert('approvals', $appr);
	    $this->db->update('billboards', ['is_draft' => 0], ['code' => $code]);
	    $this->session->set_flashdata('success','Permohonan berhasil dikirim');
	    redirect('applicants/billboard');
    }
    
    
    
    
    
	public function index(){
	   // $data['applicants']=$this->builder->getRecordByCond('applicants',['user_id' => $this->session->userdata('user_id')]);
	    $data['applicants']=$this->db->query("select * from applicants left join users on users.id = applicants.user_id where user_id = {$this->session->userdata('user_id')}")->result();
	    
	   // $data['applicants']=$this->builder->getRecordByCond('applicants',['user_id' => 6]);
	    $data['rates']=$this->builder->getAll('rates');
	    $data['street_class']=$this->builder->getAll('street_class');
	    $data['types']=$this->builder->getAll('billboard_types');
	    $data['viewpoints']=$this->builder->getAll('view_points');
	    $data['cities']=$this->builder->getAll('city');
	    $data['land_status']=$this->builder->getAll('land_status');
	    $data['placement_billboard']=$this->builder->getAll('placement_billboard');
	    $data['nsr']=$this->builder->getAll('nsr');
        $data['content'] = $this->load->view('registration/index_dev', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Registrasi','parent'=>'Registrasi','child'=>'Tambah baru','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    
    public function create(){
        
        $explode = explode(',',$this->input->post('latitude_longitude'));
        $billboard_design = '';
        foreach($this->input->post('billboard_design') as $b) {
            $billboard_design .= $b.',';
        }
        $billboard_design=substr($billboard_design, 0, -1);
        if($this->input->post('category')=="PRD"){
            $product_type=$this->input->post('product_type');
        }else{
            $product_type="NPRD";
        }
        $billboard=array(
            'code'=>$this->builder->getNewBillboardCode(),
            'applicant_code'=>$this->input->post('applicant_code'),
            'category'=>$this->input->post('category'),
            'product_type'=>$product_type,
            'billboard_type'=>$this->input->post('billboard_type'),
            'view_point'=>$this->input->post('view_point'),
            'street_class'=>$this->input->post('street_class'),
            'city_code'=>$this->input->post('city_code'),
            'district_id'=>$this->input->post('district_id'),
            'street_id'=>$this->input->post('street_id'),
            'address'=>$this->input->post('install_address'),
            'latitude'=>$explode[0],
            'longitude'=>$explode[1],
            'length'=>$this->input->post('length'),
            'width'=>$this->input->post('width'),
            'height'=>$this->input->post('height'),
            'size'=>$this->input->post('size'),
            'year_rate'=>$this->input->post('year_rate'),
            'install_date'=>date('Y-m-d',strtotime($this->input->post('install_date'))),
            'finish_date'=>date('Y-m-d',strtotime($this->input->post('finish_date'))),
            'install_duration'=>$this->input->post('install_duration'),
            'start_time'=>$this->input->post('start_time'),
            'end_time'=>$this->input->post('end_time'),
            'applicant_type'=>$this->input->post('applicant_type'),
            'land_status'=>$this->input->post('land_status'),
            'placement_billboard'=>$this->input->post('placement_billboard'),
            'billboard_text'=>$this->input->post('billboard_text'),
            'time_duration'=>$this->input->post('time_duration'),
            'total'=>$this->input->post('total'),
            'detail_location'=>$this->input->post('detail_location'),
            'contract_value'=>$this->input->post('contract_value'),
            'created_by'=>$this->session->userdata('user_id'),
            'nsr'=>$this->input->post('nsr'),
            'tax'=>$this->input->post('tax'),
            'company_name'=>$this->input->post('company_name'),
            'company_address'=>$this->input->post('company_address'),
            'company_phone'=>$this->input->post('company_phone'),
            'company_fax'=>$this->input->post('company_fax'),
            'billboard_design'=>$billboard_design,
            'attached_to_building'=>$this->input->post('attached_to_building'),
        );
        if ($this->input->post('simpan')) {
            $billboards = array_merge($billboard, [ 'is_draft' => $this->input->post('simpan') ]);
        } else {
            $billboards = array_merge($billboard, [ 'is_draft' => $this->input->post('submit') ]);
        }
        $bid=$this->builder->storeReturnId('billboards',$billboards,'registration');
        $bcode=$this->builder->getCodeById('billboards',$bid);
        $isCgr=0;
        if($product_type=='RKAL'){
            $isCgr=1;
        }
        // $datansr=array(
        //     'area'=>$this->input->post('street_class'),
        //     'category'=>$this->input->post('category'),
        //     'cigarette'=>$isCgr,
        //     'third_person'=>$this->session->userdata('third_person'),
        //     'contract_value'=>$this->input->post('contract_value'),
        //     'location'=>$this->input->post('placement_billboard'),
        //     'height'=>$this->input->post('height'),
        //     'size'=>$this->input->post('size'),
        //     'tax'=>$this->input->post('year_rate'),
        //     'total_days'=>$this->input->post('install_duration'),
        //     'total_seconds'=>$this->input->post('time_duration'),
        //     'type'=>$this->input->post('billboard_type'),
        //     'total_selebaran'=>$this->input->post('total'),
        //     'month'=>$this->input->post('total'),
        //     'total_prg'=>$this->input->post('total'),
        //     'building'=>$this->input->post('attached_to_building'),
        // );
        // $this->builder->generateNsrAndTax($bcode, $datansr);
        $data = [
            'location' => $this->input->post('land_status'),
            'applicant_type' => $this->input->post('applicant_type'),
            'third_person' =>$this->session->userdata('third_person'),
            'view_point' =>$this->session->userdata('view_point'),
            'building'=>$this->input->post('attached_to_building'),
            ];
        $this->generateRequirements($bcode, $data);
        
        // $this->generateNewLog();
        
        
        // upload persyaratan semua dari upload attachments
        $bd = explode(',',$billboard_design);
        
        $dir = "./assets/image-upload/registration_attachments/$bcode/";
        if (!is_dir($dir)) { mkdir($dir, 0777, true); }
        
        $this->load->library('upload'); 
        $cpt = count($_FILES['tampak']['name']);
        for($i=0; $i < $cpt; $i++) {
            if(!empty($_FILES['tampak']['name'][$i])){
              $name = 'BD-'.$bcode.'-'.$bd[$i];
              $_FILES['file']['name']        = $_FILES['tampak']['name'][$i];
              $_FILES['file']['type']        = $_FILES['tampak']['type'][$i];
              $_FILES['file']['tmp_name']    = $_FILES['tampak']['tmp_name'][$i];
              $_FILES['file']['error']       = $_FILES['tampak']['error'][$i];
              $_FILES['file']['size']        = $_FILES['tampak']['size'][$i];
              $config['upload_path']         = $dir;
              $config['allowed_types']       = 'jpg|jpeg|png|gif';
              $config['max_size']            = '5000';
              $config['file_name']           = $name;
              $this->upload->initialize($config);
              $this->upload->do_upload('file');
              $file_info = $this->upload->data();
              $img = $file_info['file_name'];
              
                $registration_attachments = [
                    'billboard_code' => $bcode,
                    'type' => 'BD',
                    'file' => $bcode.'/'.$img,
                    'created_by'=>$this->session->userdata('user_id'),
                ];
                $this->db->insert('registration_attachments', $registration_attachments);
            }
        }
        redirect('registration/attachment/'.$bcode);
    }
    
    
    function generateRequirements($code='', $data=''){
        $reqs=array('PBB','SPIZIN','SPIMBBR','VP1','VP2','VP3','GIPTB','LAKLAL','SPARS');
        
        if($data['applicant_type'] == 'PSN'){
            array_push($reqs, 'KTP','KK');
        } else
        if($data['applicant_type'] == 'BDU'){
            array_push($reqs, "APP","SKPP","NPWP");
        }
        
        if($data['third_person']==1){
            array_push($reqs, "SKUASA");
        }
        
        if($data['building']==1){
            array_push($reqs, "IMB");
        }
        
        
        if($data['location']=="TPP"){
            array_push($reqs, "RTPL");
        }else if($data['location']=="TPR"){
            array_push($reqs, "SHM");
        }else if($data['location']=="TBUM"){
            array_push($reqs, "SPBUM");
        }else if($data['location']=="TSW"){
            array_push($reqs, "PSPTB","SPTK","KTP-PT");
        }
        $this->db->trans_start();
        foreach($reqs as $r){
            $data=array(
                'billboard_code'=>$code,
                'type'=>$r,
                'created_by'=>$this->session->userdata('user_id')
            );
            $this->db->insert('registration_attachments', $data);
        }
        $this->db->trans_complete();
        
    }
    
    
    
    
    function calculator()  {
        $category =  $this->input->post('category');
        $billboard_type =  $this->input->post('billboard_type');
        $billboard_type_id =  $this->input->post('billboard_type_id');
        $street_class =  $this->input->post('street_class');
        $street_class_id =  $this->input->post('street_class_id');
        $length =  $this->input->post('length');
        $width =  $this->input->post('width');
        $size =  $length * $width;
        $install_duration =  $this->input->post('install_duration');
        $time_duration =  $this->input->post('time_duration');
        $total =  $this->input->post('total');
        
        if ($billboard_type == "PBM" || $billboard_type == "KN") {
            /*
                $hg = ceil($height/12);
                if($hg >= 2) {
                    $kl = 25 / 100 * $x;
                }
            */
            $data = $this->db->query("select * from nsr where type = '$category' and street_class = '$street_class'")->result();
            if ($data) {
                print_r($data[0]->rate * $size * $install_duration);
            }
        } else
        if ($billboard_type == "LED") {
            /*
                $hg = ceil($height/12);
                if($hg >= 2) {
                    $kl = 25 / 100 * $x;
                }
            */
            $data = $this->db->query("select * from nsr_price where street_class = '$street_class' and start_size <= $size and end_size >= $size")->result();
            if ($data) {
                if($data[0]->uom == "Detik") {
                    $pem = 1;
                } else if($data[0]->uom == "Menit") {
                    $pem = 60;
                } else if($data[0]->uom == "Jam") {
                    $pem = 3600;
                }
                print_r($data[0]->price * (3600/$pem) * $time_duration);
            }
            
        } else
        if ($billboard_type == "BRJ") {
            $data = $this->db->query("select * from billboard_types where code = '$billboard_type'")->result();
            if ($data) {
                if($data[0]->uom == "Bulan") {
                    $pem = 30;
                } else if($data[0]->uom == "Hari") {
                    $pem = 1;
                }
                print_r($data[0]->price * $install_duration / $pem * $size);
            }
        } else
        {
            $data = $this->db->query("select * from billboard_types where code = '$billboard_type'")->result();
            if ($data) {
                if ($data[0]->uom == "Bulan" || $data[0]->uom == "Hari") {
                    
                    if($data[0]->uom == "Bulan") {
                        $pem = 30;
                    } else if($data[0]->uom == "Hari") {
                        $pem = 1;
                    }
                    print_r($data[0]->price * $install_duration / $pem);
                } else if ($data[0]->uom == "Detik" || $data[0]->uom == "Menit" || $data[0]->uom == "Jam") {
                    if($data[0]->uom == "Detik") {
                        $pem = 1;
                    } else if($data[0]->uom == "Menit") {
                        $pem = 60;
                    } else if($data[0]->uom == "Jam") {
                        $pem = 3600;
                    }
                    print_r($data[0]->price * (3600/$pem) * $time_duration);
                }  else if ($data[0]->uom == "Lembar" || $data[0]->uom == "Unit") {
                    print_r($data[0]->price * $total);
                }  else if ($data[0]->uom == "Meter") {
                    print_r($data[0]->price * $size);
                }
            }
        }
        
    }
    
    
    
    
        
    
    public function attachment($code=''){
        
        if(isset($_POST['submit'])){
            
            $this->load->library('upload'); 
            $dir = "./assets/image-upload/registration_attachments/$code/";
            if (!is_dir($dir)) { mkdir($dir, 0777, true); }
            $dt = $this->db->query("select * from registration_attachments where billboard_code = '$code' and type != 'BD'")->result();
            foreach($dt as $rs) {
              if (!empty($_FILES[$rs->type]['name'])) {

                  $name = $rs->type.'-'.$code;
                  $_FILES['file']['name']        = $_FILES[$rs->type]['name'];
                  $_FILES['file']['type']        = $_FILES[$rs->type]['type'];
                  $_FILES['file']['tmp_name']    = $_FILES[$rs->type]['tmp_name'];
                  $_FILES['file']['error']       = $_FILES[$rs->type]['error'];
                  $_FILES['file']['size']        = $_FILES[$rs->type]['size'];
                  $config['upload_path']         = $dir;
                  $config['allowed_types']       = 'jpg|jpeg|png|gif';
                  $config['max_size']            = '5000';
                  $config['file_name']           = $name;
                  
                  if(file_exists($dir.''.$name)){
                      unlink($dir."".$name);
                  }
                  $this->upload->initialize($config);
                  $this->upload->do_upload('file');
                  $file_info = $this->upload->data();
                  $img = $file_info['file_name'];
                  $data = [
                        'file' => $code.'/'.$img,
                      ];
                $this->db->update('registration_attachments', $data, ['billboard_code' => $code, 'type' => $rs->type]);
              }
            }
            redirect('applicants/billboard');
        }
        
        
        $data['attch'] =  $this->db->query("select registration_attachments.*,attachment_types.name as atc_name from registration_attachments,attachment_types where registration_attachments.type=attachment_types.code and billboard_code = '$code' order by attachment_types.ordinal")->result();
        $data['content'] = $this->load->view('registration/attachment_dev', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Registrasi','parent'=>'Registrasi','child'=>'Dokumen Persyaratan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
     public function editregistration($code){
	    $data['applicants']=$this->db->query("select * from applicants left join users on users.id = applicants.user_id where user_id = {$this->session->userdata('user_id')}")->result();
	    $billboard=$this->builder->getRecordByCond('billboards',['code'=>$code]);
	    $city_code=$billboard[0]->city_code;
	    $district_id=$billboard[0]->district_id;
	    $data['districts']=$this->builder->getRecordByCond('districts',['city_code'=>$city_code]);
	    $data['streets']=$this->builder->getRecordByCond('streets',['district_id'=>$district_id]);
	    $data['billboard']=$billboard;
	    $data['rates']=$this->builder->getAll('rates');
	    $data['street_class']=$this->builder->getAll('street_class');
	    $data['types']=$this->builder->getAll('billboard_types');
	    $data['viewpoints']=$this->builder->getAll('view_points');
	    $data['cities']=$this->builder->getAll('city');
	    $data['land_status']=$this->builder->getAll('land_status');
	    $data['placement_billboard']=$this->builder->getAll('placement_billboard');
	    $data['nsr']=$this->builder->getAll('nsr');
        $data['content'] = $this->load->view('registration/edit', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Registrasi','parent'=>'Registrasi','child'=>'Tambah baru','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function update($bcode){
        $explode = explode(',',$this->input->post('latitude_longitude'));
        if($this->input->post('category')=="PRD"){
            $product_type=$this->input->post('product_type');
        }else{
            $product_type="NPRD";
        }
        $billboard=array(
            'id'=>$this->builder->getIdByCode('billboards',$bcode),
            'category'=>$this->input->post('category'),
            'product_type'=>$product_type,
            'billboard_type'=>$this->input->post('billboard_type'),
            'view_point'=>$this->input->post('view_point'),
            'street_class'=>$this->input->post('street_class'),
            'city_code'=>$this->input->post('city_code'),
            'district_id'=>$this->input->post('district_id'),
            'street_id'=>$this->input->post('street_id'),
            'address'=>$this->input->post('install_address'),
            'latitude'=>$explode[0],
            'longitude'=>$explode[1],
            'length'=>$this->input->post('length'),
            'width'=>$this->input->post('width'),
            'height'=>$this->input->post('height'),
            'size'=>$this->input->post('size'),
            'year_rate'=>$this->input->post('year_rate'),
            'install_date'=>$this->input->post('install_date'),
            'finish_date'=>$this->input->post('finish_date'),
            'install_duration'=>$this->input->post('install_duration'),
            'start_time'=>$this->input->post('start_time'),
            'end_time'=>$this->input->post('end_time'),
            'applicant_type'=>$this->input->post('applicant_type'),
            'land_status'=>$this->input->post('land_status'),
            'placement_billboard'=>$this->input->post('placement_billboard'),
            'billboard_text'=>$this->input->post('billboard_text'),
            'time_duration'=>$this->input->post('time_duration'),
            'total'=>$this->input->post('total'),
            'detail_location'=>$this->input->post('detail_location'),
            'contract_value'=>$this->input->post('contract_value'),
            'updated_at'=>date('Y-m-d H:i:s'),
            'attached_to_building'=>$this->input->post('attached_to_building'),
            'nsr'=>$this->input->post('nsr'),
            'tax'=>$this->input->post('tax'),
            'company_name'=>$this->input->post('company_name'),
            'company_address'=>$this->input->post('company_address'),
            'company_phone'=>$this->input->post('company_phone'),
            'company_fax'=>$this->input->post('company_fax'),
            
        );
        $this->db->where('code',$bcode);
        
        $this->builder->update('billboards',$billboard,'applicants/billboard');
    }
}
