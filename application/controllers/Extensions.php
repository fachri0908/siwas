<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extensions extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
        $this->isPost=$this->input->method(TRUE);
    }
    
    public function index(){
        $data['extensions']=$this->builder->getRecordByCond('extensions',['applicant_code'=>$this->session->userdata('user_code')]);
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Perpanjangan','child'=>'List Perpanjangan','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('extensions/index', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }

    public function request($code){
        if($_POST){
            $extcode=$this->builder->getNewExtensionCode();
            $payment=$this->builder->generateExtensionNsr($code, $this->input->post('install_duration'), $this->input->post('time_duration'));
            $data=array(
                'code'=>$extcode,
                'applicant_code'=>$this->session->userdata('user_code'),
                'billboard_code'=>$code,
                'duration'=>$this->input->post('install_duration'),
                'uom'=>$this->input->post('uom'),
                'start_date'=>$this->input->post('start_date'),
                'finish_date'=>$this->input->post('finish_date'),
                'time_duration'=>$this->input->post('time_duration'),
                'approval_status'=>0,
                'nsr'=>$payment['nsr'],
                'tax'=>$payment['tax'],
                'created_by'=> $this->session->userdata('user_id')
            );
            $extid=$this->builder->storeReturnId('extensions',$data,'extensions/request/'.$code);
            $this->generateRequirements($extcode);
            // $this->generateExtensionData($code, $extcode);
            redirect('extensions');
        }
        $data['billboard'] = $this->builder->getRecordByCond("billboards",['code'=>$code]);
        $data['attachments']=$this->builder->raw("select r.*, atc.name as atc_name,atc.regex from registration_attachments r,attachment_types atc where r.type=atc.code and r.billboard_code='$code'");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Perpanjangan','child'=>'Pengajuan Perpanjangan','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('extensions/form', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function edit($extcode){
        if($_POST){
            $ext=array(
                'duration'=>$this->input->post('install_duration'),
                'uom'=>$this->input->post('uom'),
                'start_date'=>$this->input->post('start_date'),
                'finish_date'=>$this->input->post('finish_date'),
                'time_duration'=>$this->input->post('time_duration'),
            );
            $this->db->where('code', $extcode);
            if($this->db->update('extensions', $ext)){
                $this->session->set_flashdata('success','Berhasil Menambahkan Data');
                redirect('extensions/edit/'.$extcode);
            }else{
                $this->session->set_flashdata('failed','Gagal Menambahkan Data');
                redirect('extensions/edit/'.$extcode);
            }
        }
        $ext=$this->builder->getRecordByCond("extensions",['code'=>$extcode]);
        $bcode=$ext[0]->billboard_code;
        $data['extension']=$ext;
        $data['billboard'] = $this->builder->getRecordByCond("billboards",['code'=>$bcode]);
        $data['attachments']=$this->builder->raw("select r.*, atc.name as atc_name,atc.regex from registration_attachments r,attachment_types atc where r.type=atc.code and r.billboard_code='$bcode'");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Perpanjangan','child'=>'Pengajuan Perpanjangan','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('extensions/edit', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function attachment($code){
        if($_POST){
            $this->load->library('upload'); 
            $dir = "./assets/image-upload/extension_attachments/$code/";
            if (!is_dir($dir)) { mkdir($dir, 0777, true); }
            $dt = $this->db->query("select * from extension_attachments where extension_code = '$code'")->result();
            foreach($dt as $rs) {
              if (!empty($_FILES[$rs->type]['name'])) {

                  $name = $rs->type.'-'.$code;
                  $_FILES['file']['name']        = $_FILES[$rs->type]['name'];
                  $_FILES['file']['type']        = $_FILES[$rs->type]['type'];
                  $_FILES['file']['tmp_name']    = $_FILES[$rs->type]['tmp_name'];
                  $_FILES['file']['error']       = $_FILES[$rs->type]['error'];
                  $_FILES['file']['size']        = $_FILES[$rs->type]['size'];
                  $config['upload_path']         = $dir;
                  $config['allowed_types']       = 'jpg|jpeg|png|gif';
                  $config['max_size']            = '5000';
                  $config['file_name']           = $name;
                  
                  if(file_exists($dir.''.$name)){
                      unlink($dir."".$name);
                  }
                  $this->upload->initialize($config);
                  $this->upload->do_upload('file');
                  $file_info = $this->upload->data();
                  $img = $file_info['file_name'];
                  $data = [
                        'file' => $code.'/'.$img,
                      ];
                $this->db->update('extension_attachments', $data, ['extension_code' => $code, 'type' => $rs->type]);
              }
            }
            redirect('extensions');
        }
        
        $data['attch'] =  $this->db->query("select extension_attachments.*,attachment_types.name as atc_name from extension_attachments,attachment_types where extension_attachments.type=attachment_types.code and extension_code = '$code' order by attachment_types.ordinal")->result();
        $data['content'] = $this->load->view('extensions/attachment', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Perpanjangan','parent'=>'Perpanjagan','child'=>'Dokumen Persyaratan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    
    public function submit($code){
        $this->db->where('code',$code);
        $this->db->update('extensions', array('is_draft'=>0));
        $appr=array(
            'extension_code'=>$code,
            'level'=>1,
            'created_by'=>$this->session->userdata('user_id'),
        );
        $this->builder->store('extension_approvals', $appr,'extensions');
    }
    
    public function detail($extcode){
        $data['extension']=$this->builder->getRecordByCond('extensions',['code'=>$extcode]);
        $code=$this->builder->raw("select billboard_code from extensions where code='$extcode'");
        $code=$code[0]->billboard_code;
        $billboard = $this->builder->getRecordByCond('billboards',['code'=>$code]);
        $applcode=$billboard[0]->applicant_code;
        $data['nsr']=$this->builder->raw("select nsr.*, street_class.name as area_name from nsr,street_class where nsr.street_class=street_class.code and nsr.deleted_at is null");
        $data['applicant']=$this->builder->getRecordByCond('applicants',['code'=>$applcode]);
        $data['levels']=$this->builder->getAllOrder('extensions_approval_levels','level','ASC');
        $data['billboard']=$billboard;
        $data['attachments']=$this->builder->raw("select r.*, atc.name as atc_name,atc.regex from extension_attachments r,attachment_types atc where r.type=atc.code and r.extension_code='$extcode'");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Setup','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('extensions/detail', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function admindetail($extcode){
        $data['extension']=$this->builder->getRecordByCond('extensions',['code'=>$extcode]);
        $code=$this->builder->raw("select billboard_code from extensions where code='$extcode'");
        $code=$code[0]->billboard_code;
        $billboard = $this->builder->getRecordByCond('billboards',['code'=>$code]);
        $applcode=$billboard[0]->applicant_code;
        $data['nsr']=$this->builder->raw("select nsr.*, street_class.name as area_name from nsr,street_class where nsr.street_class=street_class.code and nsr.deleted_at is null");
        $data['applicant']=$this->builder->getRecordByCond('applicants',['code'=>$applcode]);
        $data['levels']=$this->builder->getAllOrder('extensions_approval_levels','level','ASC');
        $data['billboard']=$billboard;
        $data['attachments']=$this->builder->raw("select r.*, atc.name as atc_name,atc.regex from extension_attachments r,attachment_types atc where r.type=atc.code and r.extension_code='$extcode'");
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Setup','child'=>'index','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('extensions/admin/detail', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    public function approve($level){
        $code=$this->input->get('code');
        $data=array(
            'approved_at'=>date('Y-m-d H:i:s'),
            'approved_by'=>$this->session->userdata('user_id'), // nanti akan dihganti jadi employee id
            'approve_status'=>1,
            'remark'=>$this->input->post('approve-remark')
        );
        $this->db->trans_start();
        $this->db->where('extension_code',$code);
        $this->db->where('level',$level);
        if($this->db->update('extension_approvals', $data)){
            $totalappr=$this->builder->raw("select count(id) as total from extensions_approval_levels where deleted_at is null");
            if($totalappr[0]->total >= ($level+1)){
                $newappr=array(
                    'extension_code'=>$code,
                    'level'=>$level+1,
                    'receive_at'=>date('Y-m-d H:i:s'),
                    'created_by'=>$this->session->userdata('user_id')
                );
                $this->db->insert('extension_approvals',$newappr);
            }
            if($level==$totalappr[0]->total){
                $bb=array(
                    'approval_status'=>1,
                );
                $this->db->where('code',$code);
                $this->db->update('extensions', $bb);
                $this->builder->extend($code);
                $this->builder->generateExtensionPayment($code);
            }
            $this->db->trans_complete();
            $this->session->set_flashdata('success',' Approval level '.$level.' disetujui');
        }else{
            $this->session->set_flashdata('failed','Gagal Merubah Data');
        }
        redirect('extensions/admindetail/'.$code);
    }
    
    public function reject($level){
        $code=$this->input->get('code');
        $data=array(
            'approved_at'=>date('Y-m-d H:i:s'),
            'approved_by'=>$this->session->userdata('user_id'), // nanti akan dihganti jadi employee id
            'approve_status'=>2,
            'remark'=>$this->input->post('reject-remark')
        );
        $this->db->where('extension_code',$code);
        $this->db->where('level',$level);
        if($this->db->update('extension_approvals', $data)){
            $bb=array(
                'approval_status'=>2,
            );
            $this->db->where('code',$code);
            $this->db->update('extensions', $bb);
            $this->session->set_flashdata('success',' Approval level '.$level.' ditolak');
        }else{
            $this->session->set_flashdata('failed','Gagal Merubah Data');
        }
        redirect('extensions/admindetail/'.$code);
    }
    
    public function admin(){
        $data['extensions']=$this->builder->getRecordByCond('extensions',['is_draft'=>0]);
        $data['breadcrumb'] =array('title'=>'Reklame','parent'=>'Perpanjangan','child'=>'List Perpanjangan','icon'=>'ni ni-settings text-green');
        $data['content'] = $this->load->view('extensions/admin/index', $data, TRUE);
        $this->load->view('layouts/index', $data);
    }
    
    
    function generateRequirements($code){
        $reqs=array('EXT-STS','EXT-SKPD','EXT-SKRD','EXT-IPTB','EXT-TLB','EXT-IMB','EXT-IPRA');
        
        $this->db->trans_start();
        foreach($reqs as $r){
            $data=array(
                'extension_code'=>$code,
                'type'=>$r,
                'created_by'=>$this->session->userdata('user_id')
            );
            $this->db->insert('extension_attachments', $data);
        }
        $this->db->trans_complete();
        
    }
}
