<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approvals extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function registration(){
	    if($_POST){
	       $data=array(
	           'name'=>$this->input->post('name'),
	           'approver_position_code'=>$this->input->post('approver_position_code'),
	           'level'=>$this->input->post('level'),
	       );
	       $this->builder->store("approval_levels",$data,"approvals/registration");
	    }
	   // $data['positions']=$this->builder->raw('select * from positions where code not in(select approver_position_code from approval_levels)');
	    $data['positions']=$this->builder->getAll('positions');
	    $data['levels']=$this->builder->getAllOrder('approval_levels','level','ASC');
        $data['content'] = $this->load->view('approvals/registration/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Setup','parent'=>'Setup','child'=>'persetujuan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function updateregistration($id){
        $data=array(
            'id'=>$id,
            'approver_position_code'=>$this->input->post('approver_position_code'),
            'name'=>$this->input->post('name'),
        );
        $this->builder->update('approval_levels',$data,'approvals/registration');
    }

    public function extension(){
        if($_POST){
	       $data=array(
	           'name'=>$this->input->post('name'),
	           'approver_position_code'=>$this->input->post('approver_position_code'),
	           'level'=>$this->input->post('level'),
	       );
	       $this->builder->store("extensions_approval_levels",$data,"approvals/extension");
        }
	    $data['positions']=$this->builder->getAll('positions');
	    $data['levels']=$this->builder->getAllOrder('extensions_approval_levels','level','ASC');
        $data['content'] = $this->load->view('approvals/extension/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Setup','parent'=>'Setup','child'=>'persetujuan','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
    
    public function updateextension($id){
        $data=array(
            'id'=>$id,
            'approver_position_code'=>$this->input->post('approver_position_code'),
            'name'=>$this->input->post('name'),
        );
        $this->builder->update('extensions_approval_levels',$data,'approvals/extension');
    }
    
    public function extensionup($ordinal){
        $this->db->trans_start();
        $this->db->where('level',$ordinal);
        $this->db->update('extensions_approval_levels',array('level'=>0));
        
        $this->db->where('level',$ordinal-1);
        $this->db->update('extensions_approval_levels',array('level'=>$ordinal));
        
        $this->db->where('level',0);
        $this->db->update('extensions_approval_levels',array('level'=>$ordinal-1));
        
        $this->db->trans_complete();
        redirect('approvals/extension');
    }
    
    public function extensiondown($ordinal){
        $this->db->trans_start();
        $this->db->where('level',$ordinal);
        $this->db->update('extensions_approval_levels',array('level'=>0));
        
        $this->db->where('level',$ordinal+1);
        $this->db->update('extensions_approval_levels',array('level'=>$ordinal));
        
        $this->db->where('level',0);
        $this->db->update('extensions_approval_levels',array('level'=>$ordinal+1));
        
        $this->db->trans_complete();
        redirect('approvals/extension');
    }
    
    public function registrationup($ordinal){
        $this->db->trans_start();
        $this->db->where('level',$ordinal);
        $this->db->update('approval_levels',array('level'=>0));
        
        $this->db->where('level',$ordinal-1);
        $this->db->update('approval_levels',array('level'=>$ordinal));
        
        $this->db->where('level',0);
        $this->db->update('approval_levels',array('level'=>$ordinal-1));
        
        $this->db->trans_complete();
        redirect('approvals/registration');
    }
    
    public function registrationdown($ordinal){
        $this->db->trans_start();
        $this->db->where('level',$ordinal);
        $this->db->update('approval_levels',array('level'=>0));
        
        $this->db->where('level',$ordinal+1);
        $this->db->update('approval_levels',array('level'=>$ordinal));
        
        $this->db->where('level',0);
        $this->db->update('approval_levels',array('level'=>$ordinal+1));
        
        $this->db->trans_complete();
        redirect('approvals/registration');
    }
    
}
