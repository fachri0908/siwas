<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('builder');
        if(!$this->session->userdata('is_login')) redirect('auth');
    }
    
	public function index(){
	   // $data['applicants']=$this->builder->getAll('applicants');
	    $data['applicants']=$this->builder->raw("select applicants.*,users.email,users.token,users.validated_at from applicants,users where applicants.user_id=users.id");
        $data['content'] = $this->load->view('users/index', $data, TRUE);
        $data['breadcrumb'] = array('title'=>'Profile','parent'=>'Profile','child'=>'Edit Profile','icon'=>'ni ni-folder-17 text-green');
        $this->load->view('layouts/index', $data);
    }
}
